from setuptools import setup
import os

setup(
    name='aavslmc',
    version='0.1',
    packages= [x[0] for x in os.walk('aavslmc')],
    url='https://bitbucket.org/aavslmc/aavs-tango',
    license='',
    author='Andrea DeMarco',
    author_email='andrea.demarco@um.edu.mt',
    description='AAVS LMC'
)
