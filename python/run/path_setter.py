import sys
import os
# # import from package aavslmc
# sys.path.append('..')
# # pogo inheritance imports the class by name so we need to make the inheritance classes available in path
# sys.path.append('../aavslmc/servers/abstract')

print sys.argv
cwd = os.path.dirname(sys.argv[0])
if len(cwd):
    os.chdir(cwd)

sys.path.append("..")
server_paths = ["../aavslmc/servers/hardware",
                "../aavslmc/servers/data",
                "../aavslmc/servers/observations",
                "../aavslmc/servers/jobs",
                "../aavslmc/servers/api",
                "../../../aavs-daq/python/pydaq"]

sys.path.extend(["../aavslmc/servers/abstract"])
sys.path.extend(server_paths)
