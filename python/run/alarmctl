#!/opt/aavs/python/bin/python

import logging
import importlib
import json
import argparse
from logging import config
from PyTango import Database, DbDevInfo, DeviceProxy
from PyTango import DevFailed
import sys

sys.path.append('..')
from aavslmc.config import config
from aavslmc.servers.alarms.formula_translator import AlarmTranslators


LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s [%(process)s] [%(levelname)s] [%(module)s] %(message)s',
            'datefmt': "%Y-%m-%d %H:%M:%S"
        },
        'simple': {
            'format': '%(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'aavs.ctl': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'aavs.none': {
            'handlers': [],
            'level': 'DEBUG',
        }
    }
}
logging.config.dictConfig(LOGGING)
db = Database()


def config_alarms(alarm_config, domain="test"):
    json_alarms = AlarmTranslators.python_dict_to_json_alarms(alarm_config=alarm_config)

    lmc_device_ids = db.get_device_member("{}/{}/*".format(domain, "lmc"))
    if len(lmc_device_ids) > 0:
        master_lmc_dp = DeviceProxy("{}/{}/{}".format(domain,"lmc",lmc_device_ids[0]))
        master_lmc_dp.set_timeout_millis(30000)
 
        log.info("Configuring alarms")
        for alarm in json_alarms:
            log.info(alarm)
            master_lmc_dp.command_inout("create_alarm", json.dumps({"alarm_json": alarm}))


def clear_alarms(domain="test"):
    try:
        log.info("Removing configured alarms")
        alarm_device_ids = db.get_device_member("{}/{}/*".format(domain, "Alarm"))
        lmc_device_ids = db.get_device_member("{}/{}/*".format(domain, "lmc"))

        if len(alarm_device_ids) > 0:
            alarm_device_dp = DeviceProxy("{}/{}/{}".format(domain, "Alarm", alarm_device_ids[0]))
            alarm_device_dp.set_timeout_millis(30000)

            configured_alarms = alarm_device_dp.command_inout("Configured", "")
            for alarm in configured_alarms:
                alarm_id = alarm.split("\t")[1]
                if len(lmc_device_ids) > 0:
                    try:
                        master_lmc_dp = DeviceProxy("{}/{}/{}".format(domain, "lmc", lmc_device_ids[0]))
                        master_lmc_dp.set_timeout_millis(30000)
                        master_lmc_dp.command_inout("remove_alarm", json.dumps({"alarm_name": alarm_id}))
                    except:
                        try:
                            alarm_device_dp.command_inout("ack", [str(alarm_id)])
                        except:
                            pass
                        alarm_device_dp.command_inout("remove", str(alarm_id))
    except DevFailed, e:
        log.debug("Device failure, is it exported?: %s" % str(e))

# Script entry point
if __name__ == '__main__':
    log = logging.getLogger('aavs.ctl')

    parser = argparse.ArgumentParser(description='Bootstrap')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--clear', action="store_true", default=False, help="Clear all system alarms")
    group.add_argument('--config', action="store_true", default=False, help="Configure alarms")
    args = parser.parse_args()

    log = logging.getLogger('aavs.ctl')
    alarms_config_file = config["alarms_config_file"]
    domain = config["domain"]

    if args.clear:
        try:
            log.info("Cleaning up alarms...")
            clear_alarms(domain=domain)
        except Exception as e:
            log.info(str(e))
    elif args.config:
        if alarms_config_file is not None:
            try:
                log.info("Setting up alarms on devices...")
                config_module = importlib.import_module(alarms_config_file)
                alarms_dict = config_module.alarms_config
                config_alarms(alarms_dict, domain=domain)
            except Exception as e:
                log.info(str(e))
