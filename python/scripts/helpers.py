from PyTango import Database, DbDevInfo, DeviceProxy
import os, signal, json
import PyTango

# A reference on the Database
db = Database()


def kill_device_servers(device_name):
    try:
        dev_info = db.get_device_info(device_name)
        if dev_info.pid != 0:
           os.kill(dev_info.pid, signal.SIGTERM)  #o r signal.SIGKILL
        print "Killed PID: %s" % dev_info.pid
    except Exception as ex:
        print "No process to kill for %s" % device_name


def get_dev_info(device_name):
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    return dev_info


def print_devinfo(device_name, dev_info):
    print "Device <<%s>> found:" % device_name
    print "Name: %s" % (dev_info.name)
    print "Class Name: %s" % (dev_info.class_name)
    print "Full Name: %s" % (dev_info.ds_full_name)
    print "Exported: %s" % (dev_info.exported)
    print "IOR: %s" % (dev_info.ior)
    print "Version: %s" % (dev_info.version)
    print "PID: %s" % (dev_info.pid)
    print "Started Date: %s" % (dev_info.started_date)
    print "Stopped Date: %s" % (dev_info.stopped_date)


def add_device(domain, device_server_name, device_id):

    group_name = device_server_name.lower().replace('_ds', '')
    device_name = '%s/%s/%s' % (domain, group_name, device_id)
    kill_device_servers(device_name)
    dev_info = get_dev_info(device_name)

    if not dev_info:
        dev_info = DbDevInfo()
        dev_info._class = device_server_name
        dev_info.server = '%s/%s' % (device_server_name, domain)
        # add the device
        dev_info.name = device_name
        print("Creating device: %s" % device_name)
        db.add_device(dev_info)


def create_station(domain):
    # Not working:
    # py = PyTango.Util(['Tile_DS', domain, '-v4'])
    # from Tile_DS import Tile_DSClass,Tile_DS
    # py.add_class(Tile_DSClass,Tile_DS,'Tile_DS')
    #
    # py.create_device('Tile_DS', '%s/tile/1' % domain)
    # py.create_device('Tile_DS', '%s/tile/2' % domain)
    # py.create_device('Tile_DS', '%s/tile/3' % domain)
    # --end Not working

    add_device(domain, 'Station_DS', '1')
    add_device(domain, 'Station_DS', '2')
    add_device(domain, 'Station_DS', '3')

    add_device(domain, 'Tile_DS', '1')
    add_device(domain, 'Tile_DS', '2')
    add_device(domain, 'Tile_DS', '3')


def bootstrap_station(domain):
    group_station = {'group_name': 'tiles', 'device_name': ''}
    dp_station_1 = DeviceProxy('%s/station/1' % domain)

    group_station['device_name'] = '%s/tile/1' % domain
    dp_station_1.command_inout('add_member', json.dumps(group_station))

    group_station['device_name'] = '%s/tile/3' % domain
    dp_station_1.command_inout('add_member', json.dumps(group_station))

    dp_station_2 = DeviceProxy('%s/station/2' % domain)

    group_station['device_name'] = '%s/tile/2' % domain
    dp_station_2.command_inout('add_member', json.dumps(group_station))