# from PyTango import DeviceProxy
# import time
#
# sender = DeviceProxy('test/speed/1')
# receiver = DeviceProxy('test/speed/2')
#
# start = time.time()
#
# for run in xrange(0, 4000):
#     sender.command_inout("send")
#
# end = time.time()
# elapsed_time = end - start
# print elapsed_time
#
# print "Tx: " + str(sender["cnt_sent"].value)
# print "Rx: " + str(receiver["cnt_received"].value)



#############################################################


from PyTango import DeviceProxy
import time

sender = DeviceProxy('test/speed/1')
receiver = DeviceProxy('test/speed/2')

start = time.time()

for run in xrange(0, 1000):
    sender.command_inout("send_event")
    time.sleep(0.005)
    print "Tx: " + str(sender["cnt_sent"].value)
    print "Rx: " + str(receiver["cnt_received"].value)

end = time.time()
elapsed_time = end - start
print elapsed_time

