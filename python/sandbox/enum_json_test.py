import json
from enum import Enum

class Device(Enum):
    """ Device enumeration """
    FPGA_1 = 1
    FPGA_2 = 2
    FPGA_3 = 4
    FPGA_4 = 8
    FPGA_5 = 16
    FPGA_6 = 32
    FPGA_7 = 64
    FPGA_8 = 128
    Board  = 65536

class EnumEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return {"__enum__": str(obj)}
        return json.JSONEncoder.default(self, obj)

def as_enum(d):
    if "__enum__" in d:
        name, member = d["__enum__"].split(".")
        return getattr(globals()[name], member)
    else:
        return d

def main():
    chosen = Device.FPGA_1
    print chosen

    json_result = json.dumps(chosen, cls=EnumEncoder)
    extracted = json.loads(json_result, object_hook=as_enum)
    print extracted

if __name__ == '__main__':
    main()