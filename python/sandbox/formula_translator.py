import json
import re


class FormulaTranslator(object):

    """
    This class represents a translator from the JSON alarm request we will generate, into a formula (or list of
    formulae that can be parsed by the Elettra alarm device.

    The format of our JSON alarm messages is as follows:

        {
          "alarms":[
            {
              "address": "test/device/id_1/prop1",
              "name": "name1",
              "checks":[
                { "type": "ABSOLUTE",
                  "value": "20"
                },
                { "type": "MIN",
                  "value": "0"
                },
                { "type": "MAX",
                  "value": "100"
                }
                ]
            },
            {
              "address": "test/device/id_1/prop2",
              "name": "name2",
              "checks":[
                { "type": "ABSOLUTE",
                  "value": "60"
                }
                ]
            },
            {
              "address": "test/device/id_1",
              "name": "name3",
              "checks":[
                { "type": "STATUS",
                  "value": "ON"
                }
                ]
            }
            ]
        }


    """

    states = {"ON", "OFF", "CLOSE", "OPEN", "INSERT", "EXTRACT", "MOVING", "STANDBY", "FAULT", "INIT", "RUNNING",
              "ALARM", "DISABLE", "UNKNOWN"}

    @classmethod
    def translate(cls, json_alarm_msg):
        """
        Accepts a JSON alarm request, translates it to Elettra compatible formulae, as well as returns a list of
        attributes on which the alarms are set.
        :param json_alarm_msg:
        :return: formulas, attributes
            (list of formula string compatible with Elettra alarm device)
            (list of attribute addresses for which some alarm string has been generated)
        """
        formulas=[]
        attributes=[]
        trigger_times = []
        names=[]

        alarms_dict = json.loads(json_alarm_msg)
        for alarm in alarms_dict["alarms"]:
            alarm_trigger_time = alarm.get("time")
            if alarm_trigger_time is None:
                alarm_trigger_time = "0"
            else:
                alarm_trigger_time = str(alarm_trigger_time)

            if "address" in alarm:
                address = alarm["address"]
                if re.match(r'\w+/\w+/\w+/\w+', address): # if attribute address
                    attribute_address = True
                    device_address = False
                elif re.match(r'\w+/\w+/\w+', address): # if device address
                    attribute_address = False
                    device_address = True

                if "checks" in alarm:
                    status_check = False
                    abs_check = False
                    min_check = False
                    max_check = False
                    abs_string = ""
                    min_string = ""
                    max_string = ""
                    status_string = ""

                    for check in alarm["checks"]:
                        check_type = check["type"]
                        value = check["value"]

                        if check_type == "ABSOLUTE":
                            abs_string = address + " == " + str(value)
                            abs_check = True

                        if check_type == "MIN":
                            min_check = True
                            min_string = address + " <= " + str(value)

                        if check_type == "MAX":
                            max_check = True
                            max_string = address + " >= " + str(value)

                        if check_type == "STATUS":
                            if value in cls.states:
                                status_check == True
                                status_string = address + "/State" " == " + str(value)

                    if attribute_address:
                        if abs_check == True and min_check == False and max_check == False:
                            alarm_string = "(" + abs_string + ")"
                        elif abs_check == False and min_check == True and max_check == False:
                            alarm_string = "(" + min_string + ")"
                        elif abs_check == False and min_check == False and max_check == True:
                            alarm_string = "(" + max_string + ")"
                        elif abs_check == False and min_check == True and max_check == True:
                            alarm_string = "(" + min_string + ")" + " || " + "(" + max_string + ")"
                        elif abs_check == True and min_check == True and max_check == False:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + min_string + ")"
                        elif abs_check == True and min_check == False and max_check == True:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + max_string + ")"
                        elif abs_check == True and min_check == True and max_check == True:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + min_string + ")" + " || " + "(" + max_string + ")"

                    elif device_address:
                        alarm_string = "(" + status_string + ")"

                    if "name" in alarm:
                        name = alarm["name"]

                    if len(alarm["checks"]) > 0:
                        formulas.append(alarm_string)
                        if attribute_address:
                            attributes.append(address)
                        elif device_address:
                            attributes.append(address + "/State")
                        trigger_times.append(alarm_trigger_time)
                        names.append(name)
                    alarm_string = ""


        argout = formulas, attributes, trigger_times, names
        return argout

def main():
    json_data = open("alarm_dummy.json").read()
    formulas, attributes, trigger_times, names = FormulaTranslator.translate(json_data)
    print formulas
    print attributes
    print trigger_times
    print names

if __name__ == '__main__':
    main()