from PyTango import Database, DbDevInfo, DeviceProxy
from PyTango import DbData, DbDatum
import os, signal

# A reference on the Database
db = Database()

# Kill running servers
# Get device info of Tile
try:
    dev_info = db.get_device_info('test/AlarmStream/1')
    if dev_info.pid != 0:
       os.kill(dev_info.pid, signal.SIGTERM)  #o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/AlarmStream/1"


# Get device info of Tile
try:
    dev_info = db.get_device_info('test/tile/1')
    if dev_info.pid != 0:
       os.kill(dev_info.pid, signal.SIGTERM)  #o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/tile/1"


# Get device info of TPM
try:
    dev_info = db.get_device_info('test/tpm/1')
    if dev_info.pid != 0:
       os.kill(dev_info.pid, signal.SIGTERM)  #o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/tpm/1"


# Get device info of FormulaConf
try:
    dev_info = db.get_device_info('test/formulaconf/1')
    if dev_info.pid != 0:
       os.kill(dev_info.pid, signal.SIGTERM)  #o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/formulaconf/1"

try:
    dev_info = db.get_device_info('test/dummydevice/1')
    if dev_info.pid != 0:
        os.kill(dev_info.pid, signal.SIGTERM)  # o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/dummydevice/1"

try:
    dev_info = db.get_device_info('test/Alarm/1')
    if dev_info.pid != 0:
        os.kill(dev_info.pid, signal.SIGTERM)  # o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
        print "No process to kill for 'test/Alarm/1"

# Get device info of Tile
try:
    dev_info = db.get_device_info('test/obsconf/1')
    if dev_info.pid != 0:
        os.kill(dev_info.pid, signal.SIGTERM)  # o r signal.SIGKILL
    print "Killed PID: %s" % dev_info.pid
except Exception as ex:
    print "No process to kill for 'test/obsconf/1"


# get device info of Tile_DS
def tile_device_info(device_id):
    device_name = 'test/tile/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "Tile_DS"
        dev_info.server = "Tile_DS/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

# get device info of TPM_DS
def tpm_device_info(device_id):
    device_name = 'test/tpm/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "TPM_DS"
        dev_info.server = "TPM_DS/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

#get device info of FormulaConf
def formulaconf_device_info(device_id):
    device_name = 'test/formulaconf/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "FormulaConf"
        dev_info.server = "FormulaConf/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

#get device info of FormulaConfHandler
def formulaconfhandler_device_info(device_id):
    device_name = 'test/formulaconfhandler/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "FormulaConfHandler"
        dev_info.server = "FormulaConfHandler/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

#get device info of Dummy
def dummy_device_info(device_id):
    device_name = 'test/dummy/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "DummyDevice"
        dev_info.server = "DummyDevice/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

#get device info of Alarm
def alarm_device_info(device_id):
    device_name = 'test/Alarm/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "Alarm"
        dev_info.server = "Alarm/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)
        alarm_device = DeviceProxy(device_name)

        # Setting Device Properties
        properties = {"DbHost": "localhost", "DbName": "alarm", "DbUser": "alarm_usr", "DbPasswd": "alarm_pswd",
                      "DbPort": "3306", "InstanceName": "1"}
        alarm_device.put_property(properties)

def alarm_stream_device_info(device_id):
    device_name = 'test/AlarmStream/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "AlarmStream"
        dev_info.server = "AlarmStream/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)
        alarm_device = DeviceProxy(device_name)

        # # Setting Device Properties
        # properties = {"DbHost": "localhost", "DbName": "alarm", "DbUser": "alarm_usr", "DbPasswd": "alarm_pswd",
        #               "DbPort": "3306", "InstanceName": "1"}
        # alarm_device.put_property(properties)

#get device info of Event
def event_device_info(device_id):
    device_name = 'test/EventStream/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "EventStream"
        dev_info.server = "EventStream/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)

# get device info of ObsConf
def obsconf_device_info(device_id):
    device_name = 'test/obsconf/%s' % device_id
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    if not dev_info is None:
        print "Device <<%s>> found:" % device_name
        print "Name: %s" % (dev_info.name)
        print "Class Name: %s" % (dev_info.class_name)
        print "Full Name: %s" % (dev_info.ds_full_name)
        print "Exported: %s" % (dev_info.exported)
        print "IOR: %s" % (dev_info.ior)
        print "Version: %s" % (dev_info.version)
        print "PID: %s" % (dev_info.pid)
        print "Started Date: %s" % (dev_info.started_date)
        print "Stopped Date: %s" % (dev_info.stopped_date)
    else:
        # Define Tile device name
        new_device_name = device_name
        # Define the Tango Class served by this DServer
        dev_info = DbDevInfo()
        dev_info._class = "ObservationConfiguration"
        dev_info.server = "ObservationConfiguration/test"
        # add the device
        dev_info.name = new_device_name
        print("Creating device: %s" % new_device_name)
        db.add_device(dev_info)



#formulaconfhandler_device_info(1)
#formulaconf_device_info(1)
#dummy_device_info(1)
alarm_device_info(1)
alarm_stream_device_info(1)
event_device_info(1)
obsconf_device_info(1)
tile_device_info(1)
tpm_device_info(1)
#fpga_device_info(1)

print "----------------------------------------------------------------------------------------------------------------"