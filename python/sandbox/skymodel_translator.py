import json
import re


class SkyModelTranslator(object):

    """
    This class represents a translator/builder for the JSON sky model format.

    The format of our JSON sky model is as follows:

    {
      "skymodel":{
        "metadata":{
          "fields": [
            {"name": "ra",
              "description": "desc ra",
              "units": "deg"
            },
            {"name": "dec",
              "description": "desc dec",
              "units": "m"
            }
            ]
        },
        "data":[
          [1, 0],
          [2, 0.5]
          ]
      }
    }


    """

    @classmethod
    def build(cls):
        pass

    @classmethod
    def translate(cls, json_skymodel):
        """
        Accepts a JSON skymodel, retrieves metadata and entries
        :param json_skymodel:
        :return: metadata, sources
            (list of metadata per attribute)
            (list of sources data)
        """
        metadata=[]
        sources=[]

        skymodel_dict = json.loads(json_skymodel)
        for metafield in skymodel_dict["skymodel"]["metadata"]["fields"]:
            field_name = str(metafield.get("name"))
            field_desc = str(metafield.get("description"))
            field_unit = str(metafield.get("units"))
            metadata.append({"name": field_name, "description": field_desc, "unit": field_unit})

        for source in skymodel_dict["skymodel"]["data"]:
            sources.append(source)

        argout = metadata, sources
        return argout

def main():
    json_data = open("skymodel.json").read()
    metadata, sources = SkyModelTranslator.translate(json_data)
    print metadata
    print sources

if __name__ == '__main__':
    main()