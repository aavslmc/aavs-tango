import socket
import os
import io

class SCPIDevice(object):
    """ SCPI Device for monitoring and controlling hardware through the SCPI protocol """

    def __init__(self, ip, port=2020):
        """ Class constructor """
        self._ip = ip
        self._port = port

        self._socket = None

        # Connect to device
        self._connect()

    def _connect(self):
        """ Connect to device """

        # Create an INET, STREAMing socket
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print "SCPIDevice: Failed to create socket"
            return -1

        self._socket.connect((self._ip, self._port))
        print "Socket Connected to %s" % self._ip



class POW_SUPPLY():
    """ Power Supply Class """
    ### init Function
    def __init__(self, **kwargs):
        self.use_cfg = kwargs.get("usecfg", None)
        self.mu_status= None
        self.mu_connection_type=None
        self.mu_device=None
        self.vendor=None
        self.id=None
        self.sn=None
        self.channel=None
        self.hostname=None
	def get_configuration(config_filename):
		f= open(config_filename,'r')
        lines = f.read().splitlines()
        self.vendor=lines[0].split('=')[1]
        self.id=lines[1].split('=')[1]
        self.sn=lines[2].split('=')[1]
        self.channel=lines[3].split('=')[1]
        self.hostname=self.id+self.sn
    ###connect function
    #This function create a tcp socket with the instrument
    #@param[in] ip: ipaddress of the instruments
    #@param[in] port: port value for instruments eth connection
    def connect(self, **kwargs):
        ip = kwargs.get('ip', None)
        port = kwargs.get('port', None)
        #create an INET, STREAMing socket
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print 'Failed to create socket'
            return -1
        print 'Socket Created'
        host = self.hostname
        try:
            remote_ip = socket.gethostbyname( host )
        except socket.gaierror:
            #could not resolve
            print 'Hostname could not be resolved. Exiting'
            return -1
        #Connect to remote server
        print(remote_ip)
        port=10000
        self.s.connect((remote_ip,port))
        print 'Socket Connected to ' + host + ' on ip ' + remote_ip
        return 0
    ### send_cmd
	#This function send a command on the socket connection established with instrument
    #@param[in] cmd: SCPI cmd to send
    #@return operation status, 0 passed -1 failed
    def send_cmd(self,cmd):
        try :
            #Set the whole string
            self.s.send(cmd)
        except socket.error:
            #Send failed
            print 'Send failed'
            return -1
    ###get_voltage
	#this function read the voltage value of the selected channel
	#@param[in] channel: power supply channel to be used
	#@return res: voltage value
	#@return opstat:operation status, -1 failed res value not valid, 0 passed
    def get_voltage(self,channel):
        if((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if(self.send_cmd(cmd) != -1):
                time.sleep(2)
                cmd="VOLT?\n"
                if (self.send_cmd(cmd)!=-1):
                    time.sleep(4)
                    res = self.s.recv(256)
                    f=float(res)
                    opstat = 0
                else:
                    f = -1
                    opstat = -1
            else:
                f = -1
                opstat = -1
        else:
            f = -1
            opstat = -2
        return f, opstat
    ###get_curr
	# this function read the current value of the selected channel
	#@param[in] channel: power supply channel to be used
	#@return res: current value
	#@return opstat:operation status, -1 failed res value not valid, 0 passed
    def get_current(self,channel):
        if ((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if (self.send_cmd(cmd)!=-1):
                time.sleep(2)
                cmd="CURR?\n"
                if (self.send_cmd(cmd)!=-1):
                    time.sleep(4)
                    res = self.s.recv(256)
                    f=float(res)
                    opstat = 0
                else:
                    f = -1
                    opstat = -1
            else:
                f = -1
                opstat = -1
        else:
            f = -1
            opstat = -2
        return f, opstat

    ###set_curr
	#this function set the current value of the selected channel
	#@param[in] channel: power supply channel to be used
	#@return opstat:operation status, -1 failed cmd not send, -2 failed bad param, 0 passed
    def set_current(self,channel,current):
        if ((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if (self.send_cmd(cmd)!=-1):
                time.sleep(2)
                cmd="CURR "+ current + "\n"
                o=self.send_cmd(cmd)
                time.sleep(2)
            else:
                return -1
        else:
            return -2

    ###set_voltage
	#this function set the voltage value of the selected channel
	#@param[in] channel: power supply channel to be used
	#@return opstat:operation status, -1 failed cmd not send, -2 failed bad param, 0 passed
    def set_voltage(self,channel,current):
        if ((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if (self.send_cmd(cmd)!=-1):
                time.sleep(2)
                cmd="VOLT "+ current + "\n"
                o=self.send_cmd(cmd)
                time.sleep(2)
            else:
                return -1
        else:
            return -2
    ###output_on
	#this function enable the output of selected channel
	#@param[in] channel: power supply channel to be used
	#@return opstat:operation status, -1 failed cmd not send, -2 failed bad param, 0 passed
    def output_on(self,channel):
        if ((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if (self.send_cmd(cmd)!=-1):
                time.sleep(2)
                cmd="OUTP ON\n"
                o=self.send_cmd(cmd)
                time.sleep(2)
                return o
            else:
                return -1
        else:
            return -2
    ###output_off
	#this function disable the output of selected channel
	#@param[in] channel: power supply channel to be used
	#@return opstat:operation status, -1 failed cmd not send, -2 failed bad param, 0 passed
    def output_off(self,channel):
        if ((channel > 0) & (channel < self.channel)):
            cmd="INST OUT"+ channel +"\n"
            if (self.send_cmd(cmd)!=-1):
                time.sleep(2)
                cmd="OUTP OFF\n"
                o=self.send_cmd(cmd)
                time.sleep(2)
                return o
            else:
                return -1
        else:
            return -2
    ###output_gen_on
	#this function enable the general output
	#@return opstat:operation status, -1 failed cmd not send, 0 passed
    def output_gen_on(self):
        cmd="OUTP:GEN ON\n"
        o=self.send_cmd(cmd)
        time.sleep(2)
        return o
    ###output_gen_off
	#this function disable the general output
	#@return opstat:operation status, -1 failed cmd not send, 0 passed
    def output__grn_off(self,channel):
        cmd="OUTP:GEN OFF\n"
        return self.send_cmd(cmd)
    ###get_id
	#this function return the id informations of the device
	#@return opstat:operation status, -1 failed cmd not send, 0 passed
    def get_id(self):
        cmd="*IDN?\n"
        if (self.send_cmd(cmd)!=-1):
            res = self.s.recv(256)
            opstat = 0
        else:
            res = -1
            opstat = -1
        return res, opstat

    ###disconnect
	# this function close the tcp connection with instruments
    def disconnect(self):
        self.s.close()



#INSTrument[:SELect] {OUTPut1|OUTPut2|OUTPut3|OUTPut4|OUT1|OUT2|OUT3|OUT4}
#INSTrument[:SELect]?
#INSTrument:NSELect {1|2|3|4}
#INSTrument:NSELect?

#[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude] {<voltage>| MIN | MAX}}
#[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude]? [MIN I MAX]
#[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude] {UP I DOWN}
#[SOURce:]VOLTage[:LEVel]:STEP[:INCRement) {<numeric value>| DEFault}
#[SOURce:]VOLTage[:LEVel]:STEP[:INCRement)? [Default)


#[SOURce:]CURRent[:LEVel][:IMMediate][:AMPLitude] {<current>| MIN | MAX}
#[SOURce:]CURRent[:LEVel][:IMMediate][:AMPLitude]? [MIN I MAX]
#[SOURce:]CURRent[:LEVel][:IMMediate][:AMPLitude] {UP I DOWN}
#[SOURce:]CURRent[:LEVel]:STEP[:INCRement) {<numeric value>| DEFault}
#[SOURce:]CURRent[:LEVel]:STEP[:INCRement)? [Default)

#OUTPut:SELect {OFF | ON | 0 | 1}
#OUTPut[:STATe] {OFF | ON | 0 | 1}
#OUTPut[:STATe]?
#OUTPut:GENeral {OFF | ON | 0 | 1}

#VOLTage:PROTection[:LEVel] {<voltage> | MIN | MAX }
#VOLTage:PROTection[:LEVel]? [MIN | MAX]
#VOLTage:PROTection:TRIPped?
#VOLTage:PROTection:CLEar
#VOLTage:PROTection:MODE {MEASured | PROTected}
#VOLTage:PROTection:MODE?

#MEASure[:SCALar]:CURRent [:DC]?
#MEASure[:SCALar][:VOLTage] [:DC]?
