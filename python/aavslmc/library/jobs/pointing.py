import PyTango
import datetime
import json
import jsonpickle
import pickle
import numpy as np
from aavslmc.library.utils import EnumEncoder, as_enum


from astropy import units as u
from astropy.coordinates import Angle, AltAz, SkyCoord, EarthLocation
from astropy.units import Quantity
from astropy.time.core import Time
from astropy import constants
# Mute astropy warning
import warnings
from astropy.utils.exceptions import AstropyWarning
warnings.simplefilter('ignore', category=AstropyWarning)

from aavslmc.library.utils import AntennaArray


def _run_proxy_command(proxy, cmd_name, **kwargs):
    if kwargs:
        cmd_args = json.dumps(kwargs, cls=EnumEncoder)
        return proxy.command_inout(cmd_name, cmd_args)
    else:
        return proxy.command_inout(cmd_name)

class PointingBase(object):

    def __init__(self, data=None):
        self.data = data

    def point(self):
        #This is to be implemented by subclass
        #Called by tango device on receiving call with station ID
        raise NotImplementedError

class DataLayerBase(object):

    def n_antennas(self):
        raise NotImplementedError

    def n_channels(self):
        raise NotImplementedError

    def n_pols(self):
        raise NotImplementedError

    def n_beams(self):
        raise NotImplementedError

    def bandwidth(self):
        raise NotImplementedError

    def fch1(self):
        raise NotImplementedError

    def polarisation_fpga_map(self):
        raise NotImplementedError

    def get_tile_ids(self):
        """ Returns tile ids for this station """
        raise NotImplementedError

    def get_tile_serials(self):
        """ Returns tile serials for tile ids for this station """
        raise NotImplementedError

    def get_tile_arrays(self):
        """ Returns antenna array locations for tiles in this station """
        raise NotImplementedError

    def write_weights_to_tile(self, tile_serial, weights, antenna, index):
        """ Writes beamformer coefficients to tile """
        raise NotImplementedError

    def synchronised_beamformer_coefficients(self, tile_serial, timestamp, seconds):
        """ Call to synchronise previously downloaded coefficients """
        raise NotImplementedError

class TangoDataLayer(DataLayerBase):

    def __init__(self, domain="test", station_name=None):
        self.domain = domain
        self.obs_conf = PyTango.DeviceProxy(domain+"/obsconf/main")
        self.station_device_name = station_name

    def n_antennas(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_antennas = self.obs_conf.tm_n_antennas
        return n_antennas

    def n_channels(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_channels = self.obs_conf.tm_n_channels
        return n_channels

    def n_pols(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_pols = self.obs_conf.tm_n_pols
        return n_pols

    def n_beams(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_beams = self.obs_conf.tm_n_beams
        return n_beams

    def bandwidth(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        bandwidth = self.obs_conf.tm_bandwidth
        return bandwidth

    def fch1(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        fch1 = self.obs_conf.tm_fch1
        return fch1

    def polarisation_fpga_map(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        polarisation_fpga_map = self.obs_conf.tm_polarisation_fpga_map
        return json.loads(polarisation_fpga_map)

    def get_tile_ids(self):
        """ Returns tile ids for this station """
        station_dp = PyTango.DeviceProxy(self.station_device_name)
        tiles_in_station = json.loads(station_dp.command_inout("get_member_names", json.dumps({"component_type": "tiles"})))
        tile_ids = tiles_in_station["member_device_names"]
        return tile_ids

    def get_tile_serials(self):
        """ Returns tile serials for tile ids for this station """
        tile_ids = self.get_tile_ids()
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")

        tile_serials = [_run_proxy_command(proxy=self.obs_conf, cmd_name="tm_get_tile_serial_for_tile_address", tile_address = tile_id) for tile_id in tile_ids]
        return tile_serials

    def get_tile_arrays(self):
        """ Returns antenna array locations for tiles in this station """
        tile_serials = self.get_tile_serials()

        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        #all_arrays = jsonpickle.decode(self.obs_conf.tm_tiles, keys=True)
        all_arrays = pickle.loads(self.obs_conf.tm_tiles)

        station_arrays = {}
        for tile_serial in tile_serials:
            station_arrays[tile_serial] = all_arrays[tile_serial]

        return station_arrays

    def write_weights_to_tile(self, tile_serial, weights, antenna, index):
        """ Writes beamformer coefficients to tile """

        # Get tile address for tile serial
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        tile_address = _run_proxy_command(proxy=self.obs_conf, cmd_name="tm_get_tile_address_for_tile_serial",tile_serial=tile_serial)

        # Get proxy to tile
        self.tile_proxy = PyTango.DeviceProxy(tile_address)

        # Download weights to tile
        _run_proxy_command(proxy=self.tile_proxy,cmd_name="download_weights",weights=weights,antenna=antenna,index=index)
        # self._tile.tpm.tpm_test_firmware[self._pol_fpga_map[pol]].download_beamforming_weights(list(values), antenna)

    def synchronised_beamformer_coefficients(self, tile_serial, timestamp, seconds):
        """ Call to synchronise previously downloaded coefficients """

        # Get tile address for tile serial
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        tile_address = _run_proxy_command(proxy=self.obs_conf, cmd_name="tm_get_tile_address_for_tile_serial",
                                          tile_serial=tile_serial)

        # Get proxy to tile
        self.tile_proxy = PyTango.DeviceProxy(tile_address)

        _run_proxy_command(proxy=self.tile_proxy, cmd_name="synchronised_beamformer_coefficients", timestamp=timestamp, seconds=seconds)
        #self._tile.synchronised_beamformer_coefficients(timestamp=timestamp, seconds=delay)

class AAVSPointing(PointingBase):
    """
    AAVSPointing can be replaced by arbitrary pointing implementations.

    Each implementation should implement the same interface, as specified by PointingBase.
    """

    def __init__(self, *args, **kwargs ):
        super(AAVSPointing, self).__init__(*args,**kwargs)

    def point(self):
        print "Pointing baby!"
        self.station_tile_arrays = self.data.get_tile_arrays()

        self._nof_antennas = self.data.n_antennas()
        self._nof_channels = self.data.n_channels()
        self._nof_pols = self.data.n_pols()
        self._nof_beams = self.data.n_beams()
        self._bandwidth = self.data.bandwidth()
        self._fch1 = self.data.fch1()
        self._pol_fpga_map = self.data.polarisation_fpga_map()

        self.tile_weights = {}

        self.tile_baseline_vectors_ecef = {}
        self.tile_baseline_vectors_enu = {}

        # Get displacement vectors to each antenna from reference antenna
        for tile_serial, array in self.station_tile_arrays.iteritems():
            if array is not None:
                self._array = array
                self.tile_baseline_vectors_ecef[tile_serial] = self._calculate_ecef_baseline_vectors()
                self.tile_baseline_vectors_enu[tile_serial] = self._rotate_ecef_to_enu(self.tile_baseline_vectors_ecef[tile_serial],self._array.lat,self._array.lon)

        # Generate empty weights
        self.generate_empty_weights(ones=True)

        # Mask channels
        self.mask_antennas_channels(antennas=[2, 4, 6])

        # Download weights
        self.download_weights()

    # Station-based operations #

    def download_weights(self, timestamp=None, delay=0.1):
        for tile_serial in self.station_tile_arrays:
            self._download_weights(tile_serial=tile_serial, timestamp=timestamp, delay=delay)

    def generate_empty_weights(self, ones=False):
        for tile_serial in self.station_tile_arrays:
            self._generate_empty_weights(tile_serial=tile_serial, ones=ones)

    def set_coefficient(self, antennas=None, channels=None, polarisations=None, value=(1,1)):
        for tile_serial in self.station_tile_arrays:
            self._set_coefficient(tile_serial=tile_serial, antennas=antennas, channels=channels, polarisations=polarisations, value=value)

    def set_antennas_channels(self, antennas=None, channels=None, polarisation=None):
        for tile_serial in self.station_tile_arrays:
            self._set_antennas_channels(tile_serial=tile_serial, antennas=antennas, channels=channels, polarisations=polarisation)

    def mask_antennas_channels(self, antennas=None, channels=None, polarisation=None):
        for tile_serial in self.station_tile_arrays:
            self._mask_antennas_channels(tile_serial=tile_serial, antennas=antennas, channels=channels, polarisations=polarisation)

    def save_to_file(self, pathname):
        for tile_serial in self.station_tile_arrays:
            self._save_to_file(tile_serial=tile_serial, pathname=pathname)

    # Tile-based operations #

    def _download_weights(self, tile_serial, timestamp=None, delay=0.1):
        """ Set weights provided externally.
        :param delay: Delay to application of coefficients """

        # Weights must be a numpy multi-dimensional array
        if type(self.tile_weights[tile_serial]) is not np.ndarray:
            raise Exception("Weights parameter to set wieghts must be a numpy array")

        # Weights shape must match beamformer parameters
        if np.shape(self.tile_weights[tile_serial]) != (self._nof_pols, self._nof_antennas, self._nof_channels, 2):
            raise Exception("Invalid weights shape")

        # Loop over polarisations
        for pol in range(self._nof_pols):
            # Loop over antennas
            for antenna in range(self._nof_antennas):

                # Convert weights to 8-bit values, reverse array and combine every 4 values into 32-bit words
                coeffs = self.tile_weights[tile_serial][pol, antenna, :, :].flatten()
                coeffs = self._convert_weights(coeffs)
                values = np.zeros(len(coeffs) / 4, dtype='uint32')
                for index, i in enumerate(range(0, len(coeffs), 4)):
                    value = coeffs[i + 0] | \
                            (coeffs[i + 1] << 8) | \
                            (coeffs[i + 2] << 16) | \
                            (coeffs[i + 3] << 24)
                    values[index] = value

                self.data.write_weights_to_tile(tile_serial=tile_serial, weights=values.tolist(), antenna=antenna, index=self._pol_fpga_map[str(pol)])
                # self._tile.tpm.tpm_test_firmware[self._pol_fpga_map[pol]].download_beamforming_weights(list(values),
                #                                                                                        antenna)

        # Apply coefficients
        self.data.synchronised_beamformer_coefficients(tile_serial=tile_serial, timestamp=timestamp, seconds=delay)

    def _convert_weights(self, coeffs):
        """ Convert weights from float (assuming range is between -1 and 1) to unsigned 8-bit
        :param coeffs: Coefficients to convert
        :return: Converted weight matrix
        """
        return np.array(np.array(coeffs * 127, dtype='int8'), dtype='uint8')

    def _generate_empty_weights(self, tile_serial, ones=False):
        """ Generate a matrix containing only ones or zeros
        :param ones: Fill matrix with ones, if False fill it with zeros
        :return: Return a weight matrix containing only ones or zeros
        """
        if ones:
            self.tile_weights[tile_serial] = np.ones((self._nof_pols, self._nof_antennas, self._nof_channels, 2))
            self.tile_weights[tile_serial][:, :, :, 1] = 0
        else:
            self.tile_weights[tile_serial] = np.zeros((self._nof_pols, self._nof_antennas, self._nof_channels, 2))

    def _set_antennas_channels(self, tile_serial, antennas=None, channels=None, polarisations=None):
        """ Generate beamforming coefficients to set particular channels and antennas, for particular polarisations
        :param antennas: Antennas to set
        :param channels: Channels to set
        :param polarisations: Polarisation to which set is applied
        :return: Weight matrix
        """
        self._set_coefficient(tile_serial=tile_serial,
                              antennas=antennas,
                              channels=channels,
                              polarisations=polarisations,
                              value=(1, 0))

    def _mask_antennas_channels(self, tile_serial, antennas=None, channels=None, polarisations=None):
        """ Generate beamforming coefficients to mask particular channels and antennas, for particular polarisations
        :param antennas: Antennas to mask
        :param channels: Channels to mask
        :param polarisations: Polarisation to which mask is applied
        :return: Weight matrix
        """
        self._set_coefficient(tile_serial=tile_serial,
                              antennas=antennas,
                              channels=channels,
                              polarisations=polarisations,
                              value=(0, 0))

    def _set_coefficient(self, tile_serial, antennas=None, channels=None, polarisations=None, value=(1, 1)):
        """ Generate beamforming coefficients to apply value to particular channels and antennas, for particular polarisations
        :param antennas: Antennas to apply value
        :param channels: Channels to apply value
        :param polarisations: Polarisation to which value is applied
        :param value: value to set
        :return: Weight matrix
        """

        # Value components have to be between -1 and 1
        if not (-1 <= value[0] <= 1 and -1 <= value[1] <= 1):
            print "Value components must be between -1 and 1 (inclusive)"
            return False

        # Check if weights has been created, if not generate empty weights
        if self.tile_weights[tile_serial] is None:
            self._generate_empty_weights(tile_serial=tile_serial)

        # Check if polarisation is specified, otherwise apply to all
        pols = range(self._nof_pols) if polarisations is None \
            else [polarisations] if type(polarisations) is not list else polarisations

        # Check antenna and channel selection
        if antennas is None:
            antennas = range(self._nof_antennas)
        if channels is None:
            channels = range(self._nof_channels)

        # Optimised for all channel masking
        if channels is None:
            ants = range(self._nof_antennas) if antennas is None \
                else [antennas] if type(antennas) is not list else antennas

            # Apply masking
            for p in pols:
                for a in ants:
                    self.tile_weights[tile_serial][p, a, :, :] = np.zeros((self._nof_channels, 2)) + value

            return True

        # Optimised for all antenna masking
        if antennas is None:
            chans = range(self._nof_channels) if channels is None \
                else [channels] if type(channels) is not list else channels

            # Apply masking
            for p in pols:
                for c in chans:
                    self.tile_weights[tile_serial][p, :, c, :] = np.zeros((self._nof_antennas, 2)) + value

            return True

        # Convert antennas and channels to lists
        ants = range(self._nof_antennas) if antennas is None \
            else [antennas] if type(antennas) is not list else antennas
        chans = range(self._nof_channels) if channels is None \
            else [channels] if type(channels) is not list else channels

        # Apply masking
        for p in pols:
            for a in ants:
                for c in chans:
                    self.tile_weights[tile_serial][p, a, c, :] = value

        # All done, return
        return True

    def _save_to_file(self, tile_serial, pathname):
        """ Save the generated matrix to file
        :param pathname: Pathname for file
        """
        try:
            with open(pathname, "wb") as f:
                np.save(pathname, self.tile_weights[tile_serial])
            print "Beamforming weights saved to %s" % pathname
        except IOError as e:
            print "Could not save beamforming weights to file (%s)" % e.strerror

    # POINTING FUNCTIONS #
    def _point_array_static(self, tile_serial, altitude, azimuth):
        """ Calculate the phase shift given the altitude and azimuth coordinates of a sky object as astropy angles
        :param altitude: altitude coordinates of a sky object as astropy angle
        :param azimuth: azimuth coordinates of a sky object as astropy angles
        :return: The phase shift in radians for each antenna
        """

        # Type conversions if required
        if type(altitude) is not Angle:
            if type(altitude) is str:
                altitude = Angle(altitude)
            else:
                altitude = Angle(altitude, u.deg)

        if type(azimuth) is not Angle:
            if type(azimuth) is str:
                azimuth = Angle(azimuth)
            else:
                azimuth = Angle(azimuth, u.deg)

        # Check if beamformer weights have been initialised
        if self.tile_weights[tile_serial] is None:
            self._generate_empty_weights(tile_serial=tile_serial)

        # Calculate East-North-Up vector
        vectors_enu = self._rotate_ecef_to_enu(self._baseline_vectors_ecef, self._array.lat, self._array.lon)

        for i in xrange(self._nof_channels):
            # Calculate complex coefficients
            frequency = Quantity(self._start_channel_frequency + (i * self._bandwidth / self._nof_channels),
                                 u.Hz)
            real, imag = self._phaseshifts_from_altitude_azimuth(altitude.rad, azimuth.rad, frequency,
                                                                 vectors_enu)

            # Apply to weights file
            self.tile_weights[tile_serial][:, :, i, 0] = real
            self.tile_weights[tile_serial][:, :, i, 1] = imag

    def _point_array(self, right_ascension, declination, pointing_time=None):
        """ Calculate the phase shift between two antennas which is given by the phase constant (2 * pi / wavelength)
        multiplied by the projection of the baseline vector onto the plane wave arrival vector
        :param right_ascension: Right ascension of source (astropy angle, or string that can be converted to angle)
        :param declination: Declination of source (astropy angle, or string that can be converted to angle)
        :param pointing_time: Time of observation (in format astropy time)
        :return: The phaseshift in radians for each antenna
        """

        if pointing_time is None:
            pointing_time = Time(datetime.datetime.utcnow(), scale='utc')

        reference_antenna_loc = EarthLocation.from_geodetic(self._array.lon, self._array.lat,
                                                            height=self._array.height,
                                                            ellipsoid='WGS84')
        alt, az = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination),
                                         Time(pointing_time), reference_antenna_loc)
        self.point_array_static(alt, az)

    def _calculate_ecef_baseline_vectors(self):
        """ Calculate the displacement vectors in metres for each antenna relative to master antenna.
        :return: an (n, 3) array of displacement vectors for each antenna
        """
        displacement_vectors = np.full([self._nof_antennas, 3], np.nan)
        array_positions = self._array.positions()

        ref_loc = EarthLocation.from_geodetic(lon=self._array.lon, lat=self._array.lat,
                                              height=self._array.height, ellipsoid='WGS84')
        ant_ref_pos = ref_loc.to_geocentric()
        ant_ref_values = np.array(
            [ant_ref_pos[0].to(u.m).value, ant_ref_pos[1].to(u.m).value, ant_ref_pos[2].to(u.m).value])

        for i in range(0, self._nof_antennas):
            ant_i_pos = (array_positions[0][i], array_positions[1][i], array_positions[2][i])
            ant_i_pos_values = np.array(
                [ant_i_pos[0].to(u.m).value, ant_i_pos[1].to(u.m).value, ant_i_pos[2].to(u.m).value])
            displacement_vectors[i, :] = ant_i_pos_values - ant_ref_values

        return displacement_vectors

    def _rotate_ecef_to_enu(self, ecef_vectors, latitude, longitude):
        """
        Rotates a set of vectors expressed in the geocentric Earth-Centred Earth-Fixed coordinate system to the local
        East-North-Up surface tangential (horizontal) coordinate system with the specified latitude, longitude.
        :param ecef_vectors: (n, 3) array of vectors expressed in ECEF
        :param latitude: Geodectic latitude (astropy angle)
        :param longitude: Geodectic longitude (astropy angle)
        :return: array containing vectors rotated to East North Up coordinate system
        """

        rotation_matrix = self._rotation_matrix_ecef_to_enu(latitude, longitude)
        vectors_enu = np.dot(rotation_matrix, np.array(ecef_vectors).transpose())
        return vectors_enu.transpose()

    @staticmethod
    def _ra_dec_to_alt_az(right_ascension, declination, time, location):
        """ Calculate the altitude and azimuth coordinates of a sky object from right ascension and declination and time
        :param right_ascension: Right ascension of source (in astropy Angle on string which can be converted to Angle)
        :param declination: Declination of source (in astropy Angle on string which can be converted to Angle)
        :param time: Time of observation (as astropy Time")
        :param location: astropy EarthLocation
        :return: Array containing altitude and azimuth of source as astropy angle
        """

        # Initialise SkyCoord object using the default frame (ICRS) and convert to horizontal
        # coordinates (altitude/azimuth) from the antenna's perspective.
        sky_coordinates = SkyCoord(ra=right_ascension, dec=declination)
        c_altaz = sky_coordinates.transform_to(AltAz(obstime=time, location=location))

        return c_altaz.alt, c_altaz.az

    @staticmethod
    def _rotation_matrix_ecef_to_enu(latitude, longitude):
        """
        Compute a rotation matrix for converting vectors in ECEF to local
        ENU (horizontal) cartesian coordinates at the specified latitude, longitude.
        :param latitude: The geodetic latitude of the reference point
        :param longitude: The geodetic longitude of the reference point
        :return: A 3*3 rotation (direction cosine) matrix.
        """
        sin_lat = np.sin(latitude)
        cos_lat = np.cos(latitude)
        sin_long = np.sin(longitude)
        cos_long = np.cos(longitude)

        rotation_matrix = np.zeros((3, 3), dtype=float)
        rotation_matrix[0, 0] = -sin_long
        rotation_matrix[0, 1] = cos_long
        rotation_matrix[0, 2] = 0

        rotation_matrix[1, 0] = -sin_lat * cos_long
        rotation_matrix[1, 1] = -sin_lat * sin_long
        rotation_matrix[1, 2] = cos_lat

        rotation_matrix[2, 0] = cos_lat * cos_long
        rotation_matrix[2, 1] = cos_lat * sin_long
        rotation_matrix[2, 2] = sin_lat

        return rotation_matrix

    @staticmethod
    def _phaseshifts_from_altitude_azimuth(altitude, azimuth, frequency, displacements):
        """
        Calculate the phaseshift using a target altitude Azimuth
        :param altitude: The altitude of the target astropy angle
        :param azimuth: The azimuth of the target astropy angle
        :param frequency: The frequency of the observation as astropy quantity.
        :param displacements: Numpy array: The displacement vectors between the antennae in East, North, Up
        :return: The phaseshift angles in radians
        """
        scale = np.array([-np.sin(azimuth) * np.cos(altitude),
                          -np.cos(azimuth) * np.cos(altitude),
                          -np.sin(altitude)])

        path_length = np.dot(scale, displacements.transpose())

        k = (2.0 * np.pi * frequency.to(u.Hz).value) / constants.c.value
        t = np.multiply(k, path_length)
        return np.cos(t), np.sin(t)

    def _is_above_horizon(self, right_ascension, declination, pointing_time):
        """
        Determine whether the target is above the horizon, at the specified time for the reference antenna.
        :param right_ascension: The right ascension of the target as a astropy angle
        :param declination: The declination of the target as an astropy angle.
        :param pointing_time: The observation time as an astropy Time.
        :return: True if the target coordinates are above the horizon at the specified time, false otherwise.
        """
        reference_antenna_loc = EarthLocation.from_geodetic(self._array.lon, self._array.lat,
                                                            height=self._array.height,
                                                            ellipsoid='WGS84')
        alt, az = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination), Time(pointing_time),
                                         reference_antenna_loc)

        return alt > 0.0