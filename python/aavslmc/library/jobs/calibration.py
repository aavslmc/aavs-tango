import PyTango
import json


class CalibrationBase(object):


    def __init__(self, data=None):
        self.data = data

    def calibrate(self, filename):
        #This is to be implemented by subclass
        #Called by tango device on receiving call with file
        raise NotImplementedError


class DataLayerBase(object):

    def set_coefficients(self, coefficients):
        #Called by calibration at the end for the device to set the coefficients
        raise NotImplementedError

    def get_antenna_locations(self):
        raise NotImplementedError

    def get_sky_model(self):
        raise NotImplementedError

    def get_tile_ids(self):
        raise NotImplementedError

class TangoDataLayer(DataLayerBase):

    def __init__(self, domain="test", station_name=None):
        self.domain = "test"
        self.obs_conf = PyTango.DeviceProxy(domain+"/obsconf/main")
        self.station_device_name = station_name


    def set_coefficients(self, coefficients):
        print coefficients

    def get_antenna_locations(self):
        return json.loads(self.obs_conf.tm_antenna_locations)

    def get_sky_model(self):
        return  json.loads(self.obs_conf.sm_sky_model)

    def get_tile_ids(self):
        station_dp = PyTango.DeviceProxy(self.station_device_name)
        tiles  = station_dp.tiles
        tile_ids = [tile.split("/")[2] for tile in tiles]
        return tile_ids


class MockDataLayer(DataLayerBase):

    def set_coefficients(self, coefficients):
        print coefficients

    def get_antenna_locations(self):
        return {u'telescope_model': {u'antennas': [{u'altitude': 0.0,
                                                    u'id': u'1',
                                                    u'latitude': 11.0,
                                                    u'longitude': 12.0,
                                                    u'tile_serial': u'1'},
                                                   {u'altitude': 0.0,
                                                    u'id': u'34',
                                                    u'latitude': 31.0,
                                                    u'longitude': 12.0,
                                                    u'tile_serial': u'34'}]}}

    def get_sky_model(self):

        return {u'skymodel': {u'data': [[23.39,
                              58.815,
                              12800.0,
                              0.0,
                              0.0,
                              0.0,
                              152000000.0,
                              0.77],
                             [23.39, 58.815, 12800.0, 0.0, 0.0, 0.0, 152000000.0, 0.77],
                             [23.39, 58.815, 12800.0, 0.0, 0.0, 0.0, 152000000.0, 0.77]],
                   u'metadata': {u'fields': [{u'description': u'Right Ascension',
                                              u'name': u'ra',
                                              u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'},
                                             {u'description': u'Right Ascension', u'name': u'ra', u'units': u'deg'}]}}}


    def get_tile_ids(self):
        return  ["1","4","5"]



class MockCalibration(CalibrationBase):

    def __init__(self, *args, **kwargs ):
        super(MockCalibration, self).__init__(*args,**kwargs)


    def calibrate(self, filename):
        self.data.get_telescope_model()
        self.data.set_coefficients("asd")
        print "Calibrating baby!"



