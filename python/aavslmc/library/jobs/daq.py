import PyTango
import json
import pickle
from aavslmc.library.utils import EnumEncoder, as_enum, JobTasks, decode_task_type
import pydaq.daq_receiver as daq
import logging

def _run_proxy_command(proxy, cmd_name, async = False, **kwargs):
    if kwargs:
        cmd_args = json.dumps(kwargs, cls=EnumEncoder)
        if async:
            return proxy.command_inout_asynch(cmd_name, cmd_args)
        else:
            return proxy.command_inout(cmd_name, cmd_args)
    else:
        if async:
            return proxy.command_inout_asynch(cmd_name)
        else:
            return proxy.command_inout(cmd_name)

class DaqBase(object):

    def __init__(self, data=None, daq_id=None):
        self.data = data
        self.daq_id = daq_id

    def start(self):
        #This is to be implemented by subclass
        #Called by tango device on receiving call with station ID
        raise NotImplementedError

    def stop(self):
        #This is to be implemented by subclass
        #Called by tango device on receiving call with station ID
        raise NotImplementedError

    def terminate(self):
        raise NotImplementedError

class DataLayerBase(object):

    def daq_type(self):
        raise NotImplementedError

    def n_antennas(self):
        raise NotImplementedError

    def n_channels(self):
        raise NotImplementedError

    def n_pols(self):
        raise NotImplementedError

    def n_beams(self):
        raise NotImplementedError

    def update_station_tasks(self, task_type):
        raise NotImplementedError

    def get_tile_ids(self):
        """ Returns tile ids for this station """
        raise NotImplementedError

    def get_tile_serials(self):
        """ Returns tile serials for tile ids for this station """
        raise NotImplementedError

    def get_tile_arrays(self):
        """ Returns antenna array locations for tiles in this station """
        raise NotImplementedError

    def get_owner(self):
        """ Returns proxy id of device which owns this process """
        raise NotImplementedError

class TangoDataLayer(DataLayerBase):

    def __init__(self, domain="test", station_name=None):
        self.domain = "test"
        self.obs_conf = PyTango.DeviceProxy(domain+"/obsconf/main")
        self.station_device_name = station_name

        # Set logging
        log = logging.getLogger('')
        log.setLevel(logging.INFO)
        line_format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        # ch = logging.FileHandler('/home/andrea/daqlogger.log')
        # ch.setFormatter(line_format)
        # log.addHandler(ch)

    def get_owner(self):
        return self.station_device_name

    def n_antennas(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_antennas = self.obs_conf.tm_n_antennas
        return n_antennas

    def n_channels(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_channels = self.obs_conf.tm_n_channels
        return n_channels

    def n_pols(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_pols = self.obs_conf.tm_n_pols
        return n_pols

    def update_station_tasks(self, task_type):
        self.task_log("[daq.py] In update_station_tasks()")
        station = PyTango.DeviceProxy(self.station_device_name)

        """ extend timeout for calls """
        station.set_timeout_millis(100000)
        self.task_log("[daq.py] In update_station_tasks() -- Running proxy command to update progress")
        _run_proxy_command(async=False, proxy=station, cmd_name="update_daq_task_progress", task_type=task_type)
        self.task_log("[daq.py] In update_station_tasks() -- Returning from proxy command to update progress")

    def n_beams(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        n_beams = self.obs_conf.tm_n_beams
        return n_beams

    def get_tile_ids(self):
        """ Returns tile ids for this station """
        station_dp = PyTango.DeviceProxy(self.station_device_name)
        tiles_in_station = json.loads(
            station_dp.command_inout("get_member_names", json.dumps({"component_type": "tiles"})))
        tile_ids = tiles_in_station["member_device_names"]
        return tile_ids

    def get_tile_serials(self):
        """ Returns tile serials for tile ids for this station """
        tile_ids = self.get_tile_ids()
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")

        tile_serials = [_run_proxy_command(proxy=self.obs_conf, cmd_name="tm_get_tile_serial_for_tile_address",
                                           tile_address=tile_id) for tile_id in tile_ids]
        return tile_serials

    def get_tile_arrays(self):
        """ Returns antenna array locations for tiles in this station """
        tile_serials = self.get_tile_serials()

        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")
        # all_arrays = jsonpickle.decode(self.obs_conf.tm_tiles, keys=True)
        all_arrays = pickle.loads(self.obs_conf.tm_tiles)

        station_arrays = {}
        for tile_serial in tile_serials:
            station_arrays[tile_serial] = all_arrays[tile_serial]

        return station_arrays

    def daq_type(self):
        self.obs_conf = PyTango.DeviceProxy(self.domain + "/obsconf/main")

    def task_log(self, message):
        #station_dp = PyTango.DeviceProxy(self.station_device_name)
        #station_dp.command_inout("log_me", message)
        logging.info(message)

class AAVSDaq(DaqBase):
    """
    AAVSDaq can be replaced by arbitrary DAQ implementations.

    Each implementation should implement the same interface, as specified by DaqBase.
    """

    def __init__(self, *args, **kwargs):
        super(AAVSDaq, self).__init__(*args, **kwargs)
        self.initialised = False

        self.raw_count = 0
        self.channel_count = 0
        self.beam_count = 0

        self.configuration = None

    def task_ended_callback(self, task_type, filename, tile):
        #TODO: all tiles must have sent the task_type before updating task_type_progress and switching mode
        self.data.task_log("[daq.py] Cycled Task ended")
        #enum_task_type = decode_task_type(task_type=task_type)

        """ now tell station about this task update """
        self.data.task_log("[daq.py] Updating station with progress...")
        daq_dp = PyTango.DeviceProxy(self.daq_id)

        if task_type == "burst_raw":
            self.raw_count += 1
            daq_dp["raw"] = self.raw_count
            daq_dp["task_type_progress"] = task_type
        if task_type == "burst_channel":
            self.channel_count += 1
            daq_dp["channel"] = self.channel_count
            daq_dp["task_type_progress"] = task_type
        if task_type == "burst_beam":
            self.beam_count += 1
            daq_dp["beam"] = self.beam_count
            daq_dp["task_type_progress"] = task_type

        self.data.task_log("[daq.py] Station updated.")

    def set_configuration(self, configuration):

        self.configuration = configuration

        daq_conf = configuration.copy()
        daq_conf.pop('read_raw_data', None)
        daq_conf.pop('integrated_channel', None)
        daq_conf.pop('read_channel_data', None)
        daq_conf.pop('continuous_channel', None)
        daq_conf.pop('integrated_beam', None)
        daq_conf.pop('station_beam', None)
        daq_conf.pop('correlator', None)
        daq_conf.pop('read_beam_data', None)

        # Populate configuration
        self.data.task_log("[daq.py] Populating DAQ service configuration...")
        self.data.task_log("[daq.py] %s" % str(daq_conf))
        daq.populate_configuration(configuration=daq_conf)

        # Initialise library
        if not self.initialised:
            daq.initialise_daq()
            self.initialised = True

        self.data.task_log("[daq.py] Configuration populated.")

    def terminate(self):
        if self.configuration is not None:
            if self.configuration["read_raw_data"]:
                self.data.task_log("[daq.py] Stopping Burst RAW consumer...")
                daq.stop_raw_data_consumer()

            if self.configuration["integrated_channel"]:
                self.data.task_log("[daq.py] Stopping Integrated CHANNEL consumer...")
                daq.stop_integrated_channel_data_consumer()

            if self.configuration["read_channel_data"]:
                self.data.task_log("[daq.py] Stopping Burst CHANNEL consumer...")
                daq.stop_channel_data_consumer()

            if self.configuration["continuous_channel"]:
                self.data.task_log("[daq.py] Stopping Continuous CHANNEL consumer...")
                daq.stop_continuous_channel_data_consumer()

            if self.configuration["integrated_beam"]:
                self.data.task_log("[daq.py] Stopping Integrated BEAM consumer...")
                daq.stop_integrated_beam_data_consumer()

            if self.configuration["station_beam"]:
                self.data.task_log("[daq.py] Stopping Integrated Station Beam consumer...")
                daq.stop_station_beam_data_consumer()

            if self.configuration["correlator"]:
                self.data.task_log("[daq.py] Stopping CORRELATOR consumer...")
                daq.stop_correlator()

            if self.configuration["read_beam_data"]:
                self.data.task_log("[daq.py] Stopping Burst BEAM consumer...")
                daq.stop_beam_data_consumer()

            if self.configuration["read_station_data"]:
                self.data.task_log("[daq.py] Stopping Burst Station consumer...")
                # daq.stop_station_beam_data_consumer()

    def stop(self):
        self.terminate()
        # if self.configuration["read_raw_data"]:
        #     self.data.task_log("[daq.py] Stopping Burst RAW consumer...")
        #     daq.stop_raw_data_consumer()
        #
        # if self.configuration["read_channel_data"]:
        #     self.data.task_log("[daq.py] Stopping Burst CHANNEL consumer...")
        #     daq.stop_channel_data_consumer()
        #
        # if self.configuration["read_beam_data"]:
        #     self.data.task_log("[daq.py] Stopping Burst BEAM consumer...")
        #     daq.stop_beam_data_consumer()

    def start(self):
        #TODO: we have not used conf["read_station_data"] yet

        self.data.task_log("[daq.py] Starting data acquisition...")
        # ------------------------------- Raw data consumer ------------------------------------------
        if self.configuration["read_raw_data"]:
            self.data.task_log("[daq.py] Starting RAW data consumer...")
            try:
                daq.start_raw_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] RAW data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] RAW consumer error: %s" % str(e))
                raise

        # ----------------------------- Channel data consumer ----------------------------------------
        # Running in integrated mode
        if self.configuration["integrated_channel"]:
            self.data.task_log("[daq.py] Starting Integrated CHANNEL data consumer...")
            try:
                daq.start_integrated_channel_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Integrated CHANNEL data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Integrated CHANNEL consumer error: %s" % str(e))
                raise

        # Running in continuous mode
        if self.configuration["continuous_channel"]:
            self.data.task_log("[daq.py] Starting Continuous CHANNEL data consumer...")
            try:
                daq.start_continuous_channel_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Continuous CHANNEL data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Continuous CHANNEL consumer error: %s" % str(e))
                raise

        # Running in burst mode
        if self.configuration["read_channel_data"]:
            self.data.task_log("[daq.py] Starting Burst CHANNEL data consumer...")
            try:
                daq.start_channel_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Burst CHANNEL data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Burst CHANNEL consumer error: %s" % str(e))
                raise

        # ------------------------------- Beam data consumers -----------------------------------------
        # Running in integrated mode
        if self.configuration["station_beam"]:
            self.data.task_log("[daq.py] Starting Station BEAM data consumer...")
            try:
                daq.start_station_beam_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Integrated BEAM data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Integrated BEAM consumer error: %s" % str(e))
                raise

        # Running in continuous mode
        if self.configuration["integrated_beam"]:
            self.data.task_log("[daq.py] Starting Continuous BEAM data consumer...")
            try:
                daq.start_integrated_beam_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Continuous BEAM data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Continuous BEAM consumer error: %s" % str(e))
                raise

        # Running in burst mode
        if self.configuration["read_beam_data"]:
            self.data.task_log("[daq.py] Starting Burst BEAM data consumer...")
            try:
                daq.start_beam_data_consumer(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] Burst BEAM data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] Burst BEAM consumer error: %s" % str(e))
                raise

        # --------------------------------------- Correlator ------------------------------------------
        if self.configuration["correlator"]:
            self.data.task_log("[daq.py] Starting CORRELATOR data consumer...")
            try:
                daq.start_correlator(callback=self.task_ended_callback)
                self.data.task_log("[daq.py] CORRELATOR data consumer started.")
            except Exception as e:
                self.data.task_log("[daq.py] CORRELATOR consumer error: %s" % str(e))
                raise