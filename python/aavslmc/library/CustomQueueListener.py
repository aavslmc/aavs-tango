from logutils.queue import QueueListener

class CustomQueueListener(QueueListener):
    def __init__(self, queue, *handlers):
        super(CustomQueueListener, self).__init__(queue, *handlers)
        """
        Initialise an instance with the specified queue and
        handlers.
        """
        # Changing this to a list from tuple in the parent class
        self.handlers_list = list(handlers)

    # def handle(self, record):
    #     """
    #     Override handle a record.
    #
    #     This just loops through the handlers offering them the record
    #     to handle.
    #
    #     :param record: The record to handle.
    #     """
    #     record = self.prepare(record)
    #     for handler in self.handlers:
    #         if record.levelno >= handler.level:  # This check is not in the parent class
    #             handler.handle(record)

    def addHandler(self, hdlr):
        """
        Add the specified handler to this logger.
        """
        if not (hdlr in self.handlers_list):
            self.handlers_list.append(hdlr)
            self.handlers = tuple(self.handlers_list)

    def removeHandler(self, hdlr):
        """
        Remove the specified handler from this logger.
        """
        if hdlr in self.handlers_list:
            hdlr.close()
            self.handlers_list.remove(hdlr)
            self.handlers = tuple(self.handlers_list)