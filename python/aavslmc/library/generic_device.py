"""
This module handle get/set of attributes over tango and get/run of commands over tango
It enables the properties/commands of a device instance to be accessed from another device
Also supports group modification/command runs

Note: Tango does not like unicode strings
"""

import itertools
import json

import PyTango
from PyTango import DeviceProxy, Group, AttributeProxy, DevString, DeviceData, AttrWriteType, DevFailed

import numpy
import aavslmc.library.utils as util
from aavslmc.library.helpers import get_dp_command, get_tango_device_type_id

SKIP = 0
LIMIT = 10
DEVICE_PROXY_TIMEOUT = 10000


class MissingValueError(Exception):
    pass


class DeviceProxyQueryWrapper(object):
    """
    Exposes get/filter and update operations on properties and devices,
    both individually and in groups,
    by creating proxies to the underlying devices
    This class is intended as a helper class to the LMC_DS ,
    which is responsible for routing all device queries and commands
    """

    def __init__(self, domain):
        self.domain = domain

    def get_tango_address(self, device_type, device_id='*', attribute_name=None):
        """
        Tango breaks if passed unicode strings
        :param device_type:
        :param attribute_name:
        :return:
        """
        params = [
            self.domain,
            device_type,
            str(device_id)]

        if attribute_name:
            params.append(attribute_name)

        return str('/'.join(params))

    def handle_attr_group_reply(self, group_reply, attr_match):
        if group_reply.has_failed():
            return

        if str(group_reply.get_data().value).lower() == attr_match.lower():
            return group_reply.dev_name()

    def extract_stack(self, error):
        return {"desc": error.desc,
                "severity": str(error.severity),
                "origin": error.origin,
                "reason": error.reason}

    def handle_group_reply(self, group_reply):
        error_dict = None
        if group_reply.has_failed():
            device_type, device_id = get_tango_device_type_id(group_reply.dev_name())
            device_dict = {
                'component_type': device_type,
                'component_id': device_id
            }
            err_stack = group_reply.get_err_stack()

            if err_stack:
                device_dict['request_state'] = str(err_stack[-1].severity)
                device_dict['request_status'] = err_stack[-1].desc

                error_dict = {}
                error_dict["component_type"] = device_type
                error_dict["component_id"] = device_id
                error_dict["desc"] = err_stack[-1].desc
                error_dict["stack"] = map(self.extract_stack, err_stack)

        else:
            device_dict = json.loads(group_reply.get_data())
        return device_dict, error_dict

    def filter(self, device_type=None, rack_ids=None, station_ids=None, with_value=False, status=None, skip=SKIP,
               limit=LIMIT):
        device_names_for_json = self._filter_device_list(
            device_type=device_type,
            rack_ids=rack_ids,
            station_ids=station_ids,
            with_value=with_value,
            status=status,
            skip=skip,
            limit=limit)

        device_dicts = []
        error_dicts = []
        group = Group('filter')
        group.add(device_names_for_json)
        group_device_data = DeviceData()
        group_device_data.insert(DevString, str(json.dumps({'with_value': with_value})))
        group_replies = group.command_inout('to_json', group_device_data)
        for group_reply in group_replies:
            device_dict, err_dict = self.handle_group_reply(group_reply)
            if err_dict:
                error_dicts.append(err_dict)
            device_dicts.append(device_dict)
        return {"content": device_dicts, "errors": error_dicts}

    def filter_attributes(self, attribute_name=None, device_type=None, device_id=None, rack_ids=None, station_ids=None,
                          with_value=False, status=None, skip=SKIP, limit=LIMIT,
                          component_ids=None, **kwargs):

        # Added kwargs to handle extra arguments form outside - TODO check if we can remove
        device_names_for_json = self._filter_device_list(
            device_type=device_type,
            device_id=device_id,
            rack_ids=rack_ids,
            station_ids=station_ids,
            with_value=with_value,
            status=status,
            skip=None,
            limit=None,
            component_ids=component_ids
        )
        return self.get_attributes(
            device_names_for_json,
            attribute_name=attribute_name,
            with_value=with_value,
            skip=skip, limit=limit
        )

    def filter_commands(self, command_name=None, device_type=None, device_id=None, rack_ids=None, station_ids=None,
                        with_value=False, status=None, skip=SKIP, limit=LIMIT):

        device_names_for_json = self._filter_device_list(
            device_type=device_type,
            device_id=device_id,
            rack_ids=rack_ids,
            station_ids=station_ids,
            with_value=with_value,
            status=status,
            skip=skip,
            limit=limit
        )

        return self.get_commands(
            device_names_for_json,
            command_name=command_name,
            with_value=with_value,
            skip=skip, limit=limit
        )

    def _filter_device_list(self, device_type=None, device_id=None, rack_ids=None, station_ids=None, with_value=False,
                            status=None, skip=SKIP, limit=LIMIT,
                            component_ids=None):
        """
        Obtains a list of to_json responses for devices which fall under the specified device_type, rack_ids and station_ids,
        constrained by skip and limit
        :param device_type:
        :param rack_ids:
        :param station_ids:
        :param with_value: Gives the value of the properties in to_json if True
        :param skip:
        :param limit:
        :return:
        """
        filtered_device_names = set()

        # if device_id refers to more than one device, we should apply filtering
        if device_type and device_id and device_id != '*':
            return [self.get_tango_address(device_type, device_id)]

        if device_type:
            device_type_group = Group('by_device_type')
            device_type_group.add(self.get_tango_address(device_type))
            filtered_device_names = set(device_type_group.get_device_list())

        if rack_ids:
            rack_device_proxies = [DeviceProxy(self.get_tango_address('rack', rack_id)) for rack_id in rack_ids]
            [proxy.set_timeout_millis(DEVICE_PROXY_TIMEOUT) for proxy in rack_device_proxies]
            if device_type:
                params = {"component_type": [device_type]}
            else:
                params = {}

            rack_member_lists = []
            _rack_member_lists = [rack_dp.get_member_names(json.dumps(params))
                                  for rack_dp in rack_device_proxies]

            for rack_json in _rack_member_lists:
                rack_member_lists.append(json.loads(rack_json)["member_device_names"])
            rack_member_device_names = list(itertools.chain.from_iterable(rack_member_lists))
            if device_type:
                filtered_device_names.intersection_update(rack_member_device_names)
            else:
                filtered_device_names = rack_member_device_names

        if station_ids and (not device_type or device_type == 'tile'):
            station_device_proxies = [DeviceProxy(self.get_tango_address('station', station_id)) for station_id in
                                      station_ids]
            [proxy.set_timeout_millis(DEVICE_PROXY_TIMEOUT) for proxy in station_device_proxies]
            station_member_lists = [station_dp.get_property('tiles') for station_dp in station_device_proxies]
            station_member_device_names = reduce(lambda x, y: x + y, station_member_lists)

            if device_type or rack_ids:
                filtered_device_names.intersection_update(station_member_device_names)
            else:
                filtered_device_names = station_member_device_names

        if component_ids and device_type:
            components = [self.get_tango_address(device_type, id) for id in component_ids]
            filtered_device_names.intersection_update(components)

        sorted_device_names = sorted(filtered_device_names)
        if not sorted_device_names:
            return []

        if status:
            device_names_for_json = self.paginate_by_status(sorted_device_names, status, skip, limit)
        else:
            if limit == 0:
                device_names_for_json = sorted_device_names[skip:]
            else:
                device_names_for_json = sorted_device_names[skip:limit]

        return device_names_for_json

    def paginate_by_status(self, sorted_device_names, status, skip, limit):
        device_matching_status = []

        # This is a bit less efficient probably, but so much clearer
        group = Group('filter_by_status')
        group.add(sorted_device_names)
        group_replies = group.read_attribute('state')
        for group_reply in group_replies:
            device_matching_status.append(self.handle_attr_group_reply(group_reply, status))
        # TODO handle potential errors from group replies
        device_names_for_json = [d for d in device_matching_status if d is not None]
        if limit == 0:
            device_names_for_json = device_names_for_json[skip:]
        else:
            device_names_for_json = device_names_for_json[skip:limit]

        return device_names_for_json

    def get_commands(self, device_name_list=None, command_name=None, skip=SKIP, limit=LIMIT):
        """

        :param device_name_list:
        :param command_name: If supplied, returns the command details for each device in device_name_list
            If not supplied, all details for all commands for each device in device_name_list are supplied
        :return:
        """
        cmd_list = []
        skipped = 0
        if not device_name_list:
            return cmd_list

        device_group = Group('device_cmds')
        device_group.add(device_name_list)
        for device_name in sorted(device_group.get_device_list()):
            device_proxy = DeviceProxy(device_name)
            device_proxy.set_timeout_millis(DEVICE_PROXY_TIMEOUT)

            if command_name:
                cmd_config = device_proxy.command_query(command_name)
                cmd_config_list = [cmd_config]
            else:
                cmd_config_list = sorted(device_proxy.command_list_query())

            if skip and (len(cmd_config_list)) <= skip:
                skip = skip - len(cmd_config_list)
                continue
            else:
                cmd_config_list = cmd_config_list[skip:skip + limit]

            for device_cmd_config in cmd_config_list:
                attr_dict = get_dp_command(
                    device_proxy.dev_name(), device_cmd_config, with_context=True
                )
                cmd_list.append(attr_dict)

            if len(cmd_list) == limit:
                return cmd_list
        return cmd_list

    def get_attributes(self, device_name_list=None, attribute_name=None, with_value=False, skip=SKIP, limit=LIMIT):
        """

        :param device_name_list:
        :param attribute_name: If supplied, returns the attribute details for each device in device_name_list
            If not supplied, all details for all attributes for each device in device_name_list are supplied
        :param with_value: Includes the attribute value, read timestamp and is_alarm with the attribute details
        :return:
        """

        attr_list = []
        error_dicts = []
        if device_name_list:
            device_group = Group('device_attrs')
            device_group.add(device_name_list)
            for device_name in device_group.get_device_list():
                device_proxy = DeviceProxy(device_name)
                device_proxy.set_timeout_millis(DEVICE_PROXY_TIMEOUT)
                params = json.dumps({'with_value': with_value, 'with_context': True, 'attribute_name': attribute_name})
                try:
                    attributes = json.loads(device_proxy.attributes_to_json(params))

                    if skip and (len(attributes)) <= skip:
                        skip = skip - len(attr_list)
                        continue
                    else:

                        if limit:
                            if len(attributes) <= skip + limit:
                                attr_list.extend(attributes[skip:])
                                limit = skip + limit - len(attributes)
                            else:
                                attr_list.extend(attributes[skip:skip + limit])
                        else:
                            attr_list.extend(attributes)

                        if skip and (len(attributes)) > skip:
                            skip = 0
                except DevFailed as e:
                    error_dict = {}
                    component = device_name.split("/")
                    error_dict["component_type"] = component[1]
                    error_dict["component_id"] = component[2]
                    error_dict["desc"] = e.args[-1].desc
                    error_dict["stack"] = map(self.extract_stack, e.args)
                    error_dicts.append(error_dict)

                if len(attr_list) == limit:
                    return {"content": attr_list, "errors": error_dicts}
        return {"content": attr_list, "errors": error_dicts}

    def _update_tango_attribute_config(self, attr_proxy, property_dict):
        if not self._update_attr_config_required(property_dict):
            return 0

        attr_config = attr_proxy.get_config()
        if not attr_config:
            raise Exception("Attr does not exist")

        self._try_update_attr_property(attr_config, 'min_alarm', property_dict, 'min_value', 'Not Specified')
        self._try_update_attr_property(attr_config, 'max_alarm', property_dict, 'max_value', 'Not Specified')

        self._try_update_poll(attr_proxy, property_dict)

        attr_proxy.set_config(attr_config)
        return 1

    def _try_update_attr_property(self, attr_config, attr_name, property_dict, key, default_value):
        if property_dict.get(key, 'Missing') != 'Missing':
            value = str(property_dict.get(key) or default_value)
            alarms = getattr(attr_config, 'alarms')
            setattr(alarms, attr_name, value)

    def _try_update_poll(self, attr_proxy, property_dict):
        if property_dict.get('polling_frequency', 'Missing') != 'Missing':
            if not property_dict.get('polling_frequency'):
                if attr_proxy.is_polled():
                    attr_proxy.stop_poll()
            else:
                poll_period = property_dict.get('polling_frequency')
                attr_proxy.poll(poll_period)

    def _try_write_attribute(self, attr_proxy, property_dict):
        if property_dict.get('value', 'Missing') != 'Missing':
            # if property is set to None, reset it
            property_value = property_dict.get('value', 'Not Specified')
            if attr_proxy.get_config().writable == AttrWriteType.READ:
                return 0

            type = attr_proxy.get_config().data_type
            if type == PyTango._PyTango.CmdArgType.DevVoid:
                attr_proxy.write(property_value)
            elif type == PyTango._PyTango.CmdArgType.DevBoolean:
                attr_proxy.write(util.parseBoolString(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevShort:
                attr_proxy.write(int(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevShort:
                attr_proxy.write(int(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevLong:
                attr_proxy.write(long(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevFloat:
                attr_proxy.write(float(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevDouble:
                attr_proxy.write(float(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevUShort:
                attr_proxy.write(numpy.uint(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevULong:
                attr_proxy.write(numpy.ulong(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevString:
                attr_proxy.write(str(property_value))
            elif type == PyTango._PyTango.CmdArgType.ConstDevString:
                attr_proxy.write(str(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevUChar:
                attr_proxy.write(str(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevLong64:
                attr_proxy.write(numpy.long(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevLong64:
                attr_proxy.write(numpy.ulong(property_value))
            elif type == PyTango._PyTango.CmdArgType.DevInt:
                attr_proxy.write(int(property_value))
            else:
                attr_proxy.write(str(property_value))
            return 1
        return 0

    def _update_device_tango_attribute(self, component_type, component_id, property_dict):
        # import ipdb;ipdb.set_trace()
        attr_name = self.get_tango_address(component_type, component_id, property_dict.get('name'))
        attr_proxy = AttributeProxy(attr_name)

        updated = self._update_tango_attribute_config(attr_proxy, property_dict)

        updated = updated or self._try_write_attribute(attr_proxy, property_dict)

        return updated

    def update_attributes(self, device_type, device_attributes, component_ids=None):
        # Get list from single or multiple property post
        updated = 0
        if component_ids:
            device_list = [self.get_tango_address(device_type, component_id) for component_id in component_ids]
        else:
            group = Group(device_type)
            group.add(self.get_tango_address(device_type, '*'))
            device_list = group.get_device_list()

        attributes_missing_values = [p for p in device_attributes if not p.get('value')]
        if len(attributes_missing_values):
            raise MissingValueError()
        # TODO check the case where we are sending no value, but we are sending min and max values - attribute config
        # Update multiple properties from a list of properties
        for property_dict in device_attributes:
            if property_dict.get('component_id'):
                updated += self._update_device_tango_attribute(device_type, property_dict.get('component_id'),
                                                               property_dict)
            else:
                for device_name in device_list:

                    attr_name = '/'.join([device_name, property_dict.get('name')])
                    attr_proxy = AttributeProxy(attr_name)
                    partial_update = self._try_write_attribute(attr_proxy, property_dict)

                    # Groups don't support editing alarms and polling
                    if self._update_attr_config_required(property_dict):
                        partial_update = partial_update or self._update_tango_attribute_config(attr_proxy,
                                                                                               property_dict)
                updated += partial_update  # partial update only relevant on devices in device list
        return {'meta': {'updated': {'properties': updated}}}

    def _update_attr_config_required(self, property_dict):
        if set(['min_value', 'max_value', 'polling_frequency']).intersection(property_dict.keys()):
            return True
        return False
