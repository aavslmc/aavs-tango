import select
import socket
from xml.etree import cElementTree as ElementTree


class GangliaGmetad:
    """ Class to interface with gmetad service """

    def __init__(self, hostname="localhost", xml_port=8651, interactive_port=8652):
        """ Class constructor """
        self.hostname = hostname
        self.xml_port = xml_port
        self.interactive_port = interactive_port

    def read_data_from_port(self, send=None):
        """ Read data from port """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect((self.hostname, self.xml_port))
            r, w, x = select.select([sock], [], [], 2)
            if not r:
                sock.close()
                return
        except socket.error, e:
            raise Exception("Could not open socket to %s:%d - %s" % (self.hostname, self.xml_port, e))

        try:
            if send is not None: sock.send(send)
        except socket.error, e:
            raise Exception('Could not send to %s:%d - %s' % (self.hostname, self.xml_port, e))

        data_buffer = ""
        while True:
            try:
                data = sock.recv(8192)
            except socket.error, e:
                raise Exception('Could not receive data from %s:%d - %s' % (self.hostname, self.xml_port, e))

            if not data:
                break
            data_buffer += data.decode("ISO-8859-1")

        sock.close()
        return data_buffer

    def read_xml_data(self):
        """ Read XML data """
        return self.read_data_from_port()

    def read_metrics(self):
        """ Read metrics """
        result = {}
        hosts = {}

        xml_data = self.read_xml_data()
        if xml_data:
            try:
                ganglia = ElementTree.XML(xml_data)
            except UnicodeEncodeError:
                raise Exception("Could not parse XML data")
        else:
            raise Exception("No data obtained from Gmetad")

        # Loop over all grids
        for grid_elem in ganglia.findall("GRID"):
            # Loop over all clusters in grid
            for cluster in grid_elem.findall("CLUSTER"):
                # Loop over all hosts in grid
                for host in cluster.findall("HOST"):
                    # Get host info
                    host_name = host.attrib["IP"]
                    hosts[host_name] = host.attrib

                    hosts[host_name]["CLUSTER"] = cluster.attrib["NAME"]
                    hosts[host_name]["GRID"] = grid_elem.attrib["NAME"]
                    hosts[host_name]["METRICS"] = {}

                    # For all host metrics, extract data
                    for metric in host.findall("METRIC"):
                        hosts[host_name]["METRICS"][metric.attrib["NAME"]] = metric.attrib

                        for extra_data in metric.findall("EXTRA_DATA"):
                            for extra_elem in extra_data.findall("EXTRA_ELEMENT"):
                                metric.attrib[extra_elem.attrib["NAME"]] = extra_elem.attrib["VAL"]

        result["hosts"] = hosts
        return result


class GangliaMonitor(object):
    """ Class to interact with data gathered with GangliaGmetad"""
    def __init__(self, host="localhost", metrics=None):
        self.data = None
        self.hostname = host

        # Create gmetad connection
        self.gmetad = GangliaGmetad(hostname=host)

        # Initialise metric in ganglia monitor
        self.metrics = metrics
        self.hosts = None
        self.initialise_monitor()

    def initialise_monitor(self):
        """ Initialise ganglia monitor for the metrics specified """

        # Get data
        self.data = self.gmetad.read_metrics()

        # Create list of hostnames
        self.hosts = self.data["hosts"].keys()

        if self.metrics is None:
            print "No metrics specified"
            return

        # Get list of metrics
        defined_metrics = self.data["hosts"][self.hosts[0]]['METRICS'].keys()

        # For every metric specified, check if metric exists
        for metric in self.metrics:
            if metric not in defined_metrics:
                print "Error adding metric %s" % metric
            else:
                setattr(self, metric, [0] * len(self.hosts))

        # All done

    def get_metric_value(self, host, metric):
        """ Get a metric value for a particular host """

        # Check if host is present
        if host not in self.hosts:
            raise Exception("Host not found in Ganglia service")

        # Check if metric exists
        if metric not in self.metrics:
            raise Exception("Metric not found in Ganglia service")

        # Return metric value
        value = self.data['hosts'][host]['METRICS'][metric]['VAL']
        return value

    def check_host(self, host):
        """ Check if host exists in ganglia data"""
        if host not in self.hosts:
            return False

        return True

    def update_monitor(self):
        """ Update data """

        # Get updates data from gmetad
        self.data = self.gmetad.read_metrics()

        # Loop over all hosts
        for i, host in enumerate(self.hosts):
            # Loop over all metrics
            for metric in self.metrics:
                getattr(self, metric)[i] = self.data['hosts'][host]['METRICS'][metric]['VAL']


if __name__ == "__main__":
    ganglia = GangliaMonitor(host="127.0.0.1", metrics=['cpu_user', 'cpu_system'])
    ganglia.update_monitor()
