import xml.etree.ElementTree as ET
import requests
import pickle
import re

import subprocess


class ApiException(Exception):
    pass


class AuthorisationError(Exception):
    pass


class SX1036(object):
    """ Class implementing XML communication protocol with the Mellanox SX1036 switch"""

    def __init__(self, host, username="admin", password="admin"):
        """ Class constructor """

        # XML API related information
        self.host = host
        self.api_url = "http://" + self.host + "/xtree"
        self.login_url = "http://" + self.host + "/admin/launch"
        self.querystring = {"script": "rh", "template": "login", "action": "login"}
        self.session = requests.Session()
        self._username = username
        self._password = password
        self.request_template = \
            """<?xml version="1.0" encoding="utf8" standalone="no"?>
            <xg-request>
                <action-request>
                    <action-name>/do_rest</action-name>
                    <nodes>

                    </nodes>
                </action-request>
            </xg-request>"""

        # SSH related regular expression
        self._physical_location_re = re.compile(r"Eth\d+(/\d+)+")
        self._ingress_re = re.compile(r"\s+60\sseconds\singress\srate:\s+\d+\s+bits/sec,\s+\d+\sbytes/sec,\s+(?P<ingress>\d+)\s+packets/sec")
        self._egress_re = re.compile(r"\s+60\sseconds\segress\srate:\s+\d+\s+bits/sec,\s+\d+\sbytes/sec,\s+(?P<egress>\d+)\s+packets/sec")
        self._rx_errors = re.compile(r"Rx.*bytes\s+(?P<rx_errors>\d+)\s+error\spackets.*Tx", re.DOTALL)
        self._tx_errors = re.compile(r"Tx.*bytes\s+(?P<tx_errors>\d+)\s+error\spackets", re.DOTALL)

        # SSH command related information
        self._ssh_command = "\"show interface ethernet {}\""

        # Attribute placeholders
        self.online = False
        self.modules = None
        self.interfaces = None
        self.temperatures = None

        # Get list of modules and list of interfaces
        self.update_modules_status()
        self.get_interface_information()

    def node(self, name, type, value):
        node = ET.Element("node")

        element_name = ET.Element("name")
        element_type = ET.Element("type")
        element_value = ET.Element("value")

        element_name.text = name
        element_type.text = type
        element_value.text = value

        node.append(element_name)
        node.append(element_type)
        node.append(element_value)

        return node

    def request_root(self):
        """ Get request root """
        return ET.fromstring(self.request_template)

    def login(self):
        """ Log into switch"""
        data = {"f_user_id": self._username, "f_password": self._password}
        self.session.post(self.login_url, data=data, params=self.querystring, allow_redirects=False)

    def _query_api(self, request):
        """ Compose and send query to switch """
        # Session needed to keep session cookie across requests
        headers = {"Content-Type": "application/x-www-form-urlencoded"}

        api_response = self.session.post(self.api_url, data=request, headers=headers)
        xml_response = api_response.content
        if api_response.status_code == 404:
            raise ApiException("{} responded with 404".format(self.api_url))

        root = ET.fromstring(xml_response)

        # Not authenticated has a different XMLS structure, so we hve to check for xg-status.
        xg_status = root.find("./xg-status")
        if xg_status is not None:
            code = xg_status.find("./status-code").text
            msg = xg_status.find("./status-msg").text
            raise AuthorisationError("Code {0}, Message: {1}".format(code, msg))

        return_status = root.find('./action-response/return-status')
        return_code = int(return_status.find("return-code").text)
        return_msg = return_status.find("return-msg").text

        if return_code != 0:
            raise Exception(return_msg)
        return root

    def query(self, xml_request):
        """ Send query to switch"""
        try:
            xml_response = self._query_api(xml_request)
        except AuthorisationError as e:
            self.login()
            xml_response = self._query_api(xml_request)
        return xml_response

    def get_module_information(self):
        """ Get list of module names """  #

        # Get list of chassis modules
        root = self.request_root()
        nodes = root.find('./action-request/nodes')
        nodes.append(self.node("get", "string", "/mlnxos/v1/chassis/modules/*"))
        xml_request = ET.tostring(root, encoding="utf8")

        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        self.modules = {}
        for node in reply_nodes:
            self.modules[node.find("value").text] = {'name': node.find("value").text}

        # Get list of temperature sensors
        root = self.request_root()
        nodes = root.find('./action-request/nodes')
        nodes.append(self.node("get", "string", "/mlnxos/v1/chassis/temperature/*"))
        xml_request = ET.tostring(root, encoding="utf8")

        self.temperatures = {}
        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        for node in reply_nodes:
            self.temperatures[node.find("value").text[1:]] = 0

    def update_modules_status(self):
        """ Check the status of each module """

        # If list of modules is not populated, get list of modules
        if self.modules is None:
            self.get_module_information()

        root = self.request_root()
        nodes = root.find('./action-request/nodes')
        for module in self.modules.keys():
            nodes.append(self.node("get", "string", "/mlnxos/v1/chassis/modules/%s/status" % module))
        xml_request = ET.tostring(root, encoding="utf8")

        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        for node in reply_nodes:
            module_name = node.find("name").text.split("/")[-2]
            self.modules[module_name]['status'] = True if node.find("value").text == 'OK' else False

        # Get temperature information
        root = self.request_root()
        nodes = root.find('./action-request/nodes')
        for temperature in self.temperatures:
            nodes.append(self.node("get", "string", "/mlnxos/v1/chassis/temperature/\/%s/temperature" % temperature))
        xml_request = ET.tostring(root, encoding="utf8")

        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        for node in reply_nodes:
            self.temperatures[node.find("name").text.split('/')[6]] = node.find("value").text

    def get_interface_information(self):
        """ Get interface list """
        # Get list of interfaces
        root = self.request_root()
        nodes = root.find('./action-request/nodes')
        nodes.append(self.node("get", "string", "/mlnxos/v1/vsr/vsr-default/interfaces/*"))
        xml_request = ET.tostring(root, encoding="utf8")

        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        self.interfaces = [{"index": node.find("value").text} for node in reply_nodes]

        # For each interface, get it's details
        for i, interface in enumerate(self.interfaces):
            root = self.request_root()
            nodes = root.find("./action-request/nodes")
            interface_key = "%s/%s" % ("/mlnxos/v1/vsr/vsr-default/interfaces", interface['index'])
            nodes.append(self.node("get", "string", "%s/*" % interface_key))
            xml_request = ET.tostring(root, encoding="utf8")

            xml_response = self.query(xml_request)
            reply_nodes = xml_response.findall('./action-response/nodes/node')
            for node in reply_nodes:
                key = node.find("name").text.replace("%s/" % interface_key, "")
                self.interfaces[i][key] = node.find("value").text

        # Get interface statistics
        # self._get_interface_statistics()

    def update_interface_information(self, interface):
        """ Update information of a particular interface """

        # Get interface index
        index = self._get_interface_index(interface)

        root = self.request_root()
        nodes = root.find("./action-request/nodes")
        interface_key = "/mlnxos/v1/vsr/vsr-default/interfaces/{}".format(self.interfaces[index]['index'])
        nodes.append(self.node("get", "string", "%s/*" % interface_key))
        xml_request = ET.tostring(root, encoding="utf8")

        xml_response = self.query(xml_request)
        reply_nodes = xml_response.findall('./action-response/nodes/node')
        for node in reply_nodes:
            key = node.find("name").text.replace("%s/" % interface_key, "")
            self.interfaces[index][key] = node.find("value").text

        # Get interface statistics
        # self._get_interface_statistics(interface)

    def check_status(self):
        """ Get the status for each module on device """
        try:
            # Simply check if we can query the switch
            self.update_modules_status()
            self.get_interface_information()
            self.online = True
        except Exception as e:
            self.online = False
            raise Exception('Exception occurred in get_status() in {}. Message: {}'
                            .format(self.__class__, e.message))

    def _get_interface_statistics(self, interface=None):
        """ Get interface statistics using SSH
        :param interface: Get statistics for this interface, if None get for all """

        # Update one or all interfaces
        if interface is not None:
            command = self._ssh_command.format(interface)
        else:
            command = self._ssh_command.format("")

        # Perform command over ssh
        print' '.join([str(x) for x in ["ssh {}@{}".format(self._username, self.host),
                                "cli \"enable\"", command]])
        ssh = subprocess.Popen(["ssh {}@{}".format(self._username, self.host),
                                "cli \"enable\"", command],
                               shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

        # Get result and transform into a single string
        result = ''.join(ssh.stdout.readlines()).replace('\n', ' ')

        # Find the start position within the string for each interface
        positions = [m.start(0) for m in re.finditer('Eth', result)]

        # Add dummy position to mark end of string
        positions.append(len(result))

        for i in range(len(positions) - 1):
            interface_info = result[positions[i]:positions[i + 1]]

            # Get interface physical location
            physical_location = re.match(self._physical_location_re, interface_info).group()

            # Get interface index
            index = self._get_interface_index(physical_location.replace("Eth", ""))

            # Get interface packets speeds
            info = re.search(self._ingress_re, interface_info).groupdict()
            info['ingress'] = int(info['ingress'])
            self.interfaces[index].update(info)

            info = re.search(self._egress_re, interface_info).groupdict()
            info['egress'] = int(info['egress'])
            self.interfaces[index].update(info)

            # Get error packets
            info = re.search(self._rx_errors, interface_info).groupdict()
            info['rx_errors'] = int(info['rx_errors'])
            self.interfaces[index].update(info)

            info = re.search(self._tx_errors, interface_info).groupdict()
            info['tx_errors'] = int(info['tx_errors'])
            self.interfaces[index].update(info)


    def _get_interface_index(self, interface):
        """ Interface to find """
        index = [i for i, f in enumerate(self.interfaces) if f['physical_location'] == interface]
        if len(index) == 0:
            raise Exception("Requested interface {} not found".format(interface))
        return index[0]

if __name__ == "__main__":
    switch = SX1036("sw40g-1")
    switch.update_interface_information("1/8/1")
    print switch.interfaces
