import subprocess
import re

switch_hostname = "admin@10.0.10.100"

def get_switch_transceivers():
    """ Get the cable identifiers connected to the switch """

    # Get list of interfaces on the switch
    command = "\"show interface ethernet\""
    ssh = subprocess.Popen(["ssh", switch_hostname, "cli \"enable\"", command],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = ssh.stdout.readlines()
    if result == []:
        print "Could not get list of interfaces from {}: {}".format(switch_hostname, ssh.stderr.readlines())
        return

    # Extract interfaces from result
    interfaces = re.findall("\d+/\d+[/\d+]*", ''.join(result))

    # Loop over interfaces and get transceiver information
    command = "\"show interface ethernet {} transceiver\""
    tranceivers = {}
    for interface in interfaces:
        ssh = subprocess.Popen(["ssh", switch_hostname, "cli \"enable\"", command.format(interface)],
                               shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        result = ssh.stdout.readlines()
        if result == []:
            print "Could not get list of interfaces from {}: {}".format(switch_hostname, ssh.stderr.readlines())
        else:
            result = ''.join(result)
            if re.search("Cable is not present", result) is not None:
                print "{} not connected".format(interface)
                tranceivers[interface] = None
            else:
                # Parse reply
                match = re.match(".*part\Wnumber\W+:\W(?P<part_number>[\w\-]+).*" +
                                 "serial\Wnumber\W+:\W(?P<serial_numer>\w+).*", result, re.DOTALL).groupdict()

                # Check whether link is up
                cmd = "\"show interface ethernet {} status\""
                ssh = subprocess.Popen(["ssh", switch_hostname, "cli \"enable\"",
                                       cmd.format(interface)],
                                       shell=False,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
                res = ssh.stdout.readlines()
                if res == []:
                    print "Could not get state of interface {} from {}: {}".format(interface, switch_hostname, ssh.stderr.readlines())
                    match['operational'] = False
                else:
                    if re.search("Down", ''.join(res)) is not None:
                        match['operational'] = False
                    else:
                        match['operational'] = True

                tranceivers[interface] = match
                print "{}: {}".format(interface, str(match))

    print tranceivers

get_switch_transceivers()
