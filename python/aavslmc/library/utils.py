import subprocess
import json
from enum import Enum
from astropy import units as u
from astropy.coordinates import Angle, AltAz, SkyCoord, EarthLocation

# TODO: DO NOT REMOVE THESE - REQUIRED FOR PROPER DECODING OF ENUM TYPES IN JSON
from pyfabil import Device, BoardState, Error, BoardMake, Status, RegisterType, Permission


def ping(host):
    process = subprocess.Popen(['ping', '-c1', '-w1', host], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.communicate()

    # 0 - up , 1 - no return, 2 other error
    return process.returncode == 0


def wait_for_device(device_proxy, max, period):
    time_elapsed = 0
    ping = 0
    import time
    while time_elapsed < max:
        try:
            ping = device_proxy.ping()
        except:
            time.sleep(period)
            time_elapsed += period
        if ping:
            break
    if not ping:
        raise Exception("ServerNotStarted")
    else:
        return ping


class EnumEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return {"__enum__": str(obj)}
        return json.JSONEncoder.default(self, obj)


def as_enum(d):
    if "__enum__" in d:
        name, member = d["__enum__"].split(".")
        return getattr(globals()[name], member)
    else:
        return d


def parseBoolString(string):
    return string[0].upper() == 'T'


def popx(q, x):
    for x in xrange(x):
        try:
            yield q.pop()
        except IndexError:
            raise StopIteration()

class DataLevel(Enum):
    Tile = 1,
    Station = 2

class JobTasks(Enum):
    Burst_Raw = 1,
    Integrated_Channel = 2,
    Burst_Channel = 3,
    Cont_Channel = 4,
    Integrated_Beam = 5,
    Integrated_Station_Beam = 6,
    Correlator = 7,
    Burst_Beam = 8,
    Burst_Station = 9

class JobCallbackMode(Enum):
    Burst = 1,
    Continuous = 2,
    Correlator = 3


def job_callback_mode(enum_task_type):
    if enum_task_type == JobTasks.Burst_Raw:
        callback_mode = JobCallbackMode.Burst
    elif enum_task_type == JobTasks.Integrated_Channel:
        callback_mode = JobCallbackMode.Continuous
    elif enum_task_type == JobTasks.Burst_Channel:
        callback_mode = JobCallbackMode.Burst
    elif enum_task_type == JobTasks.Cont_Channel:
        callback_mode = JobCallbackMode.Continuous
    elif enum_task_type == JobTasks.Integrated_Beam:
        callback_mode = JobCallbackMode.Continuous
    elif enum_task_type == JobTasks.Integrated_Station_Beam:
        callback_mode = JobCallbackMode.Continuous
    elif enum_task_type == JobTasks.Correlator:
        callback_mode = JobCallbackMode.Correlator
    elif enum_task_type == JobTasks.Burst_Beam:
        callback_mode = JobCallbackMode.Burst
    elif enum_task_type == JobTasks.Burst_Station:
        callback_mode = JobCallbackMode.Burst
    return callback_mode


def task_type_string(enum_task_type):
    if enum_task_type == JobTasks.Burst_Raw:
        task_type = "burst_raw"
    elif enum_task_type == JobTasks.Integrated_Channel:
        task_type = "integrated_channel"
    elif enum_task_type == JobTasks.Burst_Channel:
        task_type = "burst_channel"
    elif enum_task_type == JobTasks.Cont_Channel:
        task_type = "cont_channel"
    elif enum_task_type == JobTasks.Integrated_Beam:
        task_type = "integrated_beam"
    elif enum_task_type == JobTasks.Integrated_Station_Beam:
        task_type = "integrated_station_beam"
    elif enum_task_type == JobTasks.Correlator:
        task_type = "correlator"
    elif enum_task_type == JobTasks.Burst_Beam:
        task_type = "burst_beam"
    elif enum_task_type == JobTasks.Burst_Station:
        task_type = "burst_station"
    return task_type


def decode_task_type(task_type):
    if task_type == "burst_raw":
        enum_task_type = JobTasks.Burst_Raw
    elif task_type == "integrated_channel":
        enum_task_type = JobTasks.Integrated_Channel
    elif task_type == "burst_channel":
        enum_task_type = JobTasks.Burst_Channel
    elif task_type == "cont_channel":
        enum_task_type = JobTasks.Cont_Channel
    elif task_type == "integrated_beam":
        enum_task_type = JobTasks.Integrated_Beam
    elif task_type == "integrated_station_beam":
        enum_task_type = JobTasks.Integrated_Station_Beam
    elif task_type == "correlator":
        enum_task_type = JobTasks.Correlator
    elif task_type == "burst_beam":
        enum_task_type = JobTasks.Burst_Beam
    elif task_type == "burst_station":
        enum_task_type = JobTasks.Burst_Station

    return enum_task_type


class AntennaArray(object):
    """ Class representing antenna array """

    def __init__(self, tile_dict=None):
        self._ref_lat = None
        self._ref_lon = None
        self._ref_height = None

        self._x = None
        self._y = None
        self._z = None
        self._height = None

        self._n_antennas = None
        self._baseline_vectors_ecef = None
        self._baseline_vectors_enu = None

        # If json_list is defined, initialise array
        if tile_dict:
            self.load_from_dict(tile_dict)

    def positions(self):
        """
        :return: Antenna positions
        """
        return self._x, self._y, self._z, self._height

    @property
    def size(self):
        return self._n_antennas

    @property
    def lat(self):
        """ Return reference latitude
        :return: reference latitude
        """
        return self._ref_lat

    @property
    def lon(self):
        """ Return reference longitude
        :return: reference longitude
        """
        return self._ref_lon

    @property
    def height(self):
        """
        Retern reference height
        :return: reference height
        """
        return self._ref_height

    def load_from_dict(self, tile_dict):
        """ Load antenna positions from dictionary
        :param json_list: Dictionary of tile antenna locations
        """

        # Keep track of how many antennas are in the array
        self._n_antennas = 0

        # Convert file data to astropy format, and store lat, lon and height for each antenna
        if self._x is None:
            self._x = []

        if self._y is None:
            self._y = []

        if self._z is None:
            self._z = []

        if self._height is None:
            self._height = []

        for antenna in tile_dict["antennas"]:
            antenna_id = str(antenna.get("id"))
            antenna_lat = str(antenna.get("latitude"))
            antenna_lon = str(antenna.get("longitude"))
            antenna_height = str(antenna.get("height"))

            astropy_latitude = Angle(antenna_lat + 'd')
            astropy_longitude = Angle(antenna_lon + 'd')
            astropy_height = u.Quantity(float(antenna_height), u.m)
            loc = EarthLocation.from_geodetic(lon=astropy_longitude, lat=astropy_latitude, height=astropy_height)
            (x, y, z) = loc.to_geocentric()
            self._x.append(x)
            self._y.append(y)
            self._z.append(z)
            self._height.append(astropy_height)

            # Use first antenna as reference antenna
            if self._n_antennas == 0:
                self._ref_lat = Angle(antenna_lat + 'd')
                self._ref_lon = Angle(antenna_lon + 'd')
                self._ref_height = u.Quantity(float(antenna_height), u.m)

            self._n_antennas += 1
