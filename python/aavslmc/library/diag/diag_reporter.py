from PyTango import DeviceProxy, Database
import logging
from time import sleep
import pika #rabbitmq
import json


def get_diag_proxy():
    diag_proxy = DeviceProxy("test/DiagnosticsStream/1")
    return diag_proxy


def get_device_list():
    dp = get_diag_proxy()
    device_list = dp.command_inout("get_device_tree")
    device_list = json.loads(device_list)
    return device_list


def get_metric_report():
    dp = get_diag_proxy()
    report = dp["metric_report"]
    report = json.loads(report.value)
    return report


def start_device_state_monitoring():
    dp = get_diag_proxy()
    dp.set_timeout_millis(30000)
    dp.command_inout("start_state_monitoring")


def stop_device_state_monitoring():
    dp = get_diag_proxy()
    dp.set_timeout_millis(30000)
    dp.command_inout("stop_state_monitoring")


def get_alarms():
    dp = get_diag_proxy()
    alarm_count = dp["alarm_notifications"].value
    print(alarm_count)

    alarms = dp.command_inout("popx_alarms",alarm_count)
    alarms = json.loads(alarms)
    return alarms


def get_events():
    dp = get_diag_proxy()
    event_count = dp["event_notifications"].value

    events = dp.command_inout("popx_events", event_count)
    events = json.loads(events)
    return events

def rabbitmq_setup(conn="localhost"):
    # Establish connection to a queue, on a broker in the local machine
    connection = pika.BlockingConnection(pika.ConnectionParameters(conn))
    channel = connection.channel()

    # Establish recipient queues
    channel.queue_declare(queue='diag_alarms')
    channel.queue_declare(queue='diag_events')
    channel.queue_declare(queue='diag_metrics')
    channel.queue_declare(queue='diag_devices')

    return connection,channel

def gather_diagnostics():
    logging.info("Getting device list:")
    device_list = get_device_list()
    if not device_list in [{}, []]:
        # Messages in RabbitMQ are sent via an exchange, using default one
        channel.basic_publish(exchange='', routing_key='diag_devices', body=json.dumps(device_list))
        print("[x] Sent: %s" % json.dumps(device_list))

    logging.info("Getting latest metric report:")
    metrics = get_metric_report()
    if not metrics in [{}, []]:
        channel.basic_publish(exchange='', routing_key='diag_metrics', body=json.dumps(metrics))
        print("[x] Sent: %s" % json.dumps(metrics))

    logging.info("Getting alarm notifications batch:")
    alarms = get_alarms()
    if not alarms in [{}, []]:
        channel.basic_publish(exchange='', routing_key='diag_alarms', body=json.dumps(alarms))
        print("[x] Sent: %s" % json.dumps(alarms))

    logging.info("Getting event notifications batch:")
    events = get_events()
    if not events in [{}, []]:
        channel.basic_publish(exchange='', routing_key='diag_events', body=json.dumps(events))
        print("[x] Sent: %s" % json.dumps(events))


if __name__ == '__main__':
    try:
        # RabbitMQ connection
        connection, channel = rabbitmq_setup()

        logging.info("Starting device state monitoring...")
        start_device_state_monitoring()
        logging.info("Device state monitoring started.")

        while True:
            sleep(5)
            gather_diagnostics()

    except Exception as e:
        logging.error(str(e))
        print str(e)

        logging.info("Stopping device state monitoring...")
        stop_device_state_monitoring()
        logging.info("Device state monitoring stopped.")

        logging.info("Closing RabbitMQ connection...")
        if connection:
            connection.close()
        logging.info("Connection closed.")

