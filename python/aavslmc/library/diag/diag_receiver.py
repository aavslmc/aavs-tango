import logging
from time import sleep
import pika #rabbitmq
import json
from optparse import OptionParser

def rabbitmq_setup(conn="localhost",events=False,alarms=False,metrics=False,devices=False):
    # Establish connection to a queue, on a broker in the local machine
    connection = pika.BlockingConnection(pika.ConnectionParameters(conn))
    channel = connection.channel()

    # Establish recipient queues
    channel.queue_declare(queue='diag_alarms')
    channel.queue_declare(queue='diag_events')
    channel.queue_declare(queue='diag_metrics')
    channel.queue_declare(queue='diag_devices')

    # Establish callbacks for receiving queues
    if alarms:
        channel.basic_consume(alarm_callback, queue='diag_alarms', no_ack=True)
    if events:
        channel.basic_consume(event_callback, queue='diag_events', no_ack=True)
    if metrics:
        channel.basic_consume(metrics_callback, queue='diag_metrics', no_ack=True)
    if devices:
        channel.basic_consume(devices_callback, queue='diag_devices', no_ack=True)

    return connection,channel

def alarm_callback(ch, method, properties, body):
    print(" [x] ALARM: %r" % body)

def event_callback(ch, method, properties, body):
    print(" [x] EVENT: %r" % body)

def metrics_callback(ch, method, properties, body):
    print(" [x] METRICS: %r" % body)

def devices_callback(ch, method, properties, body):
    print(" [x] DEVICES: %r" % body)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-c", "--connection", dest="conn",
                      help="Address to connect to for receiving diagnostics", metavar="CONN")
    parser.add_option("-e", "--events",
                          action="store_true", dest="events", default=False, help="Listen for events")
    parser.add_option("-a", "--alarms",
                          action="store_true", dest="alarms", default=False, help="Listen for alarms")
    parser.add_option("-m", "--metrics",
                          action="store_true", dest="metrics", default=False, help="Listen for metrics")
    parser.add_option("-d", "--devices",
                          action="store_true", dest="devices", default=False, help="Listen for device tree")

    (options, args) = parser.parse_args()

        # RabbitMQ connection
    print options

    connection = None 
    try:
        connection, channel = rabbitmq_setup(conn = options.conn,
                                             events=options.events,
                                             alarms=options.alarms,
                                             metrics=options.metrics,
                                             devices=options.devices)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()

    except Exception as e:
        logging.error(str(e))
        print str(e)

        logging.info("Closing RabbitMQ connection...")
        if connection:
            connection.close()
        logging.info("Connection closed.")

