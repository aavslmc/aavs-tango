#!/usr/bin/env python
# -*- coding:utf-8 -*- 


##############################################################################
## license :
##============================================================================
##
## File :        BandpassCalibrationJob_DS.py
## 
## Project :     AAVS
##
## This file is part of Tango device class.
## 
## Tango is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Tango is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Tango.  If not, see <http://www.gnu.org/licenses/>.
## 
##
## $Author :      andrea.demarco$
##
## $Revision :    $
##
## $Date :        $
##
## $HeadUrl :     $
##============================================================================
##            This file is generated by POGO
##    (Program Obviously used to Generate tango Object)
##
##        (c) - Software Engineering Group - ESRF
##############################################################################

"""Job device for bandpass calibration."""

__all__ = ["BandpassCalibrationJob_DS", "BandpassCalibrationJob_DSClass", "main"]

__docformat__ = 'restructuredtext'

import PyTango
import sys
from Job_DS import Job_DS, Job_DSClass
# Add additional import
#----- PROTECTED REGION ID(BandpassCalibrationJob_DS.additionnal_import) ENABLED START -----#
from aavslmc.servers.error_handling import exception_manager
from pytcpo.Core.Common import TCPOCode
from pytcpo.Pipelines import SimpleAnomalyPipelineBuilder
import json
import inspect
#----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.additionnal_import

## Device States Description
## ON : 
## ALARM : 
## STANDBY : Job Process has been stopped and is waiting to be started again
## RUNNING : Job Process attached to DeviceServer is running
## UNKNOWN : 

class BandpassCalibrationJob_DS (Job_DS):

    #--------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.global_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.global_variables

    def __init__(self,cl, name):
        super(BandpassCalibrationJob_DS,self).__init__(cl,name)
        self.debug_stream("In __init__()")
        BandpassCalibrationJob_DS.init_device(self)
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.__init__) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.delete_device) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).delete_device()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_active_alarms_read = 0
        self.attr_device_event_counter_read = 0
        self.attr_device_event_info_read = ''
        self.attr_visual_state_read = PyTango.DevState.UNKNOWN
        self.attr_alarms_set_read = False
        self.attr_diagnosis_mode_read = False
        self.attr_rack_id_read = 0
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.init_device) ENABLED START -----#
        self.debug_stream("In init_device(9)")
        try:
            tile_config_dict = json.loads(self.tile_config)
            for tile_id_key in tile_config_dict:
                antennas_list = tile_config_dict[tile_id_key]
                antennas_tuple = tuple(antennas_list)
                tile_config_dict[tile_id_key] = antennas_tuple

            # Create and Setup Pipeline
            pipeline = SimpleAnomalyPipelineBuilder()
            pipeline.setup(params={'archive_coeffs': self.archive_coeffs,  # Archive all coefficients (debug mode)
                                   'source': self.src_location,  # Source Location (for Data)
                                   'redis_server': (self.redis_ip, self.redis_port),  # Redis Configuration
                                   'station': self.station_id,  # Station Number
                                   'tile_config': tile_config_dict,  # The Tile/Antenna specification {0: (0, 1, 2, 3, 8, 9, 10, 11)}
                                   'range_samples': (self.range_samples_min, self.range_samples_max),  # How Many Samples to read
                                   'polyn_order': self.polyn_order,  # The Polynomial Order
                                   'noise_prefilter': self.noise_prefilter,  # The Gradient for detecting and ignoring spikes
                                   'devs_multiple': self.devs_multiple,  # 5 Standard Deviations for Outlier
                                   'deriv_ignore': self.deriv_ignore,  # Ignore First Derivatives below 0.5
                                   'morph_window': self.morph_window,  # Morpholigical Window Size
                                   'outlier_ratio': self.outlier_ratio,  # Outlier Ratio Cutoff
                                   'min_overlap': self.min_overlap,  # Minimum Overlap to Tolerate
                                   'avg_length': self.avg_length,  # Length of Moving Average
                                   'mean_length': self.mean_length,  # Mean is over each 8-channels
                                   'detrend_mean': self.detrend_mean},  # De-Trend everything to around 300
                           offline=True)

            # Build and get manager
            self.manager = pipeline.build()

        except (PyTango.DevFailed, Exception) as df:
            self.set_state(PyTango.DevState.FAULT)
            self.set_status("Bandpass Calibration initialisation failed")

            self._generic_fatal_stream(command_name=inspect.stack()[0][3],
                                       command_inputs="",
                                       message=str(df))

        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.always_executed_hook) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.always_executed_hook

    #-----------------------------------------------------------------------------
    #    BandpassCalibrationJob_DS read/write attribute methods
    #-----------------------------------------------------------------------------
    
    def read_active_alarms(self, attr):
        self.debug_stream("In read_active_alarms()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.active_alarms_read) ENABLED START -----#
        attr.set_value(self.attr_active_alarms_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.active_alarms_read
        
    def write_active_alarms(self, attr):
        self.debug_stream("In write_active_alarms()")
        data=attr.get_write_value()
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.active_alarms_write) ENABLED START -----#
        self.attr_active_alarms_read = data
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.active_alarms_write
        
    def read_device_event_counter(self, attr):
        self.debug_stream("In read_device_event_counter()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.device_event_counter_read) ENABLED START -----#
        attr.set_value(self.attr_device_event_counter_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.device_event_counter_read
        
    def write_device_event_counter(self, attr):
        self.debug_stream("In write_device_event_counter()")
        data=attr.get_write_value()
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.device_event_counter_write) ENABLED START -----#
        self.attr_device_event_counter_read = data
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.device_event_counter_write
        
    def read_device_event_info(self, attr):
        self.debug_stream("In read_device_event_info()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.device_event_info_read) ENABLED START -----#
        attr.set_value(self.attr_device_event_info_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.device_event_info_read
        
    def write_device_event_info(self, attr):
        self.debug_stream("In write_device_event_info()")
        data=attr.get_write_value()
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.device_event_info_write) ENABLED START -----#
        self.attr_device_event_info_read = data
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.device_event_info_write
        
    def read_visual_state(self, attr):
        self.debug_stream("In read_visual_state()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.visual_state_read) ENABLED START -----#
        attr.set_value(self.attr_visual_state_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.visual_state_read
        
    def read_alarms_set(self, attr):
        self.debug_stream("In read_alarms_set()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.alarms_set_read) ENABLED START -----#
        attr.set_value(self.attr_alarms_set_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.alarms_set_read
        
    def read_diagnosis_mode(self, attr):
        self.debug_stream("In read_diagnosis_mode()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.diagnosis_mode_read) ENABLED START -----#
        attr.set_value(self.attr_diagnosis_mode_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.diagnosis_mode_read
        
    def write_diagnosis_mode(self, attr):
        self.debug_stream("In write_diagnosis_mode()")
        data=attr.get_write_value()
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.diagnosis_mode_write) ENABLED START -----#
        self.attr_diagnosis_mode_read = data
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.diagnosis_mode_write
        
    def read_rack_id(self, attr):
        self.debug_stream("In read_rack_id()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.rack_id_read) ENABLED START -----#
        attr.set_value(self.attr_rack_id_read)
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.rack_id_read
        
    def write_rack_id(self, attr):
        self.debug_stream("In write_rack_id()")
        data=attr.get_write_value()
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.rack_id_write) ENABLED START -----#
        self.attr_rack_id_read = data
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.rack_id_write
        
    
    
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.initialize_dynamic_attributes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.initialize_dynamic_attributes
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.read_attr_hardware) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.read_attr_hardware


    #-----------------------------------------------------------------------------
    #    BandpassCalibrationJob_DS command methods
    #-----------------------------------------------------------------------------
    
    def start(self):
        """ 
        
        :param : 
            [{`name`: `cwd`, `type`: `str`},
            {`name`: `command`, `type`: `list`}]
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In start()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.start) ENABLED START -----#
        with exception_manager(self, callback=lambda: self.set_state(PyTango.DevState.FAULT)):
            # Now start the reader, doing all
            self.manager.start()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.start
        
    def is_start_allowed(self):
        self.debug_stream("In is_start_allowed()")
        state_ok = not(self.get_state() in [PyTango.DevState.RUNNING])
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.is_start_allowed) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.is_start_allowed
        return state_ok
        
    def stop(self):
        """ 
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In stop()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.stop) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.stop
        
    def is_stop_allowed(self):
        self.debug_stream("In is_stop_allowed()")
        state_ok = not(self.get_state() in [PyTango.DevState.STANDBY])
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.is_stop_allowed) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.is_stop_allowed
        return state_ok
        
    def start_poll_command(self, argin):
        """ A helper method to starts polling a command. The idea of this
        command is to allow any device in the system to inititiate polling on
        any other device.
        
        Pickled dictionary with:
        :param proxy: Address of device on which to start polling.
        	The default is for the device to poll itself.
        :param poll_command: Name of command to poll.
        :param period: Polling period.
        
        :param argin: 
            [{`name`: `proxy`, `type`: `str`},
            {`name`: `poll_command`, `type`: `str`},
            {`name`: `period`, `type`: `int`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In start_poll_command()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.start_poll_command) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).start_poll_command(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.start_poll_command
        
    def to_json(self, argin):
        """ Returns a JSON translation of this device as a JSON document.
        
        :param argin: [{`name`: `with_value`, `type`: `str`}]
        :type: PyTango.DevString
        :return: The JSON string representing this device.
        :rtype: PyTango.DevString """
        self.debug_stream("In to_json()")
        argout = ''
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.to_json) ENABLED START -----#
        argout = super(BandpassCalibrationJob_DS, self).to_json(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.to_json
        return argout
        
    def stop_polling_command(self, argin):
        """ A helper method to stop polling a command. The idea of this 
        command is to allow any device in the system to inititiate polling on
        any other device.
                
        Pickled dictionary with:
        :param proxy: Address of device on which to start polling.
        	The default is for the device to poll itself.
        :param poll_command: Name of command to poll.
        
        :param argin: 
            [{`name`: `proxy`, `type`: `str`},
            {`name`: `poll_command`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In stop_polling_command()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.stop_polling_command) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).stop_polling_command(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.stop_polling_command
        
    def check_running(self):
        """ 
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In check_running()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.check_running) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).check_running()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.check_running
        
    def commands_to_json(self, argin):
        """ 
        
        :param argin: [{`name`: `with_context`, `type`: `bool`, `default:`True`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevString """
        self.debug_stream("In commands_to_json()")
        argout = ''
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.commands_to_json) ENABLED START -----#
        argout = super(BandpassCalibrationJob_DS, self).commands_to_json(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.commands_to_json
        return argout
        
    def attributes_to_json(self, argin):
        """ 
        
        :param argin: 
            [{`name`: `with_value`, `type`: `str`, `default`: `False`},
            {`name`: `with_context`, `type`: `str`, `default`:`True`}]
        :type: PyTango.DevString
        :return: The attributes of this device in json
        :rtype: PyTango.DevString """
        self.debug_stream("In attributes_to_json()")
        argout = ''
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.attributes_to_json) ENABLED START -----#
        argout = super(BandpassCalibrationJob_DS, self).attributes_to_json(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.attributes_to_json
        return argout
        
    def alarm_on(self, argin):
        """ Notification callback for Elettra alarm device when an alarm is triggered.
        
        The value passed to the command by the Alarm Server is made 
        by concatenating the alarm rule name and the values of the 
        attributes involved in the formula in the form attr_name=value.
         The separator character is ?;?.
        
        :param argin: [{`name`: `alarm_info`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In alarm_on()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.alarm_on) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).alarm_on(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.alarm_on
        
    def alarm_off(self, argin):
        """ Notification callback for Elettra alarm device when an alarm is back to normal.
        
        The value passed to the command by the Alarm Server is made 
        by concatenating the alarm rule name and the values of the 
        attributes involved in the formula in the form attr_name=value.
         The separator character is ?;?.
        
        :param argin: [{`name`: `alarm_info`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In alarm_off()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.alarm_off) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).alarm_off(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.alarm_off
        
    def create_alarm(self, argin):
        """ The format of our JSON alarm messages is as follows:
        
                {
                  ``alarms``:[
                    {
                      ``address``: ``test/device/id_1/prop1``,
                      ``name``: ``name1``,
                      ``checks``:[
                        { ``type``: ``ABSOLUTE``,
                          ``value``: ``20``
                        },
                        { ``type``: ``MIN``,
                          ``value``: ``0``
                        },
                        { ``type``: ``MAX``,
                          ``value``: ``100``
                        }
                        ]
                    },
                    {
                      ``address``: ``test/device/id_1/prop2``,
                      ``name``: ``name2``,
                      ``checks``:[
                        { ``type``: ``ABSOLUTE``,
                          ``value``: ``60``
                        }
                        ]
                    },
                    {
                      ``address``: ``test/device/id_1``,
                      ``name``: ``name3``,
                      ``checks``:[
                        { ``type``: ``STATUS``,
                          ``value``: ``ON``
                        }
                        ]
                    }
                    ]
                }
        
        :param argin: [{`name`: `alarm_json`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In create_alarm()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.create_alarm) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).create_alarm(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.create_alarm
        
    def remove_alarm(self, argin):
        """ Removes an alarm for this device given an alarm name.
        
        :param argin: [{`name`: `alarm_name`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In remove_alarm()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.remove_alarm) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).remove_alarm(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.remove_alarm
        
    def get_device_alarms(self):
        """ This command will return a list of alarm names which are configured for this device.
        
        :param : 
        :type: PyTango.DevVoid
        :return: List of alarm names in a JSON string.
        :rtype: PyTango.DevString """
        self.debug_stream("In get_device_alarms()")
        argout = ''
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.get_device_alarms) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).get_device_alarms()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.get_device_alarms
        return argout
        
    def emit_event(self, argin):
        """ Emits a device event with a custom message passed to this command.
        
        :param argin: [{`name`: `event_msg`, `type`: `str`}]
        :type: PyTango.DevString
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In emit_event()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.emit_event) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).emit_event(argin)
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.emit_event
        
    def get_metrics(self):
        """ gets list of attributes marked as metrics for device
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVarStringArray """
        self.debug_stream("In get_metrics()")
        argout = ['']
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.get_metrics) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).get_metrics()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.get_metrics
        return argout
        
    def on_exported_hook(self):
        """ command run by thread when device is exported
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In on_exported_hook()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.on_exported_hook) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.on_exported_hook
        
    def reset(self):
        """ Reset device to default state
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In reset()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.reset) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).reset()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.reset
        
    def ping(self):
        """ Ping device to update status
        
        :param : 
        :type: PyTango.DevVoid
        :return: 
        :rtype: PyTango.DevVoid """
        self.debug_stream("In ping()")
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.ping) ENABLED START -----#
        super(BandpassCalibrationJob_DS, self).ping()
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.ping
        

    #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.programmer_methods) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.programmer_methods

class BandpassCalibrationJob_DSClass(Job_DSClass):
    #--------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.global_class_variables

    def dyn_attr(self, dev_list):
        """Invoked to create dynamic attributes for the given devices.
        Default implementation calls
        :meth:`BandpassCalibrationJob_DS.initialize_dynamic_attributes` for each device
    
        :param dev_list: list of devices
        :type dev_list: :class:`PyTango.DeviceImpl`"""
    
        for dev in dev_list:
            try:
                dev.initialize_dynamic_attributes()
            except:
                import traceback
                dev.warn_stream("Failed to initialize dynamic attributes")
                dev.debug_stream("Details: " + traceback.format_exc())
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.dyn_attr) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.dyn_attr

    #    Class Properties
    class_property_list = {
        }
    class_property_list.update(Job_DSClass.class_property_list)


    #    Device Properties
    device_property_list = {
        'cwd':
            [PyTango.DevString,
            "cwd",
            [] ],
        'cmd':
            [PyTango.DevVarStringArray,
            "cmd",
            [] ],
        'domain':
            [PyTango.DevString,
            "The domain under which the device will be available. Unittest code should use the domain unittest",
            ["test"] ],
        'elettra_alarm_device_address':
            [PyTango.DevString,
            "TANGO address of the Elettra alarm device.",
            ["test/Alarm/1"] ],
        'central_alarmstream_device_address':
            [PyTango.DevString,
            "TANGO address of the central alarm stream device.",
            ["test/AlarmStream/1"] ],
        'central_eventstream_device_address':
            [PyTango.DevString,
            "TANGO address of the central event stream device.",
            ["test/EventStream/1"] ],
        'metric_list':
            [PyTango.DevString,
            "A list of metrics we want to expose.",
            ["metric1,metric2"] ],
        'register_list':
            [PyTango.DevString,
            "A list of registers we want to expose.",
            ["register1,register2"] ],
        'command_list':
            [PyTango.DevString,
            "A list of commands we want to expose.",
            ["start_poll_command,stop_polling_command,create_alarm,remove_alarm,get_device_alarms"] ],
        'default_poll_period':
            [PyTango.DevString,
            "This is the default polling period for attributes to be used\nfor alarms, in milliseconds.",
            ["3000"] ],
        'central_diagstream_device_address':
            [PyTango.DevString,
            "Diag stream device address",
            ["test/DiagnosticsStream/1"] ],
        'diag_mode':
            [PyTango.DevString,
            "Diagnostics mode",
            [] ],
        'rack_id':
            [PyTango.DevString,
            "The ID of the rack to which this device belongs.",
            [] ],
        'archive_coeffs':
            [PyTango.DevBoolean,
            "archive_coeffs",
            [] ],
        'src_location':
            [PyTango.DevString,
            "src_location",
            [] ],
        'redis_ip':
            [PyTango.DevString,
            "redis_ip",
            [] ],
        'redis_port':
            [PyTango.DevString,
            "redis_port",
            [] ],
        'station_id':
            [PyTango.DevString,
            "The station device address owning this job.",
            [] ],
        'tile_config':
            [PyTango.DevString,
            "tile_config",
            [] ],
        'range_samples_min':
            [PyTango.DevUShort,
            "range_samples_min",
            [] ],
        'range_samples_max':
            [PyTango.DevUShort,
            "range_samples_max",
            [] ],
        'polyn_order':
            [PyTango.DevUShort,
            "polyn_order",
            [] ],
        'noise_prefilter':
            [PyTango.DevFloat,
            "noise_prefilter",
            [] ],
        'devs_multiple':
            [PyTango.DevFloat,
            "devs_multiple",
            [] ],
        'deriv_ignore':
            [PyTango.DevFloat,
            "deriv_ignore",
            [] ],
        'morph_window':
            [PyTango.DevUShort,
            "morph_window",
            [] ],
        'outlier_ratio':
            [PyTango.DevFloat,
            "outlier_ratio",
            [] ],
        'min_overlap':
            [PyTango.DevFloat,
            "min_overlap",
            [] ],
        'avg_length':
            [PyTango.DevUShort,
            "avg_length",
            [] ],
        'mean_length':
            [PyTango.DevUShort,
            "mean_length",
            [] ],
        'detrend_mean':
            [PyTango.DevFloat,
            "detrend_mean",
            [] ],
        }
    device_property_list.update(Job_DSClass.device_property_list)


    #    Command definitions
    cmd_list = {
        'start':
            [[PyTango.DevVoid, "[{`name`: `cwd`, `type`: `str`},\n{`name`: `command`, `type`: `list`}]"],
            [PyTango.DevVoid, "none"]],
        'stop':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'start_poll_command':
            [[PyTango.DevString, "[{`name`: `proxy`, `type`: `str`},\n{`name`: `poll_command`, `type`: `str`},\n{`name`: `period`, `type`: `int`}]"],
            [PyTango.DevVoid, "none"]],
        'to_json':
            [[PyTango.DevString, "[{`name`: `with_value`, `type`: `str`}]"],
            [PyTango.DevString, "The JSON string representing this device."]],
        'stop_polling_command':
            [[PyTango.DevString, "[{`name`: `proxy`, `type`: `str`},\n{`name`: `poll_command`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'check_running':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'commands_to_json':
            [[PyTango.DevString, "[{`name`: `with_context`, `type`: `bool`, `default:`True`}]"],
            [PyTango.DevString, "none"]],
        'attributes_to_json':
            [[PyTango.DevString, "[{`name`: `with_value`, `type`: `str`, `default`: `False`},\n{`name`: `with_context`, `type`: `str`, `default`:`True`}]"],
            [PyTango.DevString, "The attributes of this device in json"]],
        'alarm_on':
            [[PyTango.DevString, "[{`name`: `alarm_info`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'alarm_off':
            [[PyTango.DevString, "[{`name`: `alarm_info`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'create_alarm':
            [[PyTango.DevString, "[{`name`: `alarm_json`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'remove_alarm':
            [[PyTango.DevString, "[{`name`: `alarm_name`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'get_device_alarms':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevString, "List of alarm names in a JSON string."]],
        'emit_event':
            [[PyTango.DevString, "[{`name`: `event_msg`, `type`: `str`}]"],
            [PyTango.DevVoid, "none"]],
        'get_metrics':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVarStringArray, "none"]],
        'on_exported_hook':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'reset':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'ping':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"],
            {
                'Polling period': "5000",
            } ],
        }
    cmd_list.update(Job_DSClass.cmd_list)


    #    Attribute definitions
    attr_list = {
        'active_alarms':
            [[PyTango.DevLong,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'Memorized':"true"
            } ],
        'device_event_counter':
            [[PyTango.DevLong64,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'device_event_info':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'visual_state':
            [[PyTango.CmdArgType.DevState,
            PyTango.SCALAR,
            PyTango.READ]],
        'alarms_set':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ]],
        'diagnosis_mode':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'rack_id':
            [[PyTango.DevUShort,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        }
    attr_list.update(Job_DSClass.attr_list)


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(BandpassCalibrationJob_DSClass,BandpassCalibrationJob_DS,'BandpassCalibrationJob_DS')
        #----- PROTECTED REGION ID(BandpassCalibrationJob_DS.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	BandpassCalibrationJob_DS.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed,e:
        print '-------> Received a DevFailed exception:',e
    except Exception,e:
        print '-------> An unforeseen exception occured....',e

if __name__ == '__main__':
    main()
