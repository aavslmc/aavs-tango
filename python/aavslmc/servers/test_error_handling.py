from aavslmc.servers.error_handling import exception_manager
import json
import PyTango
from enum import Enum

class ErrorGenerator:

    def _generic_fatal_stream(self, command_name, command_inputs, message):
        # Input can be a pickled string, so check
        if isinstance(command_inputs, str):
            try:
                # was it pickled?
                dict_content = self._unpickle_cmd_args(command_inputs)
                print ("Failure for: [%s], with input: [%s] - Exception: [%s]" % (
                    command_name, str(dict_content), message))

            except:
                print ("Failure for: [%s], with input: [%s] - Exception: [%s]" % (
                    command_name, command_inputs, message))

    def as_enum(d):
        if "__enum__" in d:
            name, member = d["__enum__"].split(".")
            return getattr(globals()[name], member)
        else:
            return d

    def _unpickle_cmd_args(self, argin):
        try:
            return json.loads(argin, object_hook=self.as_enum)
        except (PyTango.DevFailed, Exception) as df:
            return {}

    def post_error_callback(self):
        print "Cleaning up after error"

    def generate(self, argin=""):
        with exception_manager(self, callback=self.post_error_callback):
            # try something that will generate an error
            result = 10/0
            return result

if __name__ == '__main__':

    ErrorGeneratorObject = ErrorGenerator()
    result = ErrorGeneratorObject.generate(argin="")