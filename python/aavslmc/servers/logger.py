import logging
import logging.handlers
from cloghandler import ConcurrentRotatingFileHandler
from PyTango.server import run, device_property
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
import PyTango
from aavslmc.servers.error_handling import exception_manager
import datetime


class FileLogger(Device):
    __metaclass__ = DeviceMeta

    logger_dict = {}
    log_path = device_property(dtype=str, default_value="/tmp")

    def __init__(self, cl, name):
        super(FileLogger, self).__init__(cl, name)
        print self.log_path

    @command(dtype_in='DevVarStringArray', dtype_out=None)
    def log(self, details):
        with exception_manager(self, callback=lambda: self.set_state(PyTango.DevState.FAULT)):
            source_device = details[2]
            message = details[3]
            timestamp = str(datetime.datetime.fromtimestamp(float(details[0]) / 1000))
            logger = self.logger_dict.get(source_device)
            if not logger:
                logger = logging.getLogger(source_device)
                logger.setLevel(logging.INFO)

                # Add the log message handler to the logger
                handler = ConcurrentRotatingFileHandler(
                    self.log_path + "/" + source_device.replace("/", "_"), maxBytes=3000000, backupCount=5)

                # handler = logging.handlers.RotatingFileHandler(
                #     self.log_path + "/" + source_device.replace("/", "_"), maxBytes=3000000, backupCount=5)

                logger.addHandler(handler)
                self.logger_dict[source_device] = logger

            logger.info("{}]\t{}".format(timestamp, message))
            # print details


def main():
    run((FileLogger,))

if __name__ == "__main__":

    db = PyTango.Database()

    server_name = 'logger/test'
    dev_info = PyTango.DbDevInfo()
    dev_info._class = "FileLogger"
    dev_info.server = server_name
    dev_info.name =  "test/logger/1"
    db.add_device(dev_info)

    db.put_device_property("test/logger/1", {"log_path": "/opt/aavs/log/tango"})
    main()