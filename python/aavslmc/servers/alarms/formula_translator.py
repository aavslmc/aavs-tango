import json
import re
import math
from PyTango import Database, DeviceProxy

db = Database()

class AlarmTranslators(object):
    """
    This class represents a translator from the JSON alarm request we will generate, into a formula (or list of
    formulae that can be parsed by the Elettra alarm device.

    The format of our JSON alarm messages is as follows:
    """
    states = {"ON", "OFF", "CLOSE", "OPEN", "INSERT", "EXTRACT", "MOVING", "STANDBY", "FAULT", "INIT", "RUNNING",
              "ALARM", "DISABLE", "UNKNOWN"}

    @classmethod
    def translate(cls, json_alarm_msg):
        """
        Accepts a JSON alarm request, translates it to Elettra compatible formulae, as well as returns a list of
        attributes on which the alarms are set.
        :param json_alarm_msg:
        :return: formulas, attributes
            (list of formula string compatible with Elettra alarm device)
            (list of attribute addresses for which some alarm string has been generated)
        """
        formulas = []
        attributes = []
        poll_periods = []
        trigger_times = []
        names = []
        alarm_on_callbacks = []
        alarm_off_callbacks = []
        abs_change_event_values = []

        alarms_dict = json.loads(json_alarm_msg)
        for alarm in alarms_dict["alarms"]:
            alarm_trigger_time = alarm.get("time")
            if alarm_trigger_time is None:
                alarm_trigger_time = "0"
            else:
                alarm_trigger_time = str(alarm_trigger_time)

            if "address" in alarm:
                address = alarm["address"]
                if re.match(r'\w+/\w+/\w+/\w+', address): # if attribute address
                    attribute_address = True
                    device_address = False
                elif re.match(r'\w+/\w+/\w+', address): # if device address
                    attribute_address = False
                    device_address = True

                poll_period = None
                if "poll_period" in alarm:
                    poll_period = alarm["poll_period"]

                alarm_on_callback = "alarm_on"
                if "alarm_on" in alarm:
                    alarm_on_callback = alarm["alarm_on"]

                alarm_off_callback = "alarm_off"
                if "alarm_off" in alarm:
                    alarm_off_callback = alarm["alarm_off"]

                alarm_off_callback = "alarm_off"
                if "alarm_off" in alarm:
                    alarm_off_callback = alarm["alarm_off"]

                abs_change_event = 1
                if "abs_change_event" in alarm:
                    abs_change_event = alarm["abs_change_event"]

                if "checks" in alarm:
                    status_check = False
                    abs_check = False
                    min_check = False
                    max_check = False
                    abs_string = ""
                    min_string = ""
                    max_string = ""
                    status_string = ""

                    for check in alarm["checks"]:
                        check_type = check["type"]
                        value = check["value"]

                        if check_type == "ABSOLUTE":
                            abs_string = address + " == " + str(value)
                            abs_check = True

                        if check_type == "MIN":
                            min_check = True
                            min_string = address + " <= " + str(value)

                        if check_type == "MAX":
                            max_check = True
                            max_string = address + " >= " + str(value)

                        # if check_type == "STATUS":
                        #     if value in cls.states:
                        #         status_check = True
                        #         status_string = address + "/State" " == " + str(value)
                        #
                        # if check_type == "STATUS_FIELD":
                        #     if value in cls.states:
                        #         status_check = True #why?
                        #         status_string = address + " == " + str(value)

                    if attribute_address:
                        if abs_check == True and min_check == False and max_check == False:
                            alarm_string = "(" + abs_string + ")"
                        elif abs_check == False and min_check == True and max_check == False:
                            alarm_string = "(" + min_string + ")"
                        elif abs_check == False and min_check == False and max_check == True:
                            alarm_string = "(" + max_string + ")"
                        elif abs_check == False and min_check == True and max_check == True:
                            alarm_string = "(" + min_string + ")" + " || " + "(" + max_string + ")"
                        elif abs_check == True and min_check == True and max_check == False:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + min_string + ")"
                        elif abs_check == True and min_check == False and max_check == True:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + max_string + ")"
                        elif abs_check == True and min_check == True and max_check == True:
                            alarm_string = "(" + abs_string + ")" + " || " + "(" + min_string + ")" + " || " + "(" + max_string + ")"
                        # elif status_check:
                        #     alarm_string = "(" + status_string + ")"

                    # elif device_address :
                    #     alarm_string = "(" + status_string + ")"

                    if "name" in alarm:
                        name = alarm["name"]

                    if len(alarm["checks"]) > 0:
                        formulas.append(alarm_string)
                        if attribute_address:
                            attributes.append(address)
                        elif device_address:
                            attributes.append(address + "/State")

                        poll_periods.append(poll_period)
                        trigger_times.append(alarm_trigger_time)
                        names.append(name)
                        alarm_on_callbacks.append(alarm_on_callback)
                        alarm_off_callbacks.append(alarm_off_callback)
                        abs_change_event_values.append(abs_change_event)

                    alarm_string = ""

        argout = formulas, attributes, poll_periods, trigger_times, names,\
                 alarm_on_callbacks, alarm_off_callbacks, abs_change_event_values
        return argout

    @classmethod
    def python_dict_to_json_alarms(cls, alarm_config):
        json_alarms = []

        device_list = alarm_config["devices"]
        domain = alarm_config["domain"]

        for group_name, alarms in device_list.iteritems():
            for alarm in alarms:

                group_devices = db.get_device_member("{}/{}/*".format(domain, group_name))
                group_devices = [int(i) for i in list(group_devices)]
                if "ids" in alarm:
                    conf_devices = alarm["ids"]

                    # check if conf_devices actually exist in group
                    if set(conf_devices) <= set(group_devices):
                        devices = conf_devices
                    else:
                        devices = group_devices
                else:
                    devices = group_devices

                for device in devices:
                    full_device_name = "/".join([domain, group_name, str(device)])

                    device_alarms_array = []
                    dp = DeviceProxy(full_device_name)

                    #for alarm in alarms:
                    alarm_dict = {}

                    attribute = alarm["attribute"]
                    checks = alarm["checks"]

                    if "poll_period" in alarm:
                        poll_period = alarm["poll_period"]
                        alarm_dict["poll_period"] = poll_period

                    if "change_event_resolution" in alarm:
                        abs_change = alarm["change_event_resolution"]
                    else:
                        abs_change = math.ldexp(1.0, -53)  # smallest double that 0.5+epsilon != 0.5
                        # abs_change = 1.0

                    if "alarm_on_callback" in alarm:
                        alarm_on_callback = alarm["alarm_on_callback"]
                    else:
                        alarm_on_callback = "alarm_on"

                    if "alarm_off_callback" in alarm:
                        alarm_off_callback = alarm["alarm_off_callback"]
                    else:
                        alarm_off_callback = "alarm_off"

                    attribute_address = "/".join([full_device_name, attribute])
                    alarm_dict["address"] = attribute_address
                    alarm_dict["name"] = "_".join([group_name, str(device), attribute, "alarm"])
                    alarm_dict["alarm_on"] = alarm_on_callback
                    alarm_dict["alarm_off"] = alarm_off_callback
                    alarm_dict["abs_change_event"] = abs_change

                    alarm_checks_array = []
                    if 'minimum' in checks:
                        alarm_checks_array.append({"type": "MIN", "value": str(checks["minimum"])})
                    if 'maximum' in checks:
                        alarm_checks_array.append({"type": "MAX", "value": str(checks["maximum"])})
                    if 'absolute' in checks:
                        alarm_checks_array.append({"type": "ABSOLUTE", "value": str(checks["absolute"])})
                    # if 'value' in checks:
                    #     alarm_checks_array.append({"type": "value", "value": str(checks["value"])})

                    alarm_dict["checks"] = alarm_checks_array
                    device_alarms_array.append(alarm_dict)

                    command_dict = {"alarms": device_alarms_array}
                    device_alarms_array_json = json.dumps(command_dict)

                    json_alarms.append(device_alarms_array_json)

        return json_alarms


def main():
    alarms_config = {
        "domain": "test",
        "devices": {
            "tile": [
                {
                    "ids": [1],
                # list of ids to which to apply the alarm rule, optional (applied to all group if not present)
                    "attribute": "temperature",  # "name": "tile_[id]_[attribute]_alarm"
                    "poll_period": 5000,  # milliseconds
                    "change_event_resolution": 0.5,  # the absolute change that will trigger an event for an alarm check
                    "alarm_on_callback": "temp_alarm_on",  # the custom callback to call when the alarm goes ON
                    "alarm_off_callback": "temp_alarm_off",  # the custom callback to call when the alarm goes on
                    "checks":
                        {
                            "minimum": 0,
                            "maximum": 30,
                            "absolute": 85
                        }
                },
                {
                    "ids": [1],
                # list of ids to which to apply the alarm rule, optional (applied to all group if not present)
                    "attribute": "voltage",  # "name": "tile_[id]_[attribute]_alarm"
                    "poll_period": 5000,  # milliseconds
                    "change_event_resolution": 0.5,  # the absolute change that will trigger an event for an alarm check
                    "alarm_on_callback": "voltage_alarm_on",  # the custom callback to call when the alarm goes ON
                    "alarm_off_callback": "voltage_alarm_off",  # the custom callback to call when the alarm goes on
                    "checks":
                        {
                            "minimum": 0,
                            "maximum": 5,
                            "absolute": 5
                        }
                }
            ]
        }
    }

    json_alarms = AlarmTranslators.python_dict_to_json_alarms(alarm_config=alarms_config)

    for alarm in json_alarms:
        print "Decoding alarm..."
        formulas, attributes, poll_periods, trigger_times, names, \
        alarm_on_callbacks, alarm_off_callbacks, abs_change_event_values = AlarmTranslators.translate(alarm)

        print formulas
        print attributes
        print trigger_times
        print names
        print alarm_on_callbacks
        print alarm_off_callbacks
        print abs_change_event_values

if __name__ == '__main__':
    main()