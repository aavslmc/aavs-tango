from contextlib import contextmanager
import PyTango
import inspect
import sys
import traceback

@contextmanager
def exception_manager(cls, arguments="", callback=None):
    try:
        yield

    except PyTango.DevFailed as df:
        # Find caller from the relative point of this executing handler
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)

        # Form exception message
        message = "{}: {}".format(type(df).__name__, df.message)

        # Retrieve class
        class_name = str(cls.__class__.__name__)

        # Add info to message
        additional_info = traceback.format_exc()
        message = message + " [--" + additional_info + "--] "

        cls._generic_fatal_stream(command_name=class_name + "::" + calframe[2][3],
                                  command_inputs=str(arguments),
                                  message=message)

        if callback:
            callback()

        PyTango.Except.re_throw_exception(df,
                                          "AAVS_CommandFailed",
                                          message,
                                          class_name + "::" + calframe[2][3])
    except Exception as df:
        # Find caller from the relative point of this executing handler
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)

        # Form exception message
        message = "{}: {}".format(type(df).__name__, df.message)

        # Retrieve class
        class_name = str(cls.__class__.__name__)

        # Add info to message
        additional_info = traceback.format_exc()
        message = message + " [--" + additional_info + "--] "

        cls._generic_fatal_stream(command_name=class_name+"::"+calframe[2][3],
                                  command_inputs=str(arguments),
                                  message=message)

        if callback:
            callback()

        PyTango.Except.throw_exception("AAVS_CommandFailed",
                                       message,
                                       class_name + "::" + calframe[2][3])