rack_config = {
    "test/tile/3": {"width": 1, "height": 4, "row": 2, "column": 1},
    "test/pdu/1": {"width": 4, "height": 1, "row": 8, "column": 1},
    "test/switch/1": {"width": 4, "height": 1, "row": 16, "column": 1},
    "test/server/1": {"width": 4, "height": 2, "row": 17, "column": 1},
}

config = {
    "domain": "test",
    "default_logging_target": ["device::test/logger/1"],
    "default_logging_level": "DEBUG",
    "alarms_config_file": "aavslmc.config.alarms_config",
    "server_priorities":
        {
            "logger": 1,
            "Alarm": 2,
            "AlarmStream": 3,
            "EventStream": 3,
            "DiagnosticsStream": 4,
            "LMC_DS": 5,
            "Server_DS": 6,
            "Switch_DS": 6,
            "Tile_DS": 6,
            "Rack_DS": 7
        },
    "devices": {
        # "logger": {
        #     "1": {
        #         "properties": {
        #             "log_path": "/opt/aavs/log/tango"
        #         },
        #         "class": "FileLogger",
        #         "server": "logger"
        #     }
        # },
        "logger": {
            "1": {
                "properties": {
                    "log_path": "/opt/aavs/log/tango"
                },
                "class": "Logger_DS",
                "server": "Logger_DS"
            }
        },
        "Alarm": {
            "1": {
                "properties": {
                    "DbHost": "localhost",
                    "DbName": "alarm",
                    "DbPasswd": "alarm_pswd",
                    "DbPort": "3306",
                    "DbUser": "alarm_usr",
                    "InstanceName": "1",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "Alarm",
                "server": "Alarm",
                "python_server": False
            }
        },
        "AlarmStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "AlarmStream",
                "server": "AlarmStream"
            }
        },
        "EventStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "EventStream",
                "server": "EventStream"
            }
        },
        "lmc": {
            "1": {
                "properties": {
                    #    "member_list": ["tiles", "racks"],
                    #    "tiles": ["{domain}/tile/1"],
                    #    "racks": ["{domain}/rack/1"],
                    "data_path": "/opt/aavs/data",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "logging_target": ["device::test/logger/1"],
                    "logging_level": "DEBUG",
                    "rack_id": "1"
                },
                "class": "LMC_DS",
                "server": "LMC_DS"
            }
        },
        "rack": {
            "1": {
                "properties": {
                    "tiles": ["{domain}/tile/3"],
                    # "tiles": ["{domain}/tile/1", "{domain}/tile/2", "{domain}/tile/3", "{domain}/tile/4",
                    #           "{domain}/tile/5", "{domain}/tile/6", "{domain}/tile/7", "{domain}/tile/8"],
                    "servers": ["{domain}/server/1"],
                    "pdus": ["{domain}/pdu/1"],
                    "switches": ["{domain}/switch/1"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id": "1"
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            }
        },
        "tile": {
           "3": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "10.0.10.3",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "1",
                    "ingest_ip": "10.10.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/1",
                    "data_switch_port_core_0": "1/1/1",
                    "data_switch_port_core_4": "1/2/1",
                    "core_0_ip": "10.10.10.12",
                    "core_0_mac": "0x620000000002",
                    "core_4_ip": "10.10.10.13",
                    "core_4_mac": "0x620000000002",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
        },
        "server":
            {
                "1":
                    {
                        "properties": {
                            "ganglia_ip": "127.0.0.1",
                            "host": "127.0.0.1",
                            "monitored_metrics": "cpu_user,cpu_system",
                            "polling_frequency": "60000",

                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1",
                            "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                            "diag_mode": "True",
                            "rack_id": "1"
                        },
                        "class": "Server_DS",
                        "server": "Server_DS"
                    },
            },
        "switch": {
            "1": {
                "properties": {
                    "hostname": "10.0.10.11",
                    "polling_frequency": "600000",

                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id": "1"
                },
                "class": "Switch_DS",
                "server": "Switch_DS"
            },
        },
        "pdu":
            {
                "1":
                    {
                        "properties": {
                            "ip": "10.0.10.19",
                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1",
                            "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                            "diag_mode": "True",
                            "rack_id": "1"
                        },
                        "class": "PDU_DS",
                        "server": "PDU_DS"
                    },
            },
    }
}
