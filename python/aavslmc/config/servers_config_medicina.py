rack_config = {
    "test/tile/1": {"width": 1, "height": 4, "row": 2, "column": 1},
    "test/tile/2": {"width": 1, "height": 4, "row": 2, "column": 2},
    "test/tile/3": {"width": 1, "height": 4, "row": 2, "column": 3},
    "test/tile/4": {"width": 1, "height": 4, "row": 2, "column": 4},
    "test/tile/5": {"width": 1, "height": 4, "row": 10, "column": 1},
    "test/tile/6": {"width": 1, "height": 4, "row": 10, "column": 2},
    "test/tile/7": {"width": 1, "height": 4, "row": 10, "column": 3},
    "test/tile/8": {"width": 1, "height": 4, "row": 10, "column": 4},
    "test/pdu/1": {"width": 4, "height": 1, "row": 8, "column": 1},
    "test/switch/1": {"width": 4, "height": 1, "row": 16, "column": 1},
    "test/server/1": {"width": 4, "height": 2, "row": 17, "column": 1},
}

config = {
    "domain": "test",
    "default_logging_target": ["device::test/logger/1"],
    "default_logging_level": "DEBUG",
    "alarms_config_file": "aavslmc.config.alarms_config",
    "server_priorities":
        {
            "logger": 1,
            "Alarm": 2,
            "AlarmStream": 3,
            "EventStream": 3,
            "LMC_DS": 4,
            "Server_DS": 5,
            "Switch_DS": 5,
            "Tile_DS": 5,
            "Rack_DS": 6
        },
    "devices": {
        # "logger": {
        #     "1": {
        #         "properties": {
        #             "log_path": "/opt/aavs/log/tango"
        #         },
        #         "class": "FileLogger",
        #         "server": "logger"
        #     }
        # },
        "logger": {
            "1": {
                "properties": {
                    "log_path": "/opt/aavs/log/tango"
                },
                "class": "Logger_DS",
                "server": "Logger_DS"
            }
        },
        "Alarm": {
            "1": {
                "properties": {
                    "DbHost": "localhost",
                    "DbName": "alarm",
                    "DbPasswd": "alarm_pswd",
                    "DbPort": "3306",
                    "DbUser": "alarm_usr",
                    "InstanceName": "1",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Alarm",
                "server": "Alarm",
                "python_server": False
            }
        },
        "AlarmStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "AlarmStream",
                "server": "AlarmStream"
            }
        },
        "EventStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "EventStream",
                "server": "EventStream"
            }
        },
        "lmc": {
            "1": {
                "properties": {
                    #    "member_list": ["tiles", "racks"],
                    #    "tiles": ["{domain}/tile/1"],
                    #    "racks": ["{domain}/rack/1"],
                    "data_path": "/opt/aavs/data",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "logging_target": ["device::test/logger/1"],
                    "logging_level": "DEBUG",
                },
                "class": "LMC_DS",
                "server": "LMC_DS"
            }
        },
        "rack": {
            "1": {
                "properties": {
                    "tiles": ["{domain}/tile/1", "{domain}/tile/2", "{domain}/tile/3", "{domain}/tile/4",
                              "{domain}/tile/5", "{domain}/tile/6", "{domain}/tile/7", "{domain}/tile/8"],
                    "servers": ["{domain}/server/1"],
                    "pdus": ["{domain}/pdu/1"],
                    "switches": ["{domain}/switch/1"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            }
        },
        "tile": {
           # "1": {
           #      "properties": {
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "10.0.10.2",
           #          "port": "10000",
           #          "lmc_ip": "10.0.10.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
            "2": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.3",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "3": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.4",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "4": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.5",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "5": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.6",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "6": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.7",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "7": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.8",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
            "8": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "True",
                    "ip": "10.0.10.9",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
        },
        "server":
            {
                "1":
                    {
                        "properties": {
                            "ganglia_ip": "127.0.0.1",
                            "host": "127.0.0.1",
                            "monitored_metrics": "cpu_user,cpu_system",
                            "polling_frequency": "60000",

                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1"
                        },
                        "class": "Server_DS",
                        "server": "Server_DS"
                    },
            },
        "switch": {
            "1": {
                "properties": {
                    "hostname": "10.0.10.100",
                    "polling_frequency": "600000",

                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Switch_DS",
                "server": "Switch_DS"
            },
        },
        "pdu":
            {
                "1":
                    {
                        "properties": {
                            "ip": "10.0.10.201",
                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1"
                        },
                        "class": "PDU_DS",
                        "server": "PDU_DS"
                    },
            },
    }
}
