# alarms_config = {
#     "domain" : "test",
#     "devices": {
#         "pdu": [
#             {
#                 "attribute": "system_voltage", # "name": "pdu_[id]_[attribute]_alarm"
#                 "checks": {
#                     "minimum": 0,
#                     "maximum": 20,
#                     "absolute": 12
#                 },
#                 "poll_period": 5000 #milliseconds
#             },
#             {
#                 "attribute": "State",
#                 "checks": {
#                     "absolute": "ON"
#                 }
#             }
#         ]
#     }
# }

alarms_config = {
    "domain" : "test",
    "devices": {
        "tile": [
            {
                 # "ids": [1], # list of ids to which to apply the alarm rule, optional (applied to all group if not present)
                "attribute": "temperature_board", # "name": "tile_[id]_[attribute]_alarm"
                "poll_period": 10, #milliseconds
                "change_event_resolution": 0.1, # the absolute change that will trigger an event for an alarm check
                "alarm_on_callback": "temp_alarm_on", # the custom callback to call when the alarm goes ON
                "alarm_off_callback": "temp_alarm_off", # the custom callback to call when the alarm goes on
                "checks":
                    {
                        "maximum": 45
                    }
            },
            {
#                "ids": [1], # list of ids to which to apply the alarm rule, optional (applied to all group if not present)
                "attribute": "State",  # "name": "tile_[id]_[attribute]_alarm"
                "poll_period": 10,  # milliseconds
                "change_event_resolution": 0.5,  # the absolute change that will trigger an event for an alarm check
                "alarm_on_callback": "alarm_on",  # the custom callback to call when the alarm goes ON
                "alarm_off_callback": "alarm_off",  # the custom callback to call when the alarm goes on
                "checks":
                    {
                        "absolute": "FAULT"
                    }
            },
        ]
    }
}
