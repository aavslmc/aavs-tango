rack_config = {
    "test/tile/1": {"width": 2, "height": 1, "row": 30, "column" :14 }
}

config = {
    "domain": "test",
    "default_server": "Runner",
    "default_logging_target": ["device::test/logger/1"],
    "default_logging_level": "DEBUG",
    "server_priorities":
        {
            "logger": 1,
            "Alarm": 2 ,
            "AlarmStream": 3,
            "EventStream": 4,
            "Runner": 5
         },
    "devices": {
        "logger": {
            "1": {
                "properties": {
                    "log_path": "/opt/aavs/log/tango"
                },
                "class": "FileLogger",
                "server": "logger"
            }
        },
        "Alarm": {
            "1": {
                "properties": {
                    "DbHost": "localhost",
                    "DbName": "alarm",
                    "DbPasswd": "alarm_pswd",
                    "DbPort": "3306",
                    "DbUser": "alarm_usr",
                    "InstanceName": "1",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Alarm",
                "server": "Alarm",
                "python_server": False
            }
        },
        "AlarmStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "AlarmStream",
                "server": "AlarmStream"
            }
        },
        "EventStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "queue_length": "500"
                },
                "class": "EventStream",
                "server": "EventStream"
            }
        },
        "lmc": {
            "1": {
                "properties": {
                    "member_list": ["tiles", "racks"],
                    "tiles": ["{domain}/tile/1"],
                    "racks": ["{domain}/rack/1"],
                    "data_path": "/home/ben/workspace/AAVS/aavs-tango/data",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "LMC_DS",
                "server": "LMC_DS"
            }
        },
        "Station_DS": {
            "1": {
                "properties": {
                    "starter": "tango/admin/aavs",
                    "id": "1",
                    "tiles": ["{domain}/tile/1"],
                    "jobs": [],
                    "member_list": ["tiles", "jobs"],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "tile_bitfile": "/home/andrea/Documents/AAVS/aavs-access-layer/bitfiles/itpm_v1_1_tpm_test_wrap_lbrc86.bit",
                    "task_cycle": "raw,1;channel,1"
                },
                "class": "Station_DS",
                "server": "Station_DS"
            }
        },
        "tile": {
            "1": {
                "properties": {
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "10.62.14.102",
                    "port": "10000",
                    "lmc_ip": "10.0.10.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            }
        },
        "PointingJob_DS": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1"
                },
                "class": "PointingJob_DS",
                "server": "PointingJob_DS"
            }
        },
        "DAQJob_DS": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "task_cycle": "raw,1",
                    "job_configuration": '{"daq": {"nof_tiles": "1"}}',
                    "nof_antennas": "16",
                    "nof_channels": "512",
                    "nof_beams": "1",
                    "nof_pols": "2",
                    "nof_tiles": "1",
                    "raw_samples": "65536",
                    "channel_samples": "128",
                    "beam_samples": "64",
                    "receiver_ports": "4660,4700",
                    "receiver_interface": "eth0",
                    "tsamp": "1.1325",
                    "sampling_rate": "800e6",
                    "oversampling_factor": "32.0/27.0",
                    "channel_channel_per_packet": "1",
                    "channel_antenna_per_packet": "16",
                    "channel_samples_per_packet": "32",
                    "beam_samples_per_packet": "1",
                    "receiver_frame_size": "2048",
                    "receiver_frames_per_block": "32",
                    "receiver_nof_blocks": "256",
                    "directory": "/home/andrea/",
                    "operation_read_raw_data": "True",
                    "operation_read_beam_data": "False",
                    "operation_read_integrated_beam_data": "False",
                    "operation_read_channel_data": "False",
                    "operation_read_continuous_channel_data": "False",
                    "operation_read_integrated_channel_data": "False"
                },
                "class": "DAQJob_DS",
                "server": "DAQJob_DS",
                "server_id": "1"
            }
        },

        # "tpm": {
        #     "1": {
        #         "properties": {
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1"
        #         },
        #         "class": "TPM_DS",
        #         "server": "TPM_DS"
        #     }
        # },
        # "obsconf": {
        #     "main": {
        #         "properties": {
        #         },
        #         "class": "ObservationConfiguration",
        #         "server": "ObservationConfiguration"
        #     }
        # }
    }
}
