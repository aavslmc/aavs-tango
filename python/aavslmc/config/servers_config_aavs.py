rack_config = {
    "test/tile/1": {"width": 1, "height": 4, "row": 1, "column": 1},
    "test/tile/2": {"width": 1, "height": 4, "row": 1, "column": 2},
    "test/tile/3": {"width": 1, "height": 4, "row": 1, "column": 3},
    "test/tile/4": {"width": 1, "height": 4, "row": 1, "column": 4},
    "test/tile/5": {"width": 1, "height": 4, "row": 8, "column": 1},
    "test/tile/6": {"width": 1, "height": 4, "row": 8, "column": 2},
    "test/tile/7": {"width": 1, "height": 4, "row": 8, "column": 3},
    "test/tile/8": {"width": 1, "height": 4, "row": 8, "column": 4},
    "test/pdu/1": {"width": 4, "height": 1, "row": 5, "column": 1},
    "test/switch/1": {"width": 4, "height": 1, "row": 6, "column": 1},
    "test/switch/2": {"width": 4, "height": 1, "row": 12, "column": 1},


    "test/tile/9": {"width": 1, "height": 4, "row": 1, "column": 1},
    "test/tile/10": {"width": 1, "height": 4, "row": 1, "column": 2},
    "test/tile/11": {"width": 1, "height": 4, "row": 1, "column": 3},
    "test/tile/12": {"width": 1, "height": 4, "row": 1, "column": 4},
    "test/tile/13": {"width": 1, "height": 4, "row": 8, "column": 1},
    "test/tile/14": {"width": 1, "height": 4, "row": 8, "column": 2},
    "test/tile/15": {"width": 1, "height": 4, "row": 8, "column": 3},
    "test/tile/16": {"width": 1, "height": 4, "row": 8, "column": 4},
    "test/pdu/2": {"width": 4, "height": 1, "row": 5, "column": 1},
    "test/switch/3": {"width": 4, "height": 1, "row": 6, "column": 1},
    "test/switch/4": {"width": 4, "height": 1, "row": 12, "column": 1},

    "test/tile/17": {"width": 1, "height": 4, "row": 1, "column": 1},
    "test/tile/18": {"width": 1, "height": 4, "row": 1, "column": 2},
    "test/tile/19": {"width": 1, "height": 4, "row": 1, "column": 3},
    "test/tile/20": {"width": 1, "height": 4, "row": 1, "column": 4},
    "test/tile/21": {"width": 1, "height": 4, "row": 8, "column": 1},
    "test/tile/22": {"width": 1, "height": 4, "row": 8, "column": 2},
    "test/tile/23": {"width": 1, "height": 4, "row": 8, "column": 3},
    "test/tile/24": {"width": 1, "height": 4, "row": 8, "column": 4},
    "test/pdu/3": {"width": 4, "height": 1, "row": 5, "column": 1},
    "test/switch/5": {"width": 4, "height": 1, "row": 6, "column": 1},
    "test/switch/6": {"width": 4, "height": 1, "row": 12, "column": 1},

    "test/tile/25": {"width": 1, "height": 4, "row": 1, "column": 1},
    "test/pdu/4": {"width": 4, "height": 1, "row": 5, "column": 1},
    "test/switch/7": {"width": 4, "height": 1, "row": 12, "column": 1},
    "test/server/1": {"width": 4, "height": 2, "row": 14, "column": 1}
}

config = {
    "use_tango_logger": False,
    "domain": "test",
    "tango_default_logging_target": "file::/opt/aavs/log/tango/",
    "default_logging_target": ["device::test/logger/1"],
    "default_logging_level": "DEBUG",
    "alarms_config_file": "aavslmc.config.alarms_config",
    "server_priorities":
        {
            "Logger_DS": 1,
            "Alarm": 2,
            "AlarmStream": 3,
            "EventStream": 3,
            "DiagnosticsStream": 4,
            "LMC_DS": 5,
            "Server_DS": 6,
            # "Switch_DS": 6,
            "Tile_DS": 6,
            "Rack_DS": 7
        },
    "devices": {
        # "logger": {
        #     "1": {
        #         "properties": {
        #             "log_path": "/opt/aavs/log/tango"
        #         },
        #         "class": "FileLogger",
        #         "server": "logger"
        #     }
        # },
        "logger": {
            "1": {
                "properties": {
                    "log_path": "/opt/aavs/log/tango"
                },
                "class": "Logger_DS",
                "server": "Logger_DS"
            }
        },
        "Alarm": {
            "1": {
                "properties": {
                    "DbHost": "localhost",
                    "DbName": "alarm",
                    "DbPasswd": "alarm_pswd",
                    "DbPort": "3306",
                    "DbUser": "alarm_usr",
                    "InstanceName": "1",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "Alarm",
                "server": "Alarm",
                "python_server": False
            }
        },
        "AlarmStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "AlarmStream",
                "server": "AlarmStream"
            }
        },
        "EventStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "EventStream",
                "server": "EventStream"
            }
        },
        "DiagnosticsStream": {
            "1": {
                "properties": {
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True"
                },
                "class": "DiagnosticsStream",
                "server": "DiagnosticsStream"
            }
        },
        "lmc": {
            "1": {
                "properties": {
                    "data_path": "/opt/aavs/data",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "use_tango_logger": "False"
                },
                "class": "LMC_DS",
                "server": "LMC_DS"
            }
        },
        "rack": {
            "1": {
                "properties": {
                     "tiles": ["{domain}/tile/1", "{domain}/tile/2", "{domain}/tile/3", "{domain}/tile/4",
                               "{domain}/tile/5", "{domain}/tile/6", "{domain}/tile/7", "{domain}/tile/8"],
                    "servers": ["{domain}/server/1"],
                    "pdus": ["{domain}/pdu/1"],
                    "switches": ["{domain}/switch/1", "{domain}/switch/2"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id" : "1",
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            },
            "2": {
                "properties": {
                     "tiles": ["{domain}/tile/9", "{domain}/tile/10", "{domain}/tile/11", "{domain}/tile/12",
                               "{domain}/tile/13", "{domain}/tile/14", "{domain}/tile/15", "{domain}/tile/16"],
                    "servers": [],
                    "pdus": ["{domain}/pdu/2"],
                    "switches": ["{domain}/switch/3", "{domain}/switch/4"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id" : "2",
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            },
            "3": {
                "properties": {
                     "tiles": ["{domain}/tile/17", "{domain}/tile/18", "{domain}/tile/19", "{domain}/tile/20",
                               "{domain}/tile/21", "{domain}/tile/22", "{domain}/tile/23", "{domain}/tile/24"],
                    "servers": [],
                    "pdus": ["{domain}/pdu/3"],
                    "switches": ["{domain}/switch/5", "{domain}/switch/6"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id" : "3",
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            },
            "4": {
                "properties": {
                     "tiles": ["{domain}/tile/25"],
                    "servers": [],
                    "pdus": ["{domain}/pdu/4"],
                    "switches": ["{domain}/switch/7"],
                    "member_list": ["tiles", "servers", "switches", "pdus", ],
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "rack_id" : "4",
                },
                "class": "Rack_DS",
                "server": "Rack_DS",
                "components": rack_config
            }
        },
        "tile": {
           "1": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-1",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "0",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/1",
                    "data_switch_port": "1/1/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "2": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-2",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "2",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/1",
                    "data_switch_port": "1/2/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "3": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-3",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "3",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/1",
                    "data_switch_port": "1/3/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "4": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-4",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "4",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/1",
                    "data_switch_port": "1/4/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "5": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-5",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "7",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/2",
                    "data_switch_port": "1/1/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "6": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-6",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "8",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/2",
                    "data_switch_port": "1/2/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "7": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-7",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "9",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/2",
                    "data_switch_port": "1/3/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "8": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-8",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/1",
                    "pdu_interface": "10",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/2",
                    "data_switch_port": "1/4/1",
                    "rack_id": "1"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "9": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-9",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "1",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/3",
                    "data_switch_port": "1/1/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "10": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-10",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "2",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/3",
                    "data_switch_port": "1/2/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "11": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-11",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "3",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/3",
                    "data_switch_port": "1/3/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "12": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-12",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "4",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/3",
                    "data_switch_port": "1/4/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "13": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-13",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "7",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/4",
                    "data_switch_port": "1/1/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "14": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-14",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "8",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/4",
                    "data_switch_port": "1/2/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "15": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-15",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu/2",
                    "pdu_interface": "9",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/4",
                    "data_switch_port": "1/3/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS"
            },
           "16": {
                "properties": {
                    "member_list": ["antennas"],
                    "enable_test": "False",
                    "enable_ada": "False",
                    "ip": "tpm-16",
                    "port": "10000",
                    "lmc_ip": "127.0.0.1",
                    "lmc_port": "4660",
                    "elettra_alarm_device_address": "{domain}/Alarm/1",
                    "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                    "central_eventstream_device_address": "{domain}/EventStream/1",
                    "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                    "diag_mode": "True",
                    "pdu_id": "{domain}/pdu-2",
                    "pdu_interface": "10",
                    "ingest_ip": "10.0.10.10", # station
                    "ingest_mac": "0x620000000002", # station
                    "ingest_port": "4000", # station
                    "data_switch_id": "{domain}/switch/4",
                    "data_switch_port": "1/4/1",
                    "rack_id": "2"
                },
                "class": "Tile_DS",
                "server": "Tile_DS" 
            },
           # "17": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-17",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "1",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/5",
           #          "data_switch_port": "1/1/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "18": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-18",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "2",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/5",
           #          "data_switch_port": "1/12/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "19": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-19",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "3",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/5",
           #          "data_switch_port": "1/3/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "20": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-20",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "4",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/5",
           #          "data_switch_port": "1/4/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "21": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-21",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "7",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/6",
           #          "data_switch_port": "1/1/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "22": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-22",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "8",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/6",
           #          "data_switch_port": "1/2/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "23": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-23",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "9",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/6",
           #          "data_switch_port": "1/3/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           # "24": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-24",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-3",
           #          "pdu_interface": "10",
           #          "ingest_ip": "10.0.10.10", # station
           #          "ingest_mac": "0x620000000002", # station
           #          "ingest_port": "4000", # station
           #          "data_switch_id": "{domain}/switch/6",
           #          "data_switch_port": "1/4/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
           #  "25": {
           #      "properties": {
           #          "member_list": ["antennas"],
           #          "enable_test": "False",
           #          "enable_ada": "False",
           #          "ip": "tpm-25",
           #          "port": "10000",
           #          "lmc_ip": "127.0.0.1",
           #          "lmc_port": "4660",
           #          "elettra_alarm_device_address": "{domain}/Alarm/1",
           #          "central_alarmstream_device_address": "{domain}/AlarmStream/1",
           #          "central_eventstream_device_address": "{domain}/EventStream/1",
           #          "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
           #          "diag_mode": "True",
           #          "pdu_id": "{domain}/pdu-4",
           #          "pdu_interface": "1",
           #          "ingest_ip": "10.0.10.10",  # station
           #          "ingest_mac": "0x620000000002",  # station
           #          "ingest_port": "4000",  # station
           #          "data_switch_id": "{domain}/switch/7",
           #          "data_switch_port": "1/1/1",
           #          "rack_id": "3"
           #      },
           #      "class": "Tile_DS",
           #      "server": "Tile_DS"
           #  },
        },
        "server":
            {
                "1":
                    {
                        "properties": {
                            "ganglia_ip": "127.0.0.1",
                            "host": "127.0.0.1",
                            "monitored_metrics": "cpu_user,cpu_system",
                            "polling_frequency": "60000",

                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1",
                            "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                            "diag_mode": "True",
                    "rack_id": "2"
                        },
                        "class": "Server_DS",
                        "server": "Server_DS"
                    },
            },
        # "switch": {
        #     "1": {
        #         "properties": {
        #             "hostname": "sw40g-1",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "1"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "2": {
        #         "properties": {
        #             "hostname": "sw40g-2",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "1"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "3": {
        #         "properties": {
        #             "hostname": "sw40g-3",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "2"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "4": {
        #         "properties": {
        #             "hostname": "sw40g-4",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "2"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "5": {
        #         "properties": {
        #             "hostname": "sw40g-5",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "3"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "6": {
        #         "properties": {
        #             "hostname": "sw40g-6",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "3"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        #     "7": {
        #         "properties": {
        #             "hostname": "sw40g-7",
        #             "polling_frequency": "600000",
        #
        #             "elettra_alarm_device_address": "{domain}/Alarm/1",
        #             "central_alarmstream_device_address": "{domain}/AlarmStream/1",
        #             "central_eventstream_device_address": "{domain}/EventStream/1",
        #             "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
        #             "diag_mode": "True",
        #             "rack_id": "4"
        #         },
        #         "class": "Switch_DS",
        #         "server": "Switch_DS"
        #     },
        # },
        "pdu":
            {
                "1":{
                        "properties": {
                            "ip": "pdu-1",
                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1",
                            "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                            "diag_mode": "True",
                    "rack_id": "1"
                        },
                        "class": "PDU_DS",
                        "server": "PDU_DS"
                    },
                "2":{
                        "properties": {
                            "ip": "pdu-2",
                            "elettra_alarm_device_address": "{domain}/Alarm/1",
                            "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                            "central_eventstream_device_address": "{domain}/EventStream/1",
                            "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                            "diag_mode": "True",
                    "rack_id": "2"
                        },
                        "class": "PDU_DS",
                        "server": "PDU_DS"
                    },
                "3": {
                    "properties": {
                        "ip": "pdu-3",
                        "elettra_alarm_device_address": "{domain}/Alarm/1",
                        "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                        "central_eventstream_device_address": "{domain}/EventStream/1",
                        "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                        "diag_mode": "True",
                        "rack_id": "3"
                    },
                    "class": "PDU_DS",
                    "server": "PDU_DS"
                },
                "4": {
                    "properties": {
                        "ip": "pdu-4",
                        "elettra_alarm_device_address": "{domain}/Alarm/1",
                        "central_alarmstream_device_address": "{domain}/AlarmStream/1",
                        "central_eventstream_device_address": "{domain}/EventStream/1",
                        "central_diagstream_device_address": "{domain}/DiagnosticsStream/1",
                        "diag_mode": "True",
                        "rack_id": "4"
                    },
                    "class": "PDU_DS",
                    "server": "PDU_DS"
                },
            },
    }
}
