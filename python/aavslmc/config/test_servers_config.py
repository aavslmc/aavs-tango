config = {
    "domain": "test",
    "default_server": "Runner",
    "devices": {
        "lmc": {
            "1": {
                "properties": {
                    "member_list": ["tiles"],
                    "tiles": ["test/tile/1"]
                },
                "server": "LMC_DS",
                "class": "LMC_DS"
            }
        },
        "tile": {
            "1": {
                "properties": {},
                "class": "Tile_DS",
                "server": "Tile_DS"
                }
            },

    }
}
