"""
Configuration for bootstrapping tango device drivers
"""
import importlib

# change this value to which configuration file you want to use
CONFIG_FILE = "aavslmc.config.servers_config_aavs"
# CONFIG_FILE = "aavslmc.config.servers_config"
# CONFIG_FILE = "aavslmc.config.servers_config_simple"
# CONFIG_FILE = "aavslmc.config.servers_config_andrea"
# CONFIG_FILE = "aavslmc.config.servers_config_medicina"

config_module = importlib.import_module(CONFIG_FILE)
config = config_module.config
