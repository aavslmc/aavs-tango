from PyTango import Database, DbDevInfo, DeviceProxy
import platform
import signal
import os

def get_dev_info(device_name):
    """ Get device information """
    try:
        dev_info = db.get_device_info(device_name)
    except Exception as ex:
        dev_info = None
    return dev_info


def start_device_server(device_class, domain, device_name):
    """ Start device server """
    dev_info = DbDevInfo()
    dev_info._class = device_class
    dev_info.server = '%s/%s' % (device_class, domain)
    dev_info.name = device_name
    db = Database()
    db.add_device(dev_info)

    dp = DeviceProxy("tango/admin/%s" % platform.uname()[1])

    # if dev_info.server in dp.RunningServers:
    #     dp.DevRestart(dev_info.server)
    # else:
    dp.DevStart(dev_info.server)

def add_device(domain, device_server_name, device_id):
    """ Add device to database"""
    group_name = device_server_name.lower().replace('_ds', '')
    device_name = '%s/%s/%s' % (domain, group_name, device_id)
    kill_device_servers(device_name)
    dev_info = get_dev_info(device_name)

    if not dev_info:
        dev_info = DbDevInfo()
        dev_info._class = device_server_name
        dev_info.server = '%s/%s' % (device_server_name, domain)
        # add the device
        dev_info.name = device_name
        print("Creating device: %s" % device_name)
        db = Database()
        db.add_device(dev_info)


def kill_device_servers(device_name):
    """ Kill device servers """
    try:
        db = Database()
        dev_info = db.get_device_info(device_name)
        if dev_info.pid != 0:
            os.kill(dev_info.pid, signal.SIGTERM)  # o r signal.SIGKILL
        print "Killed PID: %s" % dev_info.pid
    except Exception as ex:
        print "No process to kill for %s" % device_name


def extract_alarm_info(configured_alarm):
    """ Extract alarm information """
    alarm_parts = configured_alarm.split("\t")
    name = alarm_parts[1]
    alarm_parts[7] = "\"" + alarm_parts[7] + "\""
    return name, " ".join(alarm_parts[1:])


def reset_alarms(alarm_proxy, device_name=""):
    """ Reset alarms """
    configured_alarms = alarm_proxy.Configured(device_name)
    for alarm in configured_alarms:
        name, definition = extract_alarm_info(alarm)

        try:
            alarm_proxy.Ack([name])
        except Exception as e:
            pass
        try:
            alarm_proxy.Remove(name)
        except Exception as e:
            pass


if __name__ == "__main__":
    import PyTango

    al = PyTango.DeviceProxy("test/alarm/1")
    name = "test/rack/1/members_state_alarm"
    reset_alarms(al, name)
