from time import sleep
import json
import os, signal
from subprocess import Popen, PIPE
from base import DeviceServerBaseTest
from PyTango import DeviceProxy, DbDatum, DevState
from aavslmc.library.helpers import check_job_running, get_device_group_and_id, wait_seconds

class JobTest(DeviceServerBaseTest):
    REQUIRED_DEVICE_SERVERS = ['Observation_DS']

    @classmethod
    def add_devices(cls):
        cls.add_device('Observation_DS', 'observation/1')
        cls.add_device('Observation_DS', 'observation/2')

    @classmethod
    def remove_devices(cls):
        cls.remove_device('observation/1')
        cls.remove_device('observation/2')

    mock_telescope_model = {
        'jobs': {
            'PointingJob':
                {
                    'cwd': os.path.join(DeviceServerBaseTest.device_server_path, '../aavslmc/tests'),
                    'cmd': ['python', 'jobtestrun.py']
                }
        }
    }

    def assert_command(self, dp, cmd_name, argin=None):
        try:
            if argin:
                return dp.command_inout(cmd_name, argin)
            return dp.command_inout(cmd_name)
        except Exception, ex:
            return False

    def test_jobs(self):
        """
        Testing jobs created from within an observation
        :return:
        """

        try:
            dp = DeviceProxy('%s/observation/1' % self.DOMAIN_NAME)
            self.assert_command(dp, 'init_observation', json.dumps(self.mock_telescope_model))
            job_device_names = dp.get_property('jobs')['jobs']
            self.assertEqual(len(job_device_names), 1)
            # the startswith python matcher is required
            # so that we don't catch the check running poll inside the job which starts with pgrep
            jobtestrun = ' '.join(['^python', 'jobtestrun.py', job_device_names[0]])

            job_dp = DeviceProxy(job_device_names[0])

            self.assert_command(job_dp, 'start')
            output = check_job_running(jobtestrun)
            self.assertEqual(len(output), 1)
            self.assertEqual(job_dp.state(), DevState.RUNNING)

            self.assert_command(job_dp, 'stop')
            output = check_job_running(jobtestrun)
            self.assertEqual(job_dp.state(), DevState.STANDBY)
            self.assertEqual(len(output), 0)

        finally:
            # If this fails, open jive and clean up the resources under Observation_DS/unittest
            # The issue happens because polling is still enabled on the job
            dp = DeviceProxy('%s/observation/1' % self.DOMAIN_NAME)
            dp.command_inout('stop_observation')

    def test_bad_job_propagates_alarm(self):
        try:
            dp = DeviceProxy('%s/observation/2' % self.DOMAIN_NAME)
            self.assert_command(dp, 'init_observation', json.dumps(self.mock_telescope_model))
            job_device_names = dp.get_property('jobs')['jobs']
            print job_device_names
            jobtestrun = ' '.join(['^python', 'jobtestrun.py', job_device_names[0]])
            self.assertEqual(len(job_device_names), 1)

            job_dp = DeviceProxy(job_device_names[0])

            self.assert_command(job_dp, 'start')
            output = check_job_running(jobtestrun)
            self.assertEqual(len(output), 1, output)
            self.assertEqual(job_dp.state(), DevState.RUNNING)

            # job process was killed by external conditions
            print 'killing ', output
            os.kill(int(output[0]), signal.SIGKILL)

            wait_seconds(job_dp, max=5)
            self.assertEqual(job_dp.state(), DevState.ALARM)
            wait_seconds(dp, max=5)
            self.assertEqual(dp.state(), DevState.ALARM)

        finally:
            # If this fails, open jive and clean up the resources under Observation_DS/unittest
            # The issue happens because polling is still enabled on the job
            dp = DeviceProxy('%s/observation/2' % self.DOMAIN_NAME)
            self.assert_command(dp, 'stop_observation')

