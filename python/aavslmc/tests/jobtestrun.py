from time import sleep


def run_for_while(seconds=240):
    left = seconds
    while left > 0:
        # print 'sleeping from job'
        sleep(1)

if __name__ == '__main__':
    run_for_while()