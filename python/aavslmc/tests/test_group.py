import PyTango
from PyTango import DeviceProxy, AttrQuality, AttributeProxy
from base import DeviceServerBaseTest
import json
from time import sleep

class TestGroup(DeviceServerBaseTest):
    REQUIRED_DEVICE_SERVERS = ['Station_DS', 'Tile_DS', 'TPM_DS']

    @classmethod
    def add_devices(cls):
        cls.add_device('Station_DS', 'station/1')
        cls.add_device('Tile_DS', 'tile/1')
        cls.add_device('Tile_DS', 'tile/2')
        cls.add_device('TPM_DS', 'tpm_board/1')
        cls.add_device('TPM_DS', 'tpm_board/2')

    @classmethod
    def remove_devices(cls):
        cls.remove_device('station/1')
        cls.remove_device('tile/1')
        cls.remove_device('tile/2')
        cls.remove_device('tpm_board/1')
        cls.remove_device('tpm_board/2')

    def test_add_member(self):
        device_name = '%s/station/1' % self.DOMAIN_NAME
        group_name = 'tiles'
        # self.stop_device_server('Station_DS')
        # self.start_device_server('Station_DS')
        dp = DeviceProxy(device_name)
        dp.set_logging_level(5)
        member_device_name_1 = '%s/tile/1' % self.DOMAIN_NAME
        data_1 = json.dumps({'group_name': group_name, 'device_name': member_device_name_1 })
        dp.command_inout('add_member', data_1)
        self.assertTrue(member_device_name_1 in dp.get_property(group_name)[group_name])

        member_device_name_2 = '%s/tile/2' % self.DOMAIN_NAME
        data_2 = json.dumps({'group_name': group_name, 'device_name': member_device_name_2 })
        dp.command_inout('add_member', data_2)

        tiles = dp.get_property(group_name)[group_name]
        self.assertTrue(member_device_name_2 in tiles)
        self.assertEqual(len(tiles), 2)
        dp.command_inout('remove_member', data_2)

        tiles = dp.get_property(group_name)[group_name]
        self.assertFalse(member_device_name_2 in tiles)
        self.assertEqual(len(tiles), 1)

        dp.command_inout('remove_member', data_1)
        self.assertFalse(member_device_name_1 in dp.get_property(group_name)[group_name])

    def add_members(self, device_name, group_name, members=None):
        dp = DeviceProxy(device_name)
        for member in members:
            data = json.dumps({'group_name': group_name, 'device_name': member })
            dp.command_inout('add_member', data)

    def test_alarm_propagation(self):
        device_name = '%s/station/1' % self.DOMAIN_NAME
        members_template = ['%s/tile/1', '%s/tile/2']
        members = [member % self.DOMAIN_NAME for member in members_template]

        self.add_members(device_name, 'tiles', members)
        dp = DeviceProxy(device_name)
        self.assertNotEqual(dp.state(), AttrQuality.ATTR_ALARM)
        member_dp = DeviceProxy(members[0])
        alarm_data = json.dumps({'name': 'lmc_port', 'min_alarm': '20', 'max_alarm': '50'})
        member_dp.command_inout('set_attribute_alarm', alarm_data)
        member_dp.lmc_port = 10
        attr = AttributeProxy(members[0] + '/lmc_port')
        self.assertEqual(attr.read().quality, AttrQuality.ATTR_ALARM)
        self.assertEqual(member_dp.state(), PyTango._PyTango.DevState.ALARM)
        i = 0
        while (dp.state() != PyTango._PyTango.DevState.ALARM) and i < 3:
            sleep(1)
            i+=1

        self.assertEqual(dp.state(), PyTango._PyTango.DevState.ALARM)
