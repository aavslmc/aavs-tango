from PyTango import DeviceProxy, Database, DbDevInfo, DbDatum, DevState
from base import  DeviceServerBaseTest
from unittest import  TestCase


from time import sleep
class TestSwitchON(DeviceServerBaseTest):

        REQUIRED_DEVICE_SERVERS = ['Switch_DS']

        @classmethod
        def add_devices(cls):

            cls.add_device('Switch_DS', 'switch/1')
            cls.add_device_prop('switch/1', {"host":"10.62.14.55"})

        @classmethod
        def remove_devices(cls):
            cls.remove_device( 'switch/1')

        def test_switch_status(self):

            device_name = '%s/switch/1' % self.DOMAIN_NAME
            dp = DeviceProxy(device_name)
            self.assertEqual(dp.state(), DevState.ON )
            self.assertEqual(dp.status(), 'Device is operational')


class TestSwitchBadIp(DeviceServerBaseTest):

        REQUIRED_DEVICE_SERVERS = ['Switch_DS']

        @classmethod
        def add_devices(cls):

            cls.add_device('Switch_DS', 'switch/1')
            cls.add_device_prop('switch/1', {"host":"10.62.14.54"})

        @classmethod
        def remove_devices(cls):
            cls.remove_device( 'switch/1')

        def test_switch_status(self):

            device_name = '%s/switch/1' % self.DOMAIN_NAME
            dp = DeviceProxy(device_name)
            self.assertEqual(dp.state(), DevState.FAULT )
            print dp.status()
            # self.assertEqual(dp.status(), 'Device is operational')


# class TestSwitchDB(TestCase):
#
#     def test_add_device_to_db(self):
#         db = Database()
#         dev_info = DbDevInfo()
#
#         domain = 'bentest'
#         device_class = "Switch_DS"
#         device_name  = "switch/2"
#
#         full_device_name = "%s/%s" % (domain , device_name)
#
#         dev_info.server = '%s/%s' % (device_class, domain)
#         dev_info._class = device_class
#         dev_info.name =  full_device_name
#
#         #device property
#         db_datum = DbDatum()
#         db_datum.name = "host"
#         db_datum.value_string.append("10.62.14.54")
#         db.put_device_property(full_device_name, db_datum)
#
#         db.add_device(dev_info)
#
#


#
