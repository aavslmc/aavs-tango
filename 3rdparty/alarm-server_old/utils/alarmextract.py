# -*- coding: utf-8 -*-
from PyTango import *
import sys

if len(sys.argv) != 2:
	print "Usage:", sys.argv[0], "<alarm_device_name>"
	sys.exit(0)

dev=DeviceProxy(sys.argv[1])
ans=dev.command_inout('Configured','')
print "sec\tname\tformula\ttime-threshold\tlevel\tsilent-time\tgrp\tmsg\tcmd_name_a\tcmd_name"
for a in ans:
	print a
