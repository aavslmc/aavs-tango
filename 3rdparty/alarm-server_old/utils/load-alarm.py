#!/usr/bin/python
#
# stress.py
# stress test for tango alarms
#
# LP - 23.03.2005
#
from PyTango import *
import time
import string
import sys

if len(sys.argv) != 3:
	print "Usage:", sys.argv[0], "<server>" "<alarm>"
	sys.exit(0)

dev = DeviceProxy(sys.argv[1])
dev.command_inout('Load', sys.argv[2])

# EOF
