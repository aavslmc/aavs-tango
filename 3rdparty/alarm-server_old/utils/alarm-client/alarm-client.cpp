/*
 * alarm-client.cpp
 *
 * $Author: claudio $
 *
 * $Revision: 1.2 $
 *
 * copyleft: Sincrotrone Trieste S.C.p.A. di interesse nazionale
 *           Strada Statale 14 - km 163,5 in AREA Science Park
 *           34012 Basovizza, Trieste ITALY
 */

#include <tango.h>
#include <string>

class AlarmCallback : public Tango::CallBack
{
	void push_event(Tango::EventData *);
};


void AlarmCallback::push_event(Tango::EventData *myevent)
{
	vector<string> s;
	
	// cout << "In push_event" << endl;
	// cout << "myevent->attr_name = " << myevent->attr_name << endl;
	
	try {
		if (!myevent->err) {
			cout << endl;
			*myevent->attr_value >> s;
			vector<string>::iterator si = s.begin();
			int i = 0;
			while (si != s.end()) {
				cout << i << " - " << *si << endl;
				si++;
				i++;
			}
		} else {
			cout << "Server error" << endl;
		}
	} catch (...) {
		cout << "Errore che non si sa" << endl;
	}
}


int main(int argc, char *argv[])
{
	AlarmCallback cb;
	vector<string> filtro;
	int eid;

	Tango::DeviceProxy *device = new Tango::DeviceProxy(argv[1]);

	try {
		eid = device->subscribe_event("alarm", Tango::CHANGE_EVENT, &cb, filtro);
	} catch (...) {
		cout << "subscribe_event" << endl;
		exit(0);
	}
	cout << "Registrato evento eid = " << eid << endl;
	
	while (true) {
		sleep(20);
	}
	device->unsubscribe_event(eid);
	delete device;
}

// EOF
