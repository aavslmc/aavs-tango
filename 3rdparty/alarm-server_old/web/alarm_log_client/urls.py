from django.conf.urls.defaults import *
from alarm_log_client.alarms.views import view_list
from alarm_log_client.alarms.views import view_list_redirect
from alarm_log_client.alarms.views import view_description
from alarm_log_client.alarms.views import view_descr_redirect

urlpatterns = patterns('',
    # Example:
    # (r'^mysite/', include('mysite.foo.urls')),
     
     (r'^list/$',view_list_redirect),
     (r'^alarm/$',view_list_redirect),
     #(r'^list/$','django.views.generic.simple.redirect_to',{'url': '/list/1/'}),
     #(r'^list/$',view_list, {'view_id': 1}),
     (r'^list/(?P<view_id>\d+)/$',view_list),
     #(r'^list/(\d+)/$',view_list),	     
     #(r'^descr/$',view_description),
     (r'^descr/$',view_descr_redirect),
     (r'^descr/(?P<view_id>\d+)/$',view_description),
     (r'^pictures/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/graziano/webapp/mysite/pictures'}),    #only for development use (see static files documentation)
    # Uncomment this for admin:
     (r'^admin/', include('django.contrib.admin.urls')),
)
