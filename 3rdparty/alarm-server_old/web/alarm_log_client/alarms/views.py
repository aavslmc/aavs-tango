# Create your views here.
from django.template import Context
from django.shortcuts import render_to_response
from alarm_log_client.alarms.models import alarms
from alarm_log_client.alarms.models import description
from django.http import HttpResponseRedirect
import datetime
import time

#from django.core.urlresolvers import reverse

def view_list_redirect(request):

	response = HttpResponseRedirect('/list/1/')		#TODO: get url from view
	response.delete_cookie('filter_name')
	response.delete_cookie('filter_status')
	response.delete_cookie('filter_ack')
	response.delete_cookie('filter_level')
	response.delete_cookie('filter_grp')
	response.delete_cookie('filter_year_from')
	response.delete_cookie('filter_month_from')
	response.delete_cookie('filter_day_from')
	response.delete_cookie('filter_year_to')
	response.delete_cookie('filter_month_to')
	response.delete_cookie('filter_day_to')
	response.delete_cookie('order_date')
	return response

def view_list(request, view_id):
	now = datetime.datetime.now()
	name_list = description.objects.values("name").distinct()
	status_list = alarms.objects.values("status").distinct()
	ack_list = alarms.objects.values("ack").distinct()
	level_list = description.objects.values("level").distinct()
	grp_list = description.objects.values("grp").distinct()
#	alarms_list = alarms.objects.all()
	ts_from = 0
	ts_to = 0
	
	alarms_list = alarms.objects.select_related()
	if request.method == "GET":
		filter_name = request.GET.get('filter_name', request.COOKIES.get('filter_name', ''))
		filter_status = request.GET.get('filter_status',request.COOKIES.get('filter_status',''))
		filter_ack = request.GET.get('filter_ack',request.COOKIES.get('filter_ack',''))
		filter_level = request.GET.get('filter_level',request.COOKIES.get('filter_level',''))
		filter_grp = request.GET.get('filter_grp',request.COOKIES.get('filter_grp',''))
		filter_year_from = request.GET.get('year_from',request.COOKIES.get('filter_year_from',''))
		filter_month_from = request.GET.get('month_from',request.COOKIES.get('filter_month_from',''))
		filter_day_from = request.GET.get('day_from',request.COOKIES.get('filter_day_from',''))
		filter_year_to = request.GET.get('year_to',request.COOKIES.get('filter_year_to',''))
		filter_month_to = request.GET.get('month_to',request.COOKIES.get('filter_month_to',''))
		filter_day_to = request.GET.get('day_to',request.COOKIES.get('filter_day_to',''))
#		view_id = 1			#if starting filter, go to page 1 of results
	elif request.method == "POST":
		filter_name = request.POST.get('filter_name', request.COOKIES.get('filter_name', ''))
		filter_status = request.POST.get('filter_status',request.COOKIES.get('filter_status',''))
		filter_ack = request.POST.get('filter_ack',request.COOKIES.get('filter_ack',''))
		filter_level = request.POST.get('filter_level',request.COOKIES.get('filter_level',''))
		filter_grp = request.POST.get('filter_grp',request.COOKIES.get('filter_grp',''))	
		filter_year_from = request.POST.get('year_from',request.COOKIES.get('filter_year_from',''))
		filter_month_from = request.POST.get('month_from',request.COOKIES.get('filter_month_from',''))
		filter_day_from = request.POST.get('day_from',request.COOKIES.get('filter_day_from',''))
		filter_year_to = request.POST.get('year_to',request.COOKIES.get('filter_year_to',''))
		filter_month_to = request.POST.get('month_to',request.COOKIES.get('filter_month_to',''))
		filter_day_to = request.POST.get('day_to',request.COOKIES.get('filter_day_to',''))
#		view_id = 1			#if starting filter, go to page 1 of results

	if filter_name:
#		alarms_list = alarms_list.filter(id_description=filter_name)
		#prova:
		id_list_name = description.objects.filter(name=filter_name)
		alarms_list = alarms_list.filter(id_description__in=id_list_name)

	if filter_status:
		alarms_list = alarms_list.filter(status=filter_status)

	if filter_ack:
		alarms_list = alarms_list.filter(ack=filter_ack)

	if filter_level:
		id_list_lev = description.objects.filter(level=filter_level)
		alarms_list = alarms_list.filter(id_description__in=id_list_lev)

	if filter_grp:
		id_list_grp = description.objects.filter(grp=filter_grp)
		alarms_list = alarms_list.filter(id_description__in=id_list_grp)

	if filter_year_from and filter_month_from and filter_day_from:
		start_date = datetime.date(int(filter_year_from), int(filter_month_from), int(filter_day_from))
		ts_from = int(time.mktime(start_date.timetuple()))		
		alarms_list = alarms_list.filter(time_sec__gte=ts_from)
	else:
		filter_year_from = ''
		filter_month_from = ''
		filter_day_from = ''

	if filter_year_to and filter_month_to and filter_day_to:
		end_date = datetime.date(int(filter_year_to), int(filter_month_to), int(filter_day_to))
		ts_to = int(time.mktime(end_date.timetuple())) + 60*60*24 - 1		#include this day
#		if ts_from > 0:
#			if ts_to < ts_from:
#				ts_to = ts_from + 60*60*24 - 1				#include this day
#				filter_year_to = filter_year_from
#				filter_month_to = filter_month_from
#				filter_day_to = filter_day_from		
		alarms_list = alarms_list.filter(time_sec__lte=ts_to)
	else:
		filter_year_to = ""
		filter_month_to = ""
		filter_day_to = ""	
	################################### ordering #######################################################
	order_date = int(request.COOKIES.get('order_date', -1))
	tmp_ord = request.GET.get('order', '')
	if tmp_ord == str('date'):
		order_date = int(order_date) * -1		#invert order	
	if order_date == 1:
		alarms_list = alarms_list.order_by('time_sec', 'time_usec')
	elif order_date == -1:
		alarms_list = alarms_list.order_by('-time_sec', '-time_usec')

	#if "view_id" in request.COOKIES:
	#	view_id = request.COOKIES["view_id"]
	#view_id = request.COOKIES.get("view_id", 0)
	################################### pagination #######################################################
	row_per_page = 15
	total_id = alarms_list.count()
	number_id = total_id // row_per_page		#20 results per page
	if number_id * row_per_page < total_id:
		number_id = number_id + 1
	if (int(view_id) > number_id) and (number_id > 0):
		view_id = str(number_id)
	if number_id == 0:
		view_id = '1'
	list_id = ['1']
	if int(view_id) == 2:
		list_id = list_id + ['2']
	elif int(view_id) == 3:
		list_id = list_id + ['2', '3']
	elif int(view_id) > 3:
		id1 = str(int(view_id) -1)
		id2 = str(int(view_id))
		list_id = list_id + ['...', id1, id2]
	
	if int(view_id) +2 < number_id:
		id1 = str(int(view_id) +1)
		list_id = list_id + [id1, '...', str(number_id)]
	elif int(view_id) +2 == number_id:
		id1 = str(int(view_id) +1)
		list_id = list_id + [id1, str(number_id)]
	elif int(view_id) +1 == number_id:
		list_id = list_id + [str(number_id)]		
		
	start_row = (int(view_id) - 1) * row_per_page
	end_row = int(view_id) * row_per_page
	
	table_id = { 'list_id': list_id, 'sel_id': str(view_id), 'count': total_id}
	if int(view_id) == 1:
		alarms_list = alarms_list[:row_per_page]
	elif int(view_id) == number_id:
		alarms_list = alarms_list[start_row:total_id]
	else:
		alarms_list = alarms_list[start_row:end_row]	


	selected = { 'name_sel': filter_name, 'status_sel': filter_status, 'ack_sel': filter_ack, 'level_sel': filter_level,'grp_sel': filter_grp, 'year_from_sel': filter_year_from,'month_from_sel': filter_month_from,'day_from_sel': filter_day_from,'year_to_sel': filter_year_to,'month_to_sel': filter_month_to,'day_to_sel': filter_day_to }
	
	combo_list = { 'name_list': name_list, 'status_list': status_list, 'ack_list': ack_list, 'level_list': level_list,'grp_list': grp_list}
	ordering = {'order_date': order_date}

#Entry.objects.filter(pub_date__range=(start_date, end_date))	#--> SELECT ... WHERE pub_date BETWEEN '2005-01-01' and '2005-03-31';					
	response=render_to_response('list.html',{'alarms_list': alarms_list, 'current_date': now, 'combo_list': combo_list,'selected': selected, 'table_id': table_id, 'ordering': ordering})
	response.set_cookie('filter_name',value=filter_name)
	response.set_cookie('filter_status',value=filter_status)
	response.set_cookie('filter_ack',value=filter_ack)
	response.set_cookie('filter_level',value=filter_level)
	response.set_cookie('filter_grp',value=filter_grp)
	response.set_cookie('filter_year_from',value=filter_year_from)
	response.set_cookie('filter_month_from',value=filter_month_from)
	response.set_cookie('filter_day_from',value=filter_day_from)
	response.set_cookie('filter_year_to',value=filter_year_to)
	response.set_cookie('filter_month_to',value=filter_month_to)
	response.set_cookie('filter_day_to',value=filter_day_to)
	response.set_cookie('order_date',value=order_date)
	return response
	
#############################################################################################################
########################################### DESCRIPTION #####################################################
#############################################################################################################
	
def view_descr_redirect(request):

	response = HttpResponseRedirect('/descr/1/')		#TODO: get url from view
	response.delete_cookie('filter_name_d')
	response.delete_cookie('filter_instance_d')
	response.delete_cookie('filter_active_d')
	response.delete_cookie('filter_level_d')
	response.delete_cookie('filter_grp_d')
	response.delete_cookie('filter_year_from_d')
	response.delete_cookie('filter_month_from_d')
	response.delete_cookie('filter_day_from_d')
	response.delete_cookie('filter_year_to_d')
	response.delete_cookie('filter_month_to_d')
	response.delete_cookie('filter_day_to_d')
	response.delete_cookie('order_date_d')
	return response

def view_description(request, view_id):
	now = datetime.datetime.now()
	name_list = description.objects.values("name").distinct()
	instance_list = description.objects.values("instance").distinct()	
	active_list_tmp = description.objects.values("active").distinct()
	level_list = description.objects.values("level").distinct()
	grp_list = description.objects.values("grp").distinct()	
	instance_list = description.objects.values("instance").distinct()	
	
	#active_list = []
	#for act in active_list_tmp:
	#	if act == 0:
	#		active_list = active_list + ['NON ACTIVE']
	#	elif act == 1:
	#		active_list = active_list + ['ACTIVE']
	active_list = ['ACTIVE','NOT ACTIVE']
#	alarms_list = alarms.objects.all()
	descr_list = description.objects.all()
	if request.method == "GET":
		filter_name = request.GET.get('filter_name', request.COOKIES.get('filter_name_d', ''))
		filter_instance = request.GET.get('filter_instance', request.COOKIES.get('filter_instance_d', ''))
		filter_active = request.GET.get('filter_active', request.COOKIES.get('filter_active_d', ''))
		filter_level = request.GET.get('filter_level', request.COOKIES.get('filter_level_d', ''))
		filter_grp = request.GET.get('filter_grp', request.COOKIES.get('filter_grp_d', ''))
		filter_year_from = request.GET.get('year_from', request.COOKIES.get('filter_year_from_d', ''))
		filter_month_from = request.GET.get('month_from', request.COOKIES.get('filter_month_from_d', ''))
		filter_day_from = request.GET.get('day_from', request.COOKIES.get('filter_day_from_d', ''))
		filter_year_to = request.GET.get('year_to', request.COOKIES.get('filter_year_to_d', ''))
		filter_month_to = request.GET.get('month_to', request.COOKIES.get('filter_month_to_d', ''))
		filter_day_to = request.GET.get('day_to', request.COOKIES.get('filter_day_to_d', ''))
	elif request.method == "POST":
		filter_name = request.POST.get('filter_name', request.COOKIES.get('filter_name_d', ''))
		filter_instance = request.POST.get('filter_instance', request.COOKIES.get('filter_instance_d', ''))
		filter_active = request.POST.get('filter_active', request.COOKIES.get('filter_active_d', ''))
		filter_level = request.POST.get('filter_level',request.COOKIES.get('filter_level_d', ''))
		filter_grp = request.POST.get('filter_grp',request.COOKIES.get('filter_grp_d', ''))	
		filter_year_from = request.POST.get('year_from',request.COOKIES.get('filter_year_from_d', ''))
		filter_month_from = request.POST.get('month_from',request.COOKIES.get('filter_month_from_d', ''))
		filter_day_from = request.POST.get('day_from',request.COOKIES.get('filter_day_from_d', ''))
		filter_year_to = request.POST.get('year_to',request.COOKIES.get('filter_year_to_d', ''))
		filter_month_to = request.POST.get('month_to',request.COOKIES.get('filter_month_to_d', ''))
		filter_day_to = request.POST.get('day_to',request.COOKIES.get('filter_day_to_d', ''))	
		
	if filter_name:
#		alarms_list = alarms_list.filter(id_description=filter_name)
		#prova:
		descr_list = descr_list.filter(name=filter_name)
	if filter_instance:
		descr_list = descr_list.filter(instance=filter_instance)		
	if filter_active:
		if filter_active == 'ACTIVE':
			filter_active_tmp = 1
		else:
			filter_active_tmp = 0
		descr_list = descr_list.filter(active=filter_active_tmp)		

	if filter_level:
		descr_list = descr_list.filter(level=filter_level)

	if filter_grp:
		descr_list = descr_list.filter(grp=filter_grp)

	if filter_year_from and filter_month_from and filter_day_from:
		start_date = datetime.date(int(filter_year_from), int(filter_month_from), int(filter_day_from))
		ts_from = int(time.mktime(start_date.timetuple()))		
		descr_list = descr_list.filter(time_sec__gte=ts_from)
	else:
		filter_year_from = ''
		filter_month_from = ''
		filter_day_from = ''

	if filter_year_to and filter_month_to and filter_day_to:
		end_date = datetime.date(int(filter_year_to), int(filter_month_to), int(filter_day_to))
		ts_to = int(time.mktime(end_date.timetuple())) + 60*60*24 - 1		#include this day
		descr_list = descr_list.filter(time_sec__lte=ts_to)
	else:
		filter_year_to = ""
		filter_month_to = ""
		filter_day_to = ""
		
	################################### ordering #######################################################
	order_date = int(request.COOKIES.get('order_date_d', -1))
	tmp_ord = request.GET.get('order', '')
	if tmp_ord == str('date'):
		order_date = int(order_date) * -1		#invert order	
	if order_date == 1:
		descr_list = descr_list.order_by('time_sec')
	elif order_date == -1:
		descr_list = descr_list.order_by('-time_sec')
		
	################################### pagination #######################################################3
	row_per_page = 25
	total_id = descr_list.count()
	number_id = total_id // row_per_page		#20 results per page
	if number_id * row_per_page < total_id:
		number_id = number_id + 1
	if (int(view_id) > number_id) and (number_id > 0):
		view_id = str(number_id)
	if number_id == 0:
		view_id = '1'
	list_id = ['1']
	if int(view_id) == 2:
		list_id = list_id + ['2']
	elif int(view_id) == 3:
		list_id = list_id + ['2', '3']
	elif int(view_id) > 3:
		id1 = str(int(view_id) -1)
		id2 = str(int(view_id))
		list_id = list_id + ['...', id1, id2]
	
	if int(view_id) +2 < number_id:
		id1 = str(int(view_id) +1)
		list_id = list_id + [id1, '...', str(number_id)]
	elif int(view_id) +2 == number_id:
		id1 = str(int(view_id) +1)
		list_id = list_id + [id1, str(number_id)]
	elif int(view_id) +1 == number_id:
		list_id = list_id + [str(number_id)]		
		
	start_row = (int(view_id) - 1) * row_per_page
	end_row = int(view_id) * row_per_page
	
	table_id = { 'list_id': list_id, 'sel_id': str(view_id), 'count': total_id}
	if int(view_id) == 1:
		descr_list = descr_list[:row_per_page]
	elif int(view_id) == number_id:
		descr_list = descr_list[start_row:total_id]
	else:
		descr_list = descr_list[start_row:end_row]	
			
	selected = { 'name_sel': filter_name,'instance_sel': filter_instance,'active_sel': filter_active, 'level_sel': filter_level,'grp_sel': filter_grp, 'year_from_sel': filter_year_from, 'month_from_sel': filter_month_from, 'day_from_sel': filter_day_from, 'year_to_sel': filter_year_to,'month_to_sel': filter_month_to,'day_to_sel': filter_day_to, }
	
	combo_list = { 'name_list': name_list, 'instance_list': instance_list, 'active_list': active_list,'level_list': level_list,'grp_list': grp_list}
	ordering = {'order_date': order_date}
#	return render_to_response('list.html',{'alarms_list': alarms_list,'descr_list': descr_list})
	response = render_to_response('descr.html',{'descr_list': descr_list, 'current_date': now, 'combo_list': combo_list,'selected': selected, 'table_id': table_id, 'ordering': ordering})
	
	response.set_cookie('filter_name_d',value=filter_name)
	response.set_cookie('filter_instance_d',value=filter_instance)
	response.set_cookie('filter_active_d',value=filter_active)
	response.set_cookie('filter_level_d',value=filter_level)
	response.set_cookie('filter_grp_d',value=filter_grp)
	response.set_cookie('filter_year_from_d',value=filter_year_from)
	response.set_cookie('filter_month_from_d',value=filter_month_from)
	response.set_cookie('filter_day_from_d',value=filter_day_from)
	response.set_cookie('filter_year_to_d',value=filter_year_to)
	response.set_cookie('filter_month_to_d',value=filter_month_to)
	response.set_cookie('filter_day_to_d',value=filter_day_to)
	response.set_cookie('order_date_d',value=order_date)
	return response	
