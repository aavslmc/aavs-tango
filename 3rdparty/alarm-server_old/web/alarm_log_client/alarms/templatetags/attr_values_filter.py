from django import template
from datetime import datetime

register = template.Library()

@register.filter(name='cut1')
def cut1(value):
    return value.replace(';', ' ')

@register.filter(name='cut2')
def cut2(value):
    return value.replace('|', ' ')

@register.filter(name='unix_t')
def unix_t(value):
    return datetime.fromtimestamp(value)
