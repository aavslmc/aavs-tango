from django.db import models

# Create your models here.

class alarms(models.Model):
	id_alarms = models.AutoField(primary_key=True)
	time_sec = models.IntegerField(blank=False,null=False)
	time_usec = models.IntegerField(maxlength=6,blank=False,null=False)
	status = models.CharField(maxlength=6,choices=(('ALARM','ALARM'),('NORMAL','NORMAL')))
	ack= models.CharField(maxlength=4,choices=(('ACK','ACK'),('NACK','NACK')))
#	id_description = models.IntegerField(blank=False,null=False)
	id_description = models.ForeignKey('description',db_column='id_description')
	attr_values = models.CharField(maxlength=1024)

	def __str__(self):
		return self.id_description

	class Meta:
		db_table = 'alarms'
        	ordering = ["time_sec"]

	class Admin:
		list_display   = ('time_sec', 'time_usec', 'status','attr_values')
		list_filter    = ('status', 'ack')
		ordering       = ('time_sec',)
		search_fields  = ('time_sec',)

class description(models.Model):
	id_description = models.AutoField(primary_key=True)
	name = models.CharField(maxlength=128,blank=False,null=False)
	instance = models.CharField(maxlength=64,blank=False,null=False)
	active = models.IntegerField(maxlength=1,blank=False,null=False)
	time_sec = models.IntegerField(blank=False,null=False)
	formula = models.CharField(maxlength=1024,blank=False,null=False)
	time_threshold = models.IntegerField()
	level = models.CharField(maxlength=7,choices=(('log','log'),('warning','warning'),('fault','fault')))
	grp = models.CharField(maxlength=128,blank=False,null=False)
	msg = models.CharField(maxlength=255)
	action = models.CharField(maxlength=128)
	
	def __str__(self):
		return self.name

	class Meta:
		db_table = 'description'
		ordering = ["time_sec"]

	class Admin:
		list_display   = ('name', 'active', 'formula','msg')
		list_filter    = ('name', 'active')
		ordering       = ('name',)
		search_fields  = ('name',)
