/*
 * create users and assign privileges for the alarm database
 *
 * You are URGED to change password.
 */



/*
 * grant privileges to alarm user on localhost
 */
GRANT SELECT,UPDATE,INSERT,DELETE ON alarm.* TO 'alarm'@'localhost' IDENTIFIED BY 'AlaRm321';

/*
 * grant privileges to alarm user on all other remote hosts
 */

GRANT SELECT,UPDATE,INSERT,DELETE ON alarm.* TO 'alarm'@'%' IDENTIFIED BY 'AlaRm321';


/*
 * grant privileges to alarm-client read-only user on localhost
 */
GRANT SELECT ON alarm.* TO 'alarm-client'@'localhost';

/*
 * grant privileges to alarm-client read-only user on all other remote hosts
 */

GRANT SELECT ON alarm.* TO 'alarm-client'@'%';


/*
 * "COMMIT" changes
 */

FLUSH PRIVILEGES;
