import PyTango
import pickle
import json
from pyfabil import Device, BoardState

def _run_tile_cmd(tile_proxy, command, **kwargs):
    if kwargs:
        cmd_args = json.dumps(kwargs)
        return tile_proxy.command_inout(command, cmd_args)
    else:
        return tile_proxy.command_inout(command)

# Setup Tile proxy
tile = PyTango.DeviceProxy("test/tile/1")
tile.set_timeout_millis(100000)
tile.ip = "10.62.14.102"
tile.port = 10000
tile.lmc_ip = "10.62.14.17"
tile.lmc_port = 4660
tile.tpm_address = "test/tpm_board/1"

# Program Tile
_run_tile_cmd(tile, 'program_tile', bitfile='/home/andrea/Documents/AAVS/aavs-access-layer/bitfiles/itpm_v1_1_tpm_test_wrap_lbrc86.bit')

# Initialise TPM
_run_tile_cmd(tile, 'initialise_tile')

# read some new registers
print tile.read_attribute("fpga1.beamf.truncate_adj")

# write it back
#tile.write_attribute("fpga1.beamf.truncate_adj",'0x8')
#print tile.read_attribute("fpga1.beamf.truncate_adj")

# Try out the JSON stuff!
#json_string = _run_tile_cmd(tile, 'to_json')
#print json_string

print "Current temperature: %s" % str(tile.temperature)

# att = PyTango.AttributeProxy("test/tpm_board/1/temperature")
# attribute_config = att.get_config()

# Set up alarm
#_run_tile_cmd(tile, command='set_attribute_alarm', name="temperature", min_alarm="20", max_alarm="100")

# Stop alarm
#_run_tile_cmd(tile, command='stop_attribute_alarm', name="temperature")

# Re-setup Alarm
#_run_tile_cmd(tile, command='set_attribute_alarm', name="temperature", min_alarm="20", max_alarm="50")

# And stop alarm again
#_run_tile_cmd(tile, command='stop_attribute_alarm', name="temperature")

# Stop data transmissions
_run_tile_cmd(tile, 'stop_data_transmission', mode="ALL")

# Start sending raw data in poll mode
tile.raw_sync = False
tile.raw_timestamp = 0
tile.raw_seconds = 0.2
_run_tile_cmd(tile, command='start_poll_command', poll_command='send_raw_data', period=5000)
#_run_tile_cmd(tile, command='send_raw_data')

# # Start sending channelised data in poll mode
# tile.channelised_samples = 128
# tile.channelised_timestamp = 0
# tile.channelised_seconds = 0.2
# _run_tile_cmd(tile, command='start_poll_command', poll_command='send_channelised_data', period=5000)

# # Start sending beam data in poll mode
# tile.beam_timestamp = 0
# tile.beam_seconds = 0.2
# _run_tile_cmd(tile, command='start_poll_command', poll_command='send_beam_data', period=5000)


# # Send CSP data
# tile.csp_samples_per_packet = 128
# tile.csp_number_of_samples = 1024
# tile.csp_timestamp = 0
# tile.csp_seconds = 1
# _run_tile_cmd(tile, command='send_csp_data')





#_run_tile_cmd(tile, command='start_poll_command', poll_command='send_beam_data', period=5000)
#_run_tile_cmd(tile, 'disconnect')









#
# _run_tile_cmd('initialise', simulation=True)
#
# _run_tile_cmd(
#         'set_attribute_alarm',
#         name='temperature',
#         min_alarm='40',
#         max_alarm='60'
# )
#
#
# _run_tile_cmd('sync_fpgas')
# _run_tile_cmd('calibrate_fpga_to_cpld')
#
# _run_tile_cmd('send_raw_data', period=1)
# _run_tile_cmd('stop_raw_data')
#
# _run_tile_cmd('send_channelised_data', period=1)
# _run_tile_cmd('stop_channelised_data')
#
# _run_tile_cmd('send_beam_data', period=1)
# _run_tile_cmd('stop_beam_data')
#
# _run_tile_cmd('send_integrated_beam_data', integration_time=0.5)
# _run_tile_cmd('stop_integrated_beam_data')
#
# _run_tile_cmd('send_integrated_channel_data', integration_time=0.5)
# _run_tile_cmd('stop_integrated_channel_data')
#
# _run_tile_cmd('send_csp_data')
# _run_tile_cmd('send_csp_data', samples_per_packet=128, number_of_samples=128)
# _run_tile_cmd('stop_csp_data')
#
# _run_tile_cmd('send_channelised_data_continuous', channel_id=1)
# _run_tile_cmd('stop_channelised_data_continuous')
#
# _run_tile_cmd('stop_data_transmission')
#
# _run_tile_cmd('check_synchronization')
