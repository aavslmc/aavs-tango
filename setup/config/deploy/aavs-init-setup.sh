#!/bin/bash

export setupdir=$AAVS_PATH/aavs-tango/setup

sed \
    -e "s:%%path%%:$AAVS_PATH/aavs-tango/python/run:" \
$setupdir/config/init/aavs-lmc.template.conf > $setupdir/config/init/aavs-lmc.conf

sudo -E mv $setupdir/config/init/aavs-lmc.conf /etc/init/aavs-lmc.conf

sudo initctl reload-configuration

sudo service aavs-lmc restart
