#!/opt/aavs/python/bin/python

import sys
import PyTango
import platform
db  = PyTango.Database()

log_path = sys.argv[1]
path = sys.argv[2]

hostname = platform.uname()[1]
starter = "tango/admin/" + hostname
db.put_device_property(starter, {"LogFileHome": log_path + "/tango",
                                 "startDsPath": path + "/aavs-tango/python/run"})