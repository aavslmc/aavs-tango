#!/bin/bash

export setupdir=$AAVS_PATH/aavs-tango/setup

sed \
    -e "s:%%path%%:$AAVS_PATH/aavs-tango/python/run:" \
$setupdir/config/init/alarmctl.template.conf > $setupdir/config/init/alarmctl

chmod a+x $setupdir/config/init/alarmctl

if [ ! -f $AAVS_BIN/alarmctl ]; then
  ln -s $setupdir/config/init/alarmctl $AAVS_BIN/alarmctl
fi

# chmod +x  $AAVS_BIN/alarmctl
