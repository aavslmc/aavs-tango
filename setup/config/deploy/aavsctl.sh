#!/bin/bash

export setupdir=$AAVS_PATH/aavs-tango/setup

sed \
    -e "s:%%path%%:$AAVS_PATH/aavs-tango/python/run:" \
$setupdir/config/init/aavsctl.template.conf > $setupdir/config/init/aavsctl

chmod a+x $setupdir/config/init/aavsctl

if [ ! -f $AAVS_BIN/aavsctl ]; then
  ln -s $setupdir/config/init/aavsctl $AAVS_BIN/aavsctl
fi

# chmod +x  $AAVS_BIN/aavsctl
